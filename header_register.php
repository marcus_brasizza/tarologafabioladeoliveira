<?php
include_once ('conf/config.php');
@session_start();
if(isset( $_SESSION ['CM_GRANO_USER'] ['client'])){


  $consultores = $db->select ( 'consultor', ' status_consultor = 1 and atendimento_consultor = 1' );
  foreach($consultores as $idx=>$consultor){
    $nomesConsultores[$idx] = $consultor['nome_consultor']. ' '.$consultor['sobrenome_consultor'];
    $consult[$idx] = $consultor;
  }
  ?>
  <div class="col-md-6 col-md-offset-3 escolha-consultor">
  <h2> Escolha um de nossos consultores</h2>
  <ul class="nav nav-tabs nav-justified">
    <?php
    if($nomesConsultores){
    foreach($nomesConsultores as $idx=>$nomes){
      $classFirst = ($idx == 0) ? 'class="active"' : '';
      echo '<li '.$classFirst.'><a data-toggle="tab" href="#consultor_'.$idx.'">'.$nomes.'</a></li>';
    }
    ?>
  </ul>

  <div class="tab-content">
    <?php foreach($consult as $idx=>$consultor){
      $classFirst = ($idx == 0) ? 'in active"' : '';

      if ($consultor ['facebookid'] != '') {
        $imagemCli = $consultor ['foto_consultor'];
      } 				// Imagem do face
      else {
        if (file_exists ( 'admin454/dist/img/' . $consultor ['foto_consultor'] )) {
          $imagemCli = 'admin454/dist/img/' . $consultor ['foto_consultor'];
        } else {
          $imagemCli = 'admin454/dist/img/nophoto.jpg';
        }
      }
      ?>
      <div id="consultor_<?php echo $idx ;?>" class="tab-pane fade <?php echo $classFirst ;?>">
        <div class="box box-widget   widget-user-2" style="margin-top:15px;">
          <!-- Add the bg color to the header using any of the bg-* classes -->



          <div class="widget-user-header ">
            <div class="widget-user-image" >
              <img alt="User Avatar" src="<?php echo $imagemCli;?>" class="img-circle img-responsive center-block" width="120" height="120">
            </div>
            <!-- /.widget-user-image -->
            <h1 class="widget-user-username"></h1>
            <h5 class="widget-user-desc">&nbsp;</h5>

            <div class="widget-user-header bg-light-blue color-palette">
              <div   id="agendamento_<?php echo $consultor['id_consultor'];?>">

              </div>
            </div>
            <div class="row">
              <div class="col-md-12 bio-consultor">
                <h2>Bio</h2>
                <p>
                  <?php echo $consultor['bio_consultor'];?>
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>
      <? } ?>
    </div>
  </div>
    <?php
  }}else{
    ?>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h2>Vivêncie uma experiência transformadora.</h2>
        <p>
          Faça Login e agende sua consulta
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2 col-md-offset-4">
        <a class="btn btn-default fb-button" href="loginFacebook.php" role="button">Facebook</a>
      </div>
      <div class="col-md-2">
        <a class="btn-user-login btn-acao-inside btn btn-default" href="login.php" role="button" onclick='return false'>Email</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-md-offset-4 margem-cadastro-botao">
        <span>Ainda não tenho <a id="mostrarCadastro" class='btn-acao-inside btn-mostra-cadastro' href="register.php" onclick='return false'>cadastro</button></span>
        </div>

      </div>
    </div>
    <? } ?>
    <script type="text/javascript">


    function procuraAgendamento(){

      $('[id^="agendamento_"]').each(function() {

        minha_div_consultor = new String($(this).attr('id'));
        div_final_consultor = $(this).attr('id');
        $("#minha_div_consultor").html('');
        string_id_consultor = minha_div_consultor.split('_');
        consultor = string_id_consultor[1];
        var dataRetorno = '';
        $.ajax({
          url: "process/novasConsultas.php",
          data: "id_consultor=" + consultor,
          type: "POST",
          async: false,
          success: function(data) {
            dataRetorno = data;
          }
        })
        $(this).html(dataRetorno);
      })


    }


    //Script anchor
    (function($){


      $(".btn-acao-inside").on('click',function() {

        href_link = ($(this).attr('href'))
        if(href_link != '#'){

          $(".modal-body").load('loading.php',function(){
            $('#modalSystem').modal('show');
            $('.modal-footer').show();
            $(".modal-body").load(href_link,function(){
              $('#modalSystem').modal('show');
            })
          })
        }
        //$(this).find(".modal-body").load(link.attr("href"));
      });



      if(localStorage.cliente){
        cliente = eval('('+ (localStorage.cliente) + ')');

        if(cliente.id_cliente != null){
          $(".btn-user-login ").attr('href','lockscreen.php');
        }
      }


      procuraAgendamento();



    })(jQuery);


  function confirmarAgenda(data,user){
        strUser = user

        cod_agendamento = data

        $.post('process/checkCreditos.php', {usuario: strUser, agendamento:cod_agendamento},function(data){

          data = data.split('_');

          acao = data[0];
          id_agendamento = data[1];

          if(acao == 'aceitar'){
            $(".modal-body").load('confirmarAgendamento.php?agendamento='+id_agendamento,function(){
              $('#loginModal').modal('show');
            })
          }else if(acao == 'credito'){
            $(".modal-body").load('carregarCredito.php?agendamento='+id_agendamento,function(){
              $('#modalSystem').modal('show');
            })
            //document.location='carregarCredito.php?agendamento='+id_agendamento;


          }



        });

      }


    	setInterval(procuraAgendamento(), 15000);
    </script>
