<?php
session_start();
include_once('conf/config.php');

$configs = $db->select('configuracoes');
$configs = $configs[0];
$img_logotipo = 'admin454/dist/img/'.$configs['logotipo_cliente'];

$bind = array(':transacao' => $_GET['transaction_id']);
$pagamento = $db->select('pagamento' , 'transacao_pagamento = :transacao',$bind);

$pagamento = $pagamento[0];

$cliente = $db->select('cliente','id_cliente = '.$pagamento['id_cliente']);


$cliente = $cliente[0];
$_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $cliente );

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Confirmação de pagamento | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="admin454/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="admin454/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="admin454/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
		($(function(){

			$('#modalSystem').on('hidden.bs.modal', function () {
			    document.location='index.php';
			})

			}))

    </script>
  </head>
  <?php if(isset($_GET['imprimir'])){
      echo '<body class="hold-transition skin-blue sidebar-mini" onload="window.print()">';
  }else{
  echo '<body class="hold-transition skin-blue sidebar-mini">';
}?>

       
		
        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
           <div class="widget-user-header bg-gray">
                  <div class="widget-user-image">
                    <img src="<?php echo $img_logotipo;?>" class="img-rounded" width="100">
                  </div><!-- /.widget-user-image -->
                 
                </div>
          </div>
          <!-- info row -->
          <div class="row invoice-info">
          <br>
           
            <div class="col-sm-4 invoice-col">
             <?php if($cliente['facebookid'] != ''){
             	
             	$imagem_cliente = $cliente['foto_cliente'];
             }else{
             	$imagem_cliente = 'admin454/dist/img/'.$cliente['foto_cliente'];
             	if(!file_exists($imagem_cliente)){
				$imagem_cliente = 'admin454/dist/img/nophoto.png';
				}

             }
             ?>
            	  <img alt="User Avatar" src="<?php echo $imagem_cliente;?>" class="img-circle" width="200">
            </div><!-- /.col -->
             <div class="col-sm-4 invoice-col">
              Dados do Pagador
              <address>
                <strong><?php echo $cliente['nome_cliente'];?></strong><br>
                <?php echo $cliente['endereco_cliente'] . ' ' . $cliente['numero_cliente']; ?><br>
                <?php echo $cliente['cidade_cliente'] .'-'. $cliente['estado_cliente']  ;?><br>
               
                Telefone: <?php echo $cliente['telefone_cliente']  ;?><br>
                Email: <?php echo $cliente['email_cliente']  ;?><br>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <b>Código do pagamento #<?php echo $pagamento['id_pagamento'];?></b><br>
              <br>
              <b>Código da transação:</b> <br /><?php echo $pagamento['numero_pagamento'];?><br>
              <b>Data de pagamento:</b> <br /><?php echo date('d/m/Y H:i:s',strtotime($pagamento['data_pagamento']));?><br>
              <b>Referencia de pagamento:</b> <br /><?php echo $pagamento['transacao_pagamento'];?>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
              
                    <th>Código</th>
                    <th>Serviço</th>
                    
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $pagamento['id_pagamento'];;?></td>
                    <td><?php echo $pagamento['item_pagamento'];?></td>
                    <td>R$ <?php echo number_format($pagamento['valor_pagamento'],2,',','');?></td>
                   
                  </tr>
                 
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
             	<img src='admin454/data/img/pagseguro.png'>
              
            </div><!-- /.col -->
            <div class="col-xs-6">
              <p class="lead"><?php echo date('d/m/Y',strtotime($pagamento['data_pagamento']));?></p>
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Sub-total:</th>
                    <td>R$ <?php echo number_format($pagamento['valor_pagamento'],2,',','');?></td>
                  </tr>                 
                  <tr>
                    <th>Total:</th>
                    <td>R$ <?php echo number_format($pagamento['valor_pagamento'],2,',','');?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
            <br>
              <a href="invoice.php?transaction_id=<?php echo $_GET['transaction_id'];?>&imprimir=true"  target="_blank" class="btn btn-default" ><i class="fa fa-print"></i> Imprimir</a>
              
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->


    <!-- jQuery 2.1.4 -->
    <script src="admin454/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="admin454/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="admin454/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="admin454/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="admin454/dist/js/demo.js"></script>
  </body>
</html>
