<?php 
include_once ('conf/config.php');
$carta_banco = $db->select('cartas_baralho' , '1=1 order by rand() limit 1');
$carta_banco = $carta_banco[0];

//Gerar Opacidade da carta da semana

$imageName= 'admin454/'.$carta_banco['imagem_carta'];
$im_src = imagecreatefromjpeg($imageName);
$size = getimagesize($imageName);
$im_dst = imagecreatefromjpeg($imageName);
$white = imagecolorallocate($im_dst, 255, 255, 255);
imagecolortransparent($im_dst, $white);
imagefilledrectangle($im_dst, 0, 0, $size[0], $size[1], $white);
$opacityVal = 50;// put the opacity value here
imagecopymerge($im_dst, $im_src, 0, 0, 0, 0,$size[0], $size[1], $opacityVal);
$mask = umask();

imagejpeg( $im_dst , 'img/carta-da-semana.jpg',100);
umask(0);
chmod('img/carta-da-semana.jpg',0777);
imagedestroy($im_dst);
imagedestroy($im_src);




 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Taróloga Fabíola de Oliveira</title>
  <meta name="description" content="Tarôt online com Fabiola de Oliveira">
  <meta name="keywords" content="tarôt online, esoterismo, leia sua sorte">
  <meta name="robots" content="index, follow">
  <meta content="width=device-width,initial-scale=1.0" name="viewport">
  <link rel="shortcut icon" href="img/icon.ico">
  <link rel="stylesheet" type="text/css" href="stylesheets/styles.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="javascripts/bootstrap.js"></script>
  <script src="javascripts/validator.js"></script>
  <script src="javascripts/jquery.slicknav.js"></script>
  <script src="javascripts/owl.carousel.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src="javascripts/jquery.malihu.PageScroll2id.min.js"></script>

	</head>

<body>
  <header id="box">
      <div class="login-bar">
        <a href="#">Login</a> | <a href="">Cadastre-se</a>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <img class="img-responsive center-block" src="img/logo-fabiola.png" alt="Tarologa Fabíola de Oliveira">
          </div>
          <div class="col-md-7 margem-menu">
            <ul id="menu">
            	<li><a href="#home" rel="m_PageScroll2id">Home</a></li>
            	<li><a href="#Carta-da-semana" rel="m_PageScroll2id">Carta da Semana</a></li>
            	<li><a href="#Blog" rel="m_PageScroll2id">Blog</a></li>
            	<li><a href="#Sobre" rel="m_PageScroll2id">Sobre</a></li>
              <li><a href="#Eventos" rel="m_PageScroll2id">Eventos</a></li>
              <li><a href="#contato" rel="m_PageScroll2id">Contato</a></li>
              <li><a href="#" role="button" data-toggle="modal" data-target="#agendarConsulta">Agendar consulta</a></li>
              <li class="mobile-link-only"><a href="#">Login</a></li>
              <li class="mobile-link-only"><a href="#">Cadastre-se</a></li>
            </ul>
          </div>
          <div class="col-md-1  margem-menu fb-logo">
            <a href="#"><img class="img-responsive center-block" width="20" src="img/fb.png" alt="Facebook"></a>
          </div>
        </div>
      </div>
      <script>
      	$(function(){
      		$('#menu').slicknav();
      	});
      </script>
  </header>

  <section id="home">
    <div class="container">
      <div class="row margem-home">
        <div class="col-md-4 col-md-offset-2 mobile-carta">
          <img class="img-responsive center-block" src="img/carta-home.png" alt="Carta da semana">
        </div>
        <div class="col-md-4 home-mobile">
          <h2>Loren ipsum dolor sit amet</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum nihil, vel! Officiis soluta, tempora quos iure eaque, quae dicta recusandae eum excepturi impedit amet delectus repellendus nisi at optio ipsa.<br><br>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum nihil, vel! Officiis soluta, tempora quos iure eaque, quae dicta.
          </p>
          <a class="btn btn-default btn-acao" href="header_register.php" role="button"  onclick="return false;">Agendar consulta</a> <a class="btn como-funciona" href="#">Como funciona</a>
        </div>
      </div>
    </div>
    <!--MODAL AGENDAR-->
    <div class="modal fade" id="agendarConsulta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="areaLogin">
              
          </div>
        </div>
      </div>
    </div>
  </section>


  <section id="Carta-da-semana">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-2 margem-carta home-mobile">
          <h2>Carta da semana</h2>
          <?php echo html_entity_decode( $carta_banco['texto_carta']) ;?>
          <br>
          <p>Saiba como essa carta influência sua vida</p><a class="btn btn-default btn-acao" href="header_register.php" role="button" onclick='return false'>Agende uma consulta</a>
        </div>
      </div>
    </div>
  </section>


  <section id="Blog">
    <div class="container-fluid">
      <div class="header-blog">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-1"><h2>Blog</h2></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="owl-carousel">
          <div class="carousel-img blog-image-1"><div><h2>Loren ipsum dolor</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora et facilis soluta ipsa nostrum repellat sapiente error commodi sunt ad, ut impedit neque, velit distinctio eligendi? Velit dolorem magnam natus!</p><a class="btn btn-default" href="#" role="button">Saiba mais</a></div></div>
          <div class="carousel-img blog-image-2"><div><h2>Loren ipsum dolor</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora et facilis soluta ipsa nostrum repellat sapiente error commodi sunt ad, ut impedit neque, velit distinctio eligendi? Velit dolorem magnam natus!</p><a class="btn btn-default" href="#" role="button">Saiba mais</a></div></div>
          <div class="carousel-img blog-image-3"><div><h2>Loren ipsum dolor</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora et facilis soluta ipsa nostrum repellat sapiente error commodi sunt ad, ut impedit neque, velit distinctio eligendi? Velit dolorem magnam natus!</p><a class="btn btn-default" href="#" role="button">Saiba mais</a></div></div>
        </div>
        <script>
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel({
          items:1,
          loop:true,
          margin:10,
          autoplay:true,
          autoplayTimeout:3000,
          autoplayHoverPause:true
          });
        });
        </script>
      </div>
    </div>
  </section>

  <section id="Sobre">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-1">
          <img class="img-responsive center-block img-circle" src="img/img-sobre.jpg" alt="">
        </div>
        <div class="col-md-6 home-mobile">

          <h2>Sobre a Fabíola de Oliveira</h2>
          <div class="sobre">
            <p>
              Fabíola de Oliveira é escritora, dramaturga e espiritualista. Clarividente desde a infância, a sua orientação e educação espiritual foi pautada em anos de estudos e buscas em diversos segmentos iniciáticos e espiritualistas. Onde aprendeu canalizar as suas energias e compreendeu sua missão de vida que é fazer um elo entre as dimensões visíveis e invisíveis que nos cercam. E, assim, buscar a harmonização e o perfeito equilíbrio entre os poderes do físico, vital, emocional e mental dos seres. Proporcionando mudança de pensamento, sentimento e consequentemente de vida. Acredita que tudo que nos cerca é energia e ao identificar os bloqueios na vida diária, através do Tarot, podemos encontrar a orientação necessária com vista à evolução e prosperidade em todos os sentidos.
              <br><br>
              Eu exerço a leitura do Tarot através da Taromancia (clarividência), utilizando o Tarot como oráculo. É o modo mais antigo de se trabalhar o Tarot. Após me preparar espiritualmente e me conectar com meus mentores espirituais, entro em contato com a sua energia, pois energia não tem tempo e nem espaço. Então, através das cartas do Tarot, dou uma visão geral de como está a sua situação atual E após essa introdução, você indica as as situações da sua vida que gostaria de obter melhor entendimento.
              Minha missão é demonstrar as pessoas através da Leitura do Tarot que sempre haverá um caminho e uma resposta por mais complexa que a situação se apresente. Cada ser tem em si dons e as ferramentas necessárias que precisam para seguir o bom caminho. Só é necessário uma mão que o oriente. Sou espiritualista e o meu trabalho é pautado na ética e seriedade, pois eu sei das responsabilidades e consequências de um trabalho deste porte.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="owl-carousel">
          <div class="carousel-img"><div class="col-md-6 col-md-offset-3"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ratione ut laudantium in qui d  eserunt labore commodi. Incidunt repellat, laboriosam at, dolor, odio doloribus nesciunt im  pedit ea earum assumenda iure?<br> <i>- Nome Teste</i></p></div></div>
          <div class="carousel-img"><div class="col-md-6 col-md-offset-3"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore odit vitae est quos porro quaerat, blanditiis saepe officiis rerum voluptatibus. Perspiciatis exercitationem, dolor veniam accusantium. In necessitatibus asperiores laborum, eaque.<br> <i>- Nome Teste</i></p></div></div>
          <div class="carousel-img"><div class="col-md-6 col-md-offset-3"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam mollitia hic pariatur reprehenderit quod ratione qui perspiciatis! Architecto asperiores, commodi exercitationem illum necessitatibus repellendus beatae adipisci non quas hic. Nemo!<br> <i>- Nome Teste</i></p></div></div>
        </div>
      </div>
    </div>
  </section>

  <section id="Eventos">
    <div class="container">
      <div class="row header-eventos">
        <div class="col-md-6 col-md-offset-3 margem-agenda">
          <h2>Agenda de eventos</h2>
          <p>Fique por dentro de nossos cursos e eventos.</p>
        </div>
      </div>
      <div class="row margem-menu">
        <div class="col-md-8 col-md-offset-2">
          <div class="areaEventos">
            <div class="dataEvento">
              <div class="mes">Jun</div>
              <div class="dia">00</div>
            </div>
            <div class="localEvento">
              <h2>Local do evento</h2>
              <p>Endereço do local, nº 00 - Bairro, cidade, estado</p>
            </div>
            <div class="maisInformacoes">
              <a class="btn btn-default" href="#" role="button">Mais informações</a>
            </div>
          </div>

          <div class="areaEventos">
            <div class="dataEvento">
              <div class="mes">Jun</div>
              <div class="dia">00</div>
            </div>
            <div class="localEvento">
              <h2>Local do evento</h2>
              <p>Endereço do local, nº 00 - Bairro, cidade, estado</p>
            </div>
            <div class="maisInformacoes">
              <a class="btn btn-default" href="#" role="button">Mais informações</a>
            </div>
          </div>

          <div class="areaEventos">
            <div class="dataEvento">
              <div class="mes">Jun</div>
              <div class="dia">00</div>
            </div>
            <div class="localEvento">
              <h2>Local do evento</h2>
              <p>Endereço do local, nº 00 - Bairro, cidade, estado</p>
            </div>
            <div class="maisInformacoes">
              <a class="btn btn-default" href="#" role="button">Mais informações</a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section id="contato">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <h2>Dúvidas? Entre em contato</h2>
          <p>Telefone: 11 0000 0000</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <form action="">
            <div class="form-group">
              <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nome">
            </div>
            <div class="form-group">
              <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Telefone">
            </div>
            <textarea class="form-control" rows="6"></textarea>
            <br>
            <button type="submit" class="btn btn-default btn-block">Enviar</button>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!--Script para background nav-->
  <script type="text/javascript">
    $(window).on("scroll", function() {
        if($(this).scrollTop() > 80) {
            $("header").addClass("cor-header");
        } else {
           $("header").removeClass("cor-header");
        }
    });
  //Script anchor
    (function($){
        $('html,body').load(function(){
            $("#menu a").mPageScroll2id();
        });
				
				$(".btn-acao").on('click',function() {				
					href_link = ($(this).attr('href'))
				
					$('.modal-footer').show();
					$(".modal-body").load(href_link,function(){
						$('#agendarConsulta').modal('show');
					})				
					//$(this).find(".modal-body").load(link.attr("href"));
				});
				
		
				
    })(jQuery);
  </script>
</body>
</html>
