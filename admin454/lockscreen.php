<?php
@session_start();

$msg = '';
if($_POST){

	include_once('../conf/config.php');

	$usuario  = filter_input(INPUT_POST, 'user-cmaster', FILTER_DEFAULT);
	$senha  = md5(filter_input(INPUT_POST, 'pass-cmaster', FILTER_DEFAULT));

	$bind = array(
			":usuario" => $usuario,
			":senha" =>($senha)
	);
	$dados = $db->select("consultor", "email_consultor =  :usuario and status_consultor = 1 and senha_consultor = :senha", $bind);
	$dados = $dados[0];
	
	if($dados ){
		
		$_SESSION['CM_GRANO_USER']['user'] = json_encode($dados);
		
		
	
		header('location:central.php');
		exit;

	}else{

		$msg = "<span class='label label-important'>Usuário ou senha inválidos.</span>";

	}

}else{
	session_destroy();
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GRANO | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	 <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
    
    <script>
		($(function(){

			usuario = eval('('+ (localStorage.usuario) + ')');
			console.log(usuario);
			$('#imagemUsuario').attr('src','dist/img/'+usuario.foto_consultor);
			$(".lockscreen-name").html(usuario.nome_consultor+ ' '+ usuario.sobrenome_consultor);
			$("#user-cmaster").val(usuario.email_consultor);

			$("#pass-cmaster").focus();
			}))
 
    </script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
          <a href="index.php"><b>FRAMEWORK</b>GRANO</a>
      </div>
      <!-- User name -->
      <div class="lockscreen-name">John Doe</div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
          <img src="" alt="Consultor" id='imagemUsuario'>
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials"  action="lockscreen.php" method="post">
          <div class="input-group">
            <input type='hidden' name='user-cmaster' id='user-cmaster'>
            <input type="password" class="form-control focusedInput" name='pass-cmaster' id='pass-cmaster' placeholder="password">

            <div class="input-group-btn">
              <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
          </div>
        </form><!-- /.lockscreen credentials -->

      </div><!-- /.lockscreen-item -->
      <div class="help-block text-center">
        Entre sua senha para recuperar sua sessão
      </div>
      <div class="text-center">
        <a href="login.php">Caso não seja você clique aqui</a>
      </div>
      
    </div><!-- /.center -->

    <!-- jQuery 2.1.4 -->
   
	
	
  </body>
</html>
