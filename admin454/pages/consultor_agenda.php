
<div id="editdialog" style="z-index: 999999999"></div>

<?php
$pagina_atual = 'agenda';

?>
<link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet"
	href="plugins/fullcalendar/fullcalendar.print.css" media="print">
<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="plugins/fullcalendar/pt-br.js"></script>

<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<link rel="stylesheet"
	href="plugins/timepicker/bootstrap-timepicker.min.css">

<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>
<input type="hidden" name='consultor' id='consultor'  value = "<?php echo $_GET['consultor'];?>" >
<section class="content">
	<!-- Default box -->
	<div class="box">
		
		<div class="box-body">
			<div id="calendar"></div>
		</div>
		<!-- /.box-body -->
		
		<!-- /.box-footer-->
	</div>
	<!-- /.box -->
</section>

<script>

          var calendar = '';
          ($(function(){
     		 $( "#editdialog" ).dialog({
     				height: 370,
     				width: 300,

     				
     				resizable: false,
     				autoOpen: false,
     				show: {
     					effect: "blind",
     					duration: 1000
     				},
     				hide: {
     					effect: "explode",
     					duration: 1000
     				},
     				title : 'Agenda do consultor'
     			});

         	  calendar = $('#calendar').fullCalendar({
         		 header: {
         			 left: 'title',
         			 right: 'prev,next ,agendaDay'
         		 },
          		
         		 events: {
         					url: 'process/loadEvents.php?consultor='+$('#consultor').val(),
         					type: 'POST', // Send post data

         			},
         		 eventClick: function(calEvent, jsEvent, view)
         		 {

					console.log(calEvent);
					confirma = confirm("Deseja excluir o evento clicado? ("+calEvent.title+")");
					if(confirma){
					$.post('process/removeEvento.php', {id_agendamento:calEvent.id},function(data){
						 calendar.fullCalendar( 'refetchEvents' );
						})
					}

         			 return false;
         		 },
         		 select: function (start, end, allDay) {

          			start = new String(start).replace('GMT+0000' , '');
          			
         			 var check = new Date(start);
         			
          		    var today = (new Date());
            		 
          		    if(check.getTime() < today.getTime())
          		    {
         		    	 calendar.fullCalendar('unselect');
         		    	 return false;
          		    }
							
       				         	dtIni = new String(start);
          				         dtFim = new String(end);
					consultor = $("#consultor").val();
        			 var url = "process/adicionarEvento.php";
        				$.ajax({
        					type: 'POST',
        					context: this,
        					url: url,
        					data: {
        						dtIni: dtIni,
        						dtFim: dtFim,
        						consultor: consultor


        					},
        					dataType: 'html',
        					success: function(data) {
        						$('#editdialog').html(data);
        					},
        					error:function() {

        					}
        					
        					
        				});

        		       $( "#editdialog" ).dialog( "open" );
        			 calendar.fullCalendar('unselect');
        		 },


        		 viewRender: function(currentView){
     				var minDate = moment(),
     				maxDate = moment().add(6,'weeks');
     				// Past
     				if (minDate >= currentView.start && minDate <= currentView.end) {
     					$(".fc-prev-button").prop('disabled', true); 
     					$(".fc-prev-button").addClass('fc-state-disabled'); 
     				}
     				else {
     					$(".fc-prev-button").removeClass('fc-state-disabled'); 
     					$(".fc-prev-button").prop('disabled', false); 
     				}
     				// Future
     				if (maxDate >= currentView.start && maxDate <= currentView.end) {
     					$(".fc-next-button").prop('disabled', true); 
     					$(".fc-next-button").addClass('fc-state-disabled'); 
     				} else {
     					$(".fc-next-button").removeClass('fc-state-disabled'); 
     					$(".fc-next-button").prop('disabled', false); 
     				}
     			},
        		 
     			selectable:true,
    			timezone: 'America/Sao_Paulo'
         	  })

         	 calendar.fullCalendar('changeView', 'agendaDay');
           	 

         		
          }))
          
          </script>

