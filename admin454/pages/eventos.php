<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<?php
$usuario = json_decode ( $_SESSION ['CM_GRANO_USER'] ['user'] );
if ($_POST == true) {
    
    
    foreach($_POST as $idx=>$value){
        $$idx=$value;
    }
    
    list($data,$hora) = explode(' ', $data_evento);
    list($dia,$mes,$ano) = explode('/',$data);
    
    $data_evento = $ano.'-'.$mes.'-'.$dia. ' '.$hora;
    
    foreach($_POST as $idx=>$value){    
        $arrayInsert[$idx] = $$idx;    
    }
    
    if($id_evento != ''){

        unset($arrayInsert['id_evento']);
        //UPDATE
        $banco->update('eventos',$arrayInsert , " id_evento = {$id_evento}");
    }else{
        //InSERT
        $banco->insert('eventos',$arrayInsert);
       
    }
    echo "<script>document.location.href = 'central.php?action-grano-filter=eventos'</script>";
    exit;
}

if (isset ( $_GET ['status'] )) {
	if ($_GET ['status'] == 0) {
		$status = 0;
	} else {
		$status = 1;
	}
	$bind = array (
			'status_evento' => $status
	);
	$id_consultor = base64_decode($_GET ['id_evento']);
	$banco->update ( 'eventos', $bind, " id_evento = {$id_consultor}" );

	echo "<script>document.location.href = 'central.php?action-grano-filter=eventos'</script>";

	exit ();
}


if (isset ( $_GET ['edit_evento'] )) {

	$id_evento = base64_decode($_GET ['edit_evento']);
	$bind = array('id_evento' => $id_evento);
	$eventoEdit = $banco->select ( 'eventos' , 'id_evento = :id_evento' , $bind);
	if($eventoEdit){
		$eventoEdit = $eventoEdit[0];

	}else{
		$eventoEdit = array();
	}

	foreach($eventoEdit as $idx=>$evento){
		$$idx = $evento;
	}




}

$pagina_atual = 'eventos';


	
	$eventos = $banco->select ( 'eventos' ); 


?>

<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Eventos</h3>

			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>

						<th>Data</th>
						<th>Título</th>						
						<th>Status</th>

					</tr>
					<?php

					if ($eventos) {
						foreach ( $eventos as $evento ) {

							$status = ($evento ['status_evento'] == 1) ? '<span class="label label-success">Atvio</span>' : '<span class="label label-danger">Inativo</span>';
							?>

					<tr>

						<td><a href="?action-grano-filter=eventos&edit_evento=<?php echo base64_encode($evento['id_evento']);?>"><?php echo date('d/m/Y H:i:s', strtotime($evento['data_evento'])) ; ?></a></td>
						<td><?php echo $evento['titulo_evento'] ;?></td>
				
						<td><a
							href="?action-grano-filter=eventos&amp;id_evento=<?php echo base64_encode($evento['id_evento']);?>&amp;status=<?php echo ($evento['status_evento'] == 1 ? '0':'1') ;?>"><?php echo $status ;?>
						</a></td>
					</tr>

					<?php
						}
					}
					;
					?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>

	<!-- /.box-body -->
	<div class="box-footer bg-gray color-palette">
		<h3 class="box-title">Consultor</h3>

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<!-- text input -->
		
			<input		type='hidden' name='id_evento' id='id_evento' value="<?php echo $id_evento;?>">
			
		

			<!-- /.form group -->

			<div class="form-group col-xs-5">
				<label>Local do evento:</label> <input type="text"
					class="form-control" name='titulo_evento' placeholder="Local do evento" value="<?php echo $titulo_evento;?>" required>
			</div>
	<div class="clearfix"></div>
			
			<div class="form-group col-xs-5">
				<label>Data do evento:</label> <input type=text class="form-control"
					name='data_evento' 
					data-mask  value="<?php echo ($data_evento) ? date('d/m/Y H:i:s',strtotime($data_evento)) : '' ;?>" required>
			</div>
			<div class="clearfix"></div>

			<h3 class="box-title">
				Informações: <small>Escreva algo sobre o evento</small>
			</h3>
			<!-- tools box -->



			<div class="box-body pad">
				<textarea class="textarea" name="texto_evento"
					placeholder="Escreva algo"
					style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
					cols="" rows=""><?php echo $texto_evento ; ?></textarea>

			</div>
			<div>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</form>
	</div>

</section>
<script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
    	  $("[data-mask]").inputmask('d/m/y h:m:s'  );
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();



      });




    </script>

