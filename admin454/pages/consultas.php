<?php
$pagina_atual = 'consultas';
$usuario = json_decode ( $_SESSION ['CM_GRANO_USER'] ['user'] );

$sql = "select  mensagem_agendamento_cliente , id_agendamento_cliente,finalizado_agendamento,nota_agendamento,
inicio_agendamento,fim_agendamento ,ac.info_agendamento_cliente,
c.nome_cliente  from agendamento a  join agendamento_cliente ac on ac.id_agendamento = a.id_agendamento 
join cliente c on c.id_cliente = ac.id_cliente  where a.id_consultor = ".$usuario->id_consultor. " order by finalizado_agendamento ASC , fim_agendamento ASC";
$retorno = $banco->run($sql);

?>
<link rel="stylesheet"
href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
type="text/javascript"></script>
<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">
	
	
	<div class="box-body">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Agendamentos pendentes</h3>
				
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-striped">
					<tr>					
						<th>Agendamento</th>
						<th>Cliente</th>
						<th>Solicitação</th>
						<th></th>
						<th></th>						
					</tr>
					
					<?php 
					if($retorno){
						foreach ($retorno as $consulta) {

							$strData = date('d/m/Y',strtotime($consulta['inicio_agendamento'])). '<br /> <strong> ' . date('H:i:s',strtotime($consulta['inicio_agendamento'])) .'<br />às<br />'.date('H:i:s',strtotime($consulta['fim_agendamento'])).'</strong>';						
							if($consulta['finalizado_agendamento'] == '1'){
								echo '<tr class="alert alert-success">';
								$btn_finaliza = 'Consulta : '.(($consulta['nota_agendamento'] == null ) ? ' Não avaliada':$consulta['nota_agendamento']);
								$btn_email = nl2br(html_entity_decode($consulta['mensagem_agendamento_cliente']));

								$btn_criarSala = '';

							}else{
								echo '<tr>';
								$btn_email = '	<a class="btn-email btn btn-default btn-flat" id_agendamento="'.$consulta['id_agendamento_cliente'].'">Enviar E-mail</a>';
								$btn_finaliza = '	<a class="btn-agendamento btn btn-default btn-flat" id_agendamento="'.$consulta['id_agendamento_cliente'].'">Iniciar atendimento</a>';

								
							}
							?>

							<td nowrap><?php echo $strData;?></td>
							<td nowrap><?php echo $consulta['nome_cliente'];?></td>
							<td width="60%"><?php echo nl2br(html_entity_decode($consulta['info_agendamento_cliente']));?></td>
							<td>
								<?php echo $btn_criarSala;?>
								<?php echo $btn_finaliza;?>
							</td>			
							<td><?php echo $btn_email ;?></td>						
						</tr>
						<?php 	}}?>

					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		
		<!-- /.box-body -->
		
		
	</section>
	
	<script type="text/javascript">   
		($(function(){
			$(".btn-agendamento").click(function(){
				var agendamento = $(this).attr('id_agendamento');
				confirma = confirm('Deseja realmente criar a sala para iniciar o atendimento?');
				if(confirma){			
					$.post('process/alteraAgendamento.php' , {id_agendamento_cliente : $(this).attr('id_agendamento')},function(data){		
						if(data == 'ok'){
							popIn = window.open ('../tvConsultor.php?agendamento='+agendamento,'win1','width=700,height=500,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no');

							if(!popIn){
								alert("Bloqueio de pop-up detectado.\nVerifique seu navegador e habilite a opção");
							}
						}
					})
				}
			})


			$(".btn-email").click(function(){

				confirma = confirm('Deseja realmente enviar um lembrete para o cliente?');
				if(confirma){						
					$.post('../process/emailAgendamento.php' , {id_agendamento_cliente : $(this).attr('id_agendamento')},function(data){

						if(data == 'ok'){
							alert('Lembrete enviado com sucesso');
						}				

					})
				}

			})

		}))
	</script>
	
