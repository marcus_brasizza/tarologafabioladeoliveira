<?php


$carta_tarot  = filter_input(INPUT_GET, 'carta', FILTER_SANITIZE_SPECIAL_CHARS);

$carta_tarot = base64_decode($carta_tarot);
$bind = array(':imagem_carta' => $carta_tarot);
$carta_banco = $banco->select('cartas_baralho' , 'imagem_carta = :imagem_carta', $bind);

$carta_banco = $carta_banco[0];
$id_carta = $carta_banco['id_carta'];
$texto_carta = $carta_banco['texto_carta'];
if ($_POST == true) {
    
    if(isset($_POST['carta_semana'])){
        
        $db->run("Update cartas_baralho set carta_semana = 0 ");
        
    }
	
	$imagem_carta  = filter_input(INPUT_POST, 'imagem_carta', FILTER_SANITIZE_SPECIAL_CHARS);
	$texto_carta  = filter_input(INPUT_POST, 'texto_carta', FILTER_SANITIZE_SPECIAL_CHARS);
	$carta_semana  = filter_input(INPUT_POST, 'carta_semana', FILTER_SANITIZE_SPECIAL_CHARS);
	
	
	foreach($_POST as $idx => $valores){
		
		$array_baralho[$idx] = $$idx;
	}

		
		if($array_baralho['id_carta'] == ''){
			$db->insert('cartas_baralho',$array_baralho);
			
		}else{
			$db->update('cartas_baralho',$array_baralho, ' id_carta = '.$id_carta);
		}
	
	echo "<script>document.location.href = 'central.php?action-grano-filter=cartas_tarot'</script>";
	exit ();
}





$pagina_atual = 'Edição de carta';
$configuracoes = $banco->select ( 'configuracoes' );
$configuracoes= $configuracoes[0];

$logotipo_cliente = $configuracoes['logotipo_cliente'];
$token_pagseguro = $configuracoes['token_pagseguro'];
$email_pagseguro = $configuracoes['email_pagseguro'];

$url_pagseguro_homolog = $configuracoes['url_pagseguro_homolog'];
$ambiente = $configuracoes['ambiente'];
$url_pagseguro_prod = $configuracoes['url_pagseguro_prod'];
$valor_consulta = number_format($configuracoes['valor_consulta'],2,',','');

?>
<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box-footer bg-gray color-palette">
			
	
		

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<input type='hidden' name='imagem_carta' id='imagem_carta' value="<?php echo $carta_tarot ;?>">
			<input type='hidden' name='id_carta' id='id_carta' value="<?php echo $id_carta ;?>">
			<div class="col-md-3">
			<div class="form-group">
			
				
				<div >
					
                      
                          <?php if(file_exists($carta_tarot)){ ?>

						<img src="<?php echo $carta_tarot;?>" id='carta' alt="" width="200">
					<?php } else{ ?>
						<img src="dist/img/nophoto.jpg" id='avatar_consultor' alt="">
					<?php  } ?>
                       
				</div>
			</div>
			</div>
			
			
			
			<div class="col-md-9">
			<h3 class="box-title">
				Carta : <small>Escreva algo sobre a carta ao lado</small>
			</h3>
			<!-- tools box -->



			<div class="box-body pad">
				<textarea class="textarea" name="texto_carta"
					placeholder="Escreva algo"
					style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
					cols="" rows=""><?php echo $texto_carta ; ?></textarea>

			</div>
					<div class="clearfix"></div>
				<div class="form-group col-xs-3">
				<label>Carta da semana</label>
				<div class="checkbox">
					<label> <input type="checkbox" name="carta_semana"
						id="carta_semana" value="1" <?php echo $carta_semana == '1' ? 'checked' : '' ; ?>> Sim
					</label>
				</div>
				
	
			</div>
			<div class="clearfix"></div>
			<div>
				<button type="submit" class="btn btn-primary">Salvar</button>
			</div>
		
			</div>

		
			
		
		
		</form>
	</div>

</section>
<script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
    
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();



      });
      </script>