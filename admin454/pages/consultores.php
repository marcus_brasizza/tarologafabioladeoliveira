<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<?php
$usuario = json_decode ( $_SESSION ['CM_GRANO_USER'] ['user'] );
if ($_POST == true) {

	$nascimento = $_POST['nascimento_consultor'];
	list($dia,$mes,$ano) = explode('/' , $nascimento);

	$nasc = $ano.'-'.$mes.'-'.$dia;
	$_POST['nascimento_consultor'] = $nasc;
	if($_POST['id_consultor'] != ''){
		$id_consultor = $_POST['id_consultor'];
		$senha = $_POST['senha_consultor'];
		if($senha == ''){
			unset($_POST['senha_consultor']);
		}else{
			$senha = md5($senha);
			$_POST['senha_consultor'] = $senha;
		}

		$banco->update ( 'consultor', $_POST, " id_consultor = {$id_consultor}" );
		unset($_POST['id_consultor']);

	}else{
		unset($_POST['id_consultor']);
	$banco->insert ( 'consultor', $_POST );
	}
	
		if($usuario->nivel_consultor == "A") { 
		echo "<script>document.location.href = 'central.php?action-grano-filter=consultores'</script>";
	}else{
			echo "<script>document.location.href = 'central.php'</script>";
	}
	exit ();
}

if (isset ( $_GET ['status'] )) {
	if ($_GET ['status'] == 0) {
		$status = 0;
	} else {
		$status = 1;
	}
	$bind = array (
			'status_consultor' => $status
	);
	$id_consultor = base64_decode($_GET ['consultor']);
	$banco->update ( 'consultor', $bind, " id_consultor = {$id_consultor}" );

	if($usuario->nivel_consultor == "A") { 
	echo "<script>document.location.href = 'central.php?action-grano-filter=consultores'</script>";
}else{
		echo "<script>document.location.href = 'central.php'</script>";
}
	exit ();
}


if (isset ( $_GET ['edit_consultor'] )) {

	$id_consultor = base64_decode($_GET ['edit_consultor']);
	$bind = array('id_consultor' => $id_consultor);
	$consultorEdit = $banco->select ( 'consultor' , 'id_consultor = :id_consultor' , $bind);
	if($consultorEdit){
		$consultorEdit = $consultorEdit[0];

	}else{
		$consultorEdit = array();
	}

	foreach($consultorEdit as $idx=>$consultor){
		$$idx = $consultor;
	}




}

$pagina_atual = 'consultores';

if($usuario->nivel_consultor == "A") {
	
	$consultores = $banco->select ( 'consultor' ); 


?>

<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Consultores</h3>

			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>

						<th>Nome</th>
						<th>Skype</th>
						<th>Agenda</th>
						<th>Status</th>

					</tr>
					<?php

					if ($consultores) {
						foreach ( $consultores as $consultor ) {

							$status = ($consultor ['status_consultor'] == 1) ? '<span class="label label-success">Atvio</span>' : '<span class="label label-danger">Inativo</span>';
							?>

					<tr>

						<td><a href="?action-grano-filter=consultores&edit_consultor=<?php echo base64_encode($consultor['id_consultor']);?>"><?php echo $consultor['nome_consultor'] . ' ' . $consultor['sobrenome_consultor'] ; ?></a></td>
						<td><?php echo $consultor['skype_consultor'] ;?></td>
						<td><a href="?action-grano-filter=consultor_agenda&consultor=<?php echo base64_encode($consultor['id_consultor']);?>"><i class="fa fa-calendar"></i></a></td>
						<td><a
							href="?action-grano-filter=consultores&amp;consultor=<?php echo base64_encode($consultor['id_consultor']);?>&amp;status=<?php echo ($consultor['status_consultor'] == 1 ? '0':'1') ;?>"><?php echo $status ;?>
						</a></td>
					</tr>

					<?php
						}
					}
					;
					?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<?php } ?>
	<!-- /.box-body -->
	<div class="box-footer bg-gray color-palette">
		<h3 class="box-title">Consultor</h3>

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<!-- text input -->
			<input type='hidden' name='foto_consultor' id='foto_consultor' value="<?php echo $foto_consultor;?>">
			<input		type='hidden' name='id_consultor' id='id_consultor' value="<?php echo $id_consultor;?>">
			<div class="form-group col-xs-5">
				<label>Nome do Consultor</label> <input type="text"
					class="form-control" name='nome_consultor' placeholder="Seu nome" value="<?php echo $nome_consultor;?>">
			</div>
			<div class="form-group col-xs-5">
				<label>Sobrenome do Consultor</label> <input type="text"
					class="form-control" name='sobrenome_consultor'
					placeholder="Sobrenome" value="<?php echo $sobrenome_consultor;?>">
			</div>
			<div class="clearfix"></div>


			<div class="form-group col-xs-5">
				<label>Nascimento:</label> <input type="date" class="form-control"
					name='nascimento_consultor' data-inputmask="'alias': 'dd/mm/yyyy'"
					data-mask  value="<?php echo ($nascimento_consultor) ? date('d/m/Y',strtotime($nascimento_consultor)) : '' ;?>">
			</div>
			<!-- /.form group -->

			<div class="form-group col-xs-5">
				<label for="exampleInputEmail1">Email address</label> <input
					type="email" class="form-control" name='email_consultor'
					id="exampleInputEmail1" placeholder="Enter email" required value="<?php echo $email_consultor;?>">
			</div>
			<div class="clearfix"></div>

			<div class="form-group col-xs-5">
				<label>Skype do Consultor</label> <input type="text"
					class="form-control" name='skype_consultor' placeholder="Skype" value="<?php echo $skype_consultor;?>">
			</div>
			<div class="form-group col-xs-5">
				<label>Senha do Consultor</label> <input type="password"
					class="form-control" name='senha_consultor' <?php echo ($senha_consultor == '' ? 'required' : '' ); ?>>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<label for="exampleInputFile">Foto - <small> Arraste sua imagem aqui</small></label>
				<article>
					<div class="form-group col-xs-5 bg-gray color-palette">
						<div id="holder"></div>
					</div>
					<p id="upload" class="hidden">
						<label>Drag &amp; drop not supported, but you can still upload via
							this input field:<br> <input type="file">
						</label>
					</p>
					<p id="filereader">File API &amp; FileReader API not supported</p>
					<p id="formdata">XHR2's FormData is not supported</p>
					<p id="progress">XHR2's upload progress isn't supported</p>
				</article>
				<div >
					<ul class="users-list clearfix">
                        <li>
                          <?php 
												
													
													if(file_exists("dist/img/".$foto_consultor) && $foto_consultor != NULL ){ ?>

						<img src="dist/img/<?php echo $foto_consultor ; ?>" id='avatar_consultor' alt="">
					<?php } else{ ?>
						<img src="dist/img/nophoto.jpg" id='avatar_consultor' alt="">
					<?php  } ?>
                        </li>
                        </ul>

					</p>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="form-group col-xs-2">
				<label>Sexo</label>
				<div class="radio">
					<label> <input type="radio" name="sexo_consultor"
						id="sexo_consultorM" value="M" <?php echo $sexo_consultor == 'M' ? 'checked' : '' ; ?>> Masculino
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="sexo_consultor"
						id="sexo_consultorF" value="F" <?php echo $sexo_consultor == 'F' ? 'checked' : '' ; ?>> Feminino
					</label>
				</div>

			</div>

			<div class="form-group col-xs-3">
				<label>Status</label>
				<div class="radio">
					<label> <input type="radio" name="status_consultor"
						id="status_consultorA" value="1" <?php echo $status_consultor == '1' ? 'checked' : '' ; ?>> Ativo
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="status_consultor"
						id="status_consultorI" value="0" <?php echo $status_consultor == '0' ? 'checked' : '' ; ?>> Inativo
					</label>
				</div>

			</div>
			
			
			<div class="form-group col-xs-3">
				<label>Efetua atendimento</label>
				<div class="radio">
					<label> <input type="radio" name="atendimento_consultor"
						id="atendimento_consultorA" value="1" <?php echo $atendimento_consultor == '1' ? 'checked' : '' ; ?>> Sim
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="atendimento_consultor"
						id="atendimento_consultorI" value="0" <?php echo $atendimento_consultor == '0' ? 'checked' : '' ; ?>> Não
					</label>
				</div>

			</div>

			<div class="clearfix"></div>

			<h3 class="box-title">
				Bio: <small>Escreva algo sobre você</small>
			</h3>
			<!-- tools box -->



			<div class="box-body pad">
				<textarea class="textarea" name="bio_consultor"
					placeholder="Escreva algo"
					style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
					cols="" rows=""><?php echo $bio_consultor ; ?></textarea>

			</div>
			<div>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</form>
	</div>

</section>
<script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
    	  $("[data-mask]").inputmask();
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();



      });


      var holder = document.getElementById('holder'),
      tests = {
        filereader: typeof FileReader != 'undefined',
        dnd: 'draggable' in document.createElement('span'),
        formdata: !!window.FormData,
        progress: "upload" in new XMLHttpRequest
      },
      support = {
        filereader: document.getElementById('filereader'),
        formdata: document.getElementById('formdata'),
        progress: document.getElementById('progress')
      },
      acceptedTypes = {
        'image/png': true,
        'image/jpeg': true,
        'image/gif': true
      },
      progress = document.getElementById('uploadprogress'),
      fileupload = document.getElementById('upload');

  "filereader formdata progress".split(' ').forEach(function (api) {
    if (tests[api] === false) {
      support[api].className = 'fail';
    } else {
      // FFS. I could have done el.hidden = true, but IE doesn't support
      // hidden, so I tried to create a polyfill that would extend the
      // Element.prototype, but then IE10 doesn't even give me access
      // to the Element object. Brilliant.
      support[api].className = 'hidden';
    }
  });

  function previewfile(file) {

    if (tests.filereader === true && acceptedTypes[file.type] === true) {
      var reader = new FileReader();
      reader.onload = function (event) {
        var image = new Image();
        image.src = event.target.result;
        image.width = 250; // a fake resize
        holder.appendChild(image);
      };

      reader.readAsDataURL(file);
      return true;
    }  else {
      alert('Arquivo não suportado');
      return false;

    }
  }

  function readfiles(files) {
      debugger;
      var formData = tests.formdata ? new FormData() : null;
      for (var i = 0; i < files.length; i++) {
        if (tests.formdata) formData.append('file', files[i]);
          if(!previewfile(files[i])){
            return false;
          }
      }

      // now post a new XHR request
      if (tests.formdata) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                loadEnd((xhr.responseText))
            }
        }
        holder.innerHTML = '';
        xhr.open('POST', 'process/upload_galeria.php?foto_consultor_antiga='+$("#foto_consultor").val());
        xhr.onload = function() {
          console.log("INICIANDO O UPLOAD");
        };
      retorno =   xhr.send(formData);



      }
  }

  function loadEnd(text) {
    holder.innerHTML = '';

  	$("#foto_consultor").val(text);
  	$("#avatar_consultor").attr('src' , 'dist/img/'+text);

  }


  function uploadGaleria(){

   // location.reload();

  }

  if (tests.dnd) {
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files);
    }
  } else {
    fileupload.className = 'hidden';
    fileupload.querySelector('input').onchange = function () {
      readfiles(this.files);
    };
  }


    </script>

