<?php
if ($_POST == true) {
	
	

if($_POST['id_blog'] != ''){
		$id_blog = $_POST['id_blog'];	

		$banco->update ( 'blog', $_POST, " id_blog = {$id_blog}" );
		unset($_POST['id_blog']);
	}else{
		unset($_POST['id_blog']);
	$banco->insert ( 'blog', $_POST );
	}
	echo "<script>document.location.href = '?action-grano-filter=blog'</script>";
	exit ();
}

function nomeConsultor($banco,$cod_consultor){
	
	$consultor =$banco->select('consultor', 'id_consultor='.$cod_consultor);
	$consultor = $consultor[0];
	$nome = $consultor['nome_consultor'].' '.$consultor['sobrenome_consultor'];
	return $nome;
	
}

$usuario = json_decode ( $_SESSION ['CM_GRANO_USER'] ['user'] );
$id_consultor = $usuario->id_consultor;
$editoriais_banco = $banco->select('editorial' , 'status_editorial = 1');
if($editoriais_banco){
	$option_editorial = '';
	foreach($editoriais_banco as $editorial){
		$option_editorial.= "<option>{$editorial['nome_editorial']}</option>\n\t";
	}
	
}


if (isset ( $_GET ['edit_blog'] )) {

	$id_blog = base64_decode($_GET ['edit_blog']);
	
	$bind = array('id_blog' => $id_blog);
	$blogEdit = $banco->select ( 'blog' , 'id_blog = :id_blog' , $bind);
	if($blogEdit){
		$blogEdit = $blogEdit[0];

	}else{
		$blogEdit = array();
	}

	foreach($blogEdit as $idx=>$blog){
		$$idx = $blog;
	}
	$arrEditoriais = explode(',',$editoriais);
	
	$strEditoriais.= '[';
	foreach($arrEditoriais as $edit){
		$strEditoriais.= '"'.$edit.'",';
	}
	$strEditoriais = substr($strEditoriais,0,strlen($strEditoriais)-1);
	$strEditoriais.= ']';
	
	

	?>
	<script>
	($(function(){

		$('.select2').val(<?php echo $strEditoriais;?>);
		
	}))
	
	</script>
	<?php 
}

$pagina_atual = 'Blog';
$blogs = $banco->select ( 'blog' );

?>

<style>
#holder {
	border: 10px dashed #ccc; -
	width: 200px;
	min-height: 200px;
	margin: 20px auto;
}

#holder.hover {
	border: 10px dashed #0c0;
}

#holder img {
	display: block;
	margin: 10px auto;
}

#holder p {
	margin: 10px;
	font-size: 14px;
}
</style>

<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	
	
<link rel="stylesheet" href="plugins/select2/select2.min.css">
 <script src="plugins/select2/select2.full.min.js"></script>

<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Posts</h3>

			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>

						
						<th>Autor</th>
						<th>Editorial</th>
						<th>Titulo</th>
						<th>Lido</th>

					</tr>
					<?php

					if ($blogs) {
						foreach ( $blogs as $blog ) {							
							?>

					<tr>

						<td><a href="?action-grano-filter=blog&edit_blog=<?php echo base64_encode($blog['id_blog']);?>"><?php echo nomeConsultor($banco,$blog['id_consultor']) ; ?></a></td>
						<td><?php echo $blog['editoriais'] ;?></td>
						
						<td><?php echo substr($blog['titulo_blog'],0,30).'...' ;?></td>
						<td><?php echo $blog['views'] ;?></td>
					</tr>

					<?php
						}
					}
					;
					?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer bg-gray color-palette">
		<h3 class="box-title">Consultor</h3>

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<!-- text input -->
			<input type='hidden' name='imagem_blog' id='imagem_blog' value="<?php echo $imagem_blog;?>">
			<input		type='hidden' name='id_blog' id='id_blog' value="<?php echo $id_blog;?>">
			<input		type='hidden' name='id_consultor' id='id_consultor' value="<?php echo $id_consultor;?>">
			<input		type='hidden' name='editoriais' id='editoriais' value="<?php echo $editoriais;?>">
				<div class="form-group col-xs-9">
				<label>Título do post</label> <input type="text"
					class="form-control" name='titulo_blog' placeholder="Título da postagem" value="<?php echo $titulo_blog;?>">
			</div>
			<div class="clearfix"></div>
			<div class="form-group col-xs-5">
				<label>Editoriais pertencente</label> 
				
			<select class="form-control select2"   multiple="multiple" data-placeholder="Editorial pertencente" style="width: 100%;">
                     <?php echo $option_editorial; ?>
                    </select>
				 
			</div>
			
			
			<div class="clearfix"></div>
			<div class="form-group">
				<label for="exampleInputFile">Imagem - <small> Coloque uma imagem no post</small></label>
				<article>
					<div class="form-group col-xs-5 bg-gray color-palette">
						<div id="holder"></div>
					</div>
					<p id="upload" class="hidden">
						<label>Drag &amp; drop not supported, but you can still upload via
							this input field:<br> <input type="file">
						</label>
					</p>
					<p id="filereader">File API &amp; FileReader API not supported</p>
					<p id="formdata">XHR2's FormData is not supported</p>
					<p id="progress">XHR2's upload progress isn't supported</p>
				</article>
				<div >
					<ul class="users-list clearfix">
                        <li>
                          <?php if(file_exists("dist/img/".$imagem_blog)){ ?>

						<img src="dist/img/<?php echo $imagem_blog ; ?>" id='avatar_consultor' alt="">
					<?php } else{ ?>
						<img src="dist/img/nophoto.jpg" id='avatar_consultor' alt="">
					<?php  } ?>
                        </li>
                        </ul>

					</p>
				</div>
			</div>
		
			<div class="clearfix"></div>

			<h3 class="box-title">
				Texto: <small>Escreva um texto sobre o assunto</small>
			</h3>
			<!-- tools box -->



			<div class="box-body pad">
				<textarea class="textarea" name="texto_blog"
					placeholder="Escreva algo"
					style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
					cols="" rows=""><?php echo $texto_blog ; ?></textarea>

			</div>
			<div>
				<button type="button" class="btn btn-primary" onclick='showValues()'>Enviar</button>
			</div>
		</form>
	</div>

</section>
<script type="text/javascript">
					function showValues(){
					
						if($('.select2').select2("val") != null){
							var editoriais = '';
						$.each($('.select2').select2("val"),function(idx,value){
								editoriais = editoriais + value+',';							
	
							});
						alert(editoriais);
						editoriais =  editoriais.substr(0,editoriais.length-1);
						$("#editoriais").val(editoriais);
						}
						$("#meuFormulario").submit();
			
						}
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
   
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();

		$(".select2").select2();

      });


      var holder = document.getElementById('holder'),
      tests = {
        filereader: typeof FileReader != 'undefined',
        dnd: 'draggable' in document.createElement('span'),
        formdata: !!window.FormData,
        progress: "upload" in new XMLHttpRequest
      },
      support = {
        filereader: document.getElementById('filereader'),
        formdata: document.getElementById('formdata'),
        progress: document.getElementById('progress')
      },
      acceptedTypes = {
        'image/png': true,
        'image/jpeg': true,
        'image/gif': true
      },
      progress = document.getElementById('uploadprogress'),
      fileupload = document.getElementById('upload');

  "filereader formdata progress".split(' ').forEach(function (api) {
    if (tests[api] === false) {
      support[api].className = 'fail';
    } else {
      // FFS. I could have done el.hidden = true, but IE doesn't support
      // hidden, so I tried to create a polyfill that would extend the
      // Element.prototype, but then IE10 doesn't even give me access
      // to the Element object. Brilliant.
      support[api].className = 'hidden';
    }
  });

  function previewfile(file) {

    if (tests.filereader === true && acceptedTypes[file.type] === true) {
      var reader = new FileReader();
      reader.onload = function (event) {
        var image = new Image();
        image.src = event.target.result;
        image.width = 250; // a fake resize
        holder.appendChild(image);
      };

      reader.readAsDataURL(file);
      return true;
    }  else {
      alert('Arquivo não suportado');
      return false;

    }
  }

  function readfiles(files) {
      debugger;
      var formData = tests.formdata ? new FormData() : null;
      for (var i = 0; i < files.length; i++) {
        if (tests.formdata) formData.append('file', files[i]);
          if(!previewfile(files[i])){
            return false;
          }
      }

      // now post a new XHR request
      if (tests.formdata) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                loadEnd((xhr.responseText))
            }
        }
        holder.innerHTML = '';
        xhr.open('POST', 'process/upload_galeria.php?foto_consultor_antiga='+$("#foto_consultor").val());
        xhr.onload = function() {
          console.log("INICIANDO O UPLOAD");
        };
      retorno =   xhr.send(formData);



      }
  }

  function loadEnd(text) {
    holder.innerHTML = '';

  	$("#imagem_blog").val(text);
  	$("#avatar_consultor").attr('src' , 'dist/img/'+text);

  }


  function uploadGaleria(){

   // location.reload();

  }

  if (tests.dnd) {
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files);
    }
  } else {
    fileupload.className = 'hidden';
    fileupload.querySelector('input').onchange = function () {
      readfiles(this.files);
    };
  }


    </script>

