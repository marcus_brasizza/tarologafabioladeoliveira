
<?php

$usuario = json_decode($_SESSION["CM_GRANO_USER"]["user"]);


$pagina_atual = 'gerenciamento';

$clientes = $banco->select('cliente' , ' status_cliente = 1 ');

$totalClientes = count($clientes);


$ultimosClientes = $banco->select('cliente' , ' status_cliente = 1  order by id_cliente DESC limit 10');
$totalUltimos = count($ultimosClientes);


if($usuario->nivel_consultor != "A"){

	$whereAgenda = ' id_consultor = ' . $usuario->id_consultor;
	
	
	$sql = "select  count(*) total
  from agendamento a  join agendamento_cliente ac on ac.id_agendamento = a.id_agendamento 
	join cliente c on c.id_cliente = ac.id_cliente  where {$whereAgenda} and finalizado_agendamento = 0 order by  fim_agendamento ASC";
	
	$agendamento_cliente = $db->run($sql  );
	$totalConsultas = $agendamento_cliente[0]['total'];

}else{

	$pagamentos = $db->select ( 'pagamento', ' status_pagamento is not null order by data_pagamento DESC limit 100');

	if ($pagamentos [0]) {
	    foreach ( $pagamentos as $pagamento ) {
	        $status_pagamento = $db->select('status_pagamento' , 'id_status = '.$pagamento['status_pagamento']);
	        $status_pagamento = $status_pagamento[0];
	        if($status_pagamento['nome_status']){
	            $strStatus = $status_pagamento['nome_status'];
	        }else{
	            $strStatus = $pagamento['status_pagamento'];
	        }
					$forma = $db->select('formas_pagamento', 'id_forma = '.$pagamento ['id_forma']);
					$forma = $forma[0];
	        $infos [strtotime ( $pagamento ['data_pagamento'] )] [] = array (
	            'id_info' => $pagamento ['id_pagamento'],
	            'data' => date ( 'd/m/Y H:i:s', strtotime ( $pagamento ['data_pagamento'] ) ),
	            'info' => $pagamento ['item_pagamento'],
	            'status' => ($pagamento ['status_pagamento']) ?  $pagamento ['status_pagamento'] : 'x',
	            'str_status' => ($pagamento ['status_pagamento']) ?  $strStatus : '',
	            'forma_pagamento' => $forma ['nome_forma'],
	            'transaction_id' => $pagamento['transacao_pagamento'],
	            'tipo' => 'C'
	        );
	    }
	}
	if($infos){
	ksort($infos);
	$infos = array_reverse($infos);
}
}


$agendamento = $banco->select('agendamento' , $whereAgenda);

if($agendamento[0]){
	$total_agendada = 0 ;
	$total_realizadas= 0;
	foreach($agendamento as $agenda){
		
		if($agenda['livre_agendamento'] == null){
			$total_agendada+=1;
		}else{
			$total_realizadas+=1;
		}
	}
}


$pagamentos= $banco->select('pagamento');
if($pagamentos[0]){	
	foreach($pagamentos as $pagamento){
			switch($pagamento['status_pagamento']){					
				case 1: // aguardando:
					case 2: // aguardando:
					$pagto['aguardo']+=$pagamento['valor_pagamento'];
					break;
				case 3:
					$pagto['convertido'] += $pagamento['valor_pagamento'];
					break;
				default:
					$pagto['cancelado']+=$pagamento['valor_pagamento'];
					break;
			}	
	}
}

?>
<h1>
            Framework Grano
            <small>Version 2.0</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
            <li class="active"><?php echo $pagina_atual;?></li>
          </ol>



        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">



            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
<?php if($usuario->nivel_consultor == "A") {?>
            	<div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Créditos convertidos</span>
                  <span class="info-box-number">R$ <?php echo number_format($pagto['convertido'],2,',','.');?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div>
            
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Créditos cancelados</span>
                  <span class="info-box-number">R$ <?php echo number_format($pagto['cancelado'],2,',','.');?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div>	
            
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Créditos em liberação</span>
                  <span class="info-box-number">R$ <?php echo number_format($pagto['aguardo'],2,',','.');?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div>	
            <div class='clearfix'></div>
            <?php }else{ ?>
						
						
						<div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
								<a
								href="?action-grano-filter=consultas&consultor=<?php echo base64_encode($usuario->id_consultor);?>">
								<span class="info-box-icon bg-red"><i class="fa fa-skype"></i></span>
								<div class="info-box-content">
                  <span class="info-box-text">Consultas pendentes</span>
									  <span class="info-box-number"><?php echo $totalConsultas; ?></span>
										</a>
          </div>
              
              </div><!-- /.info-box -->
            </div>	
						<?php } ?>
            <div class="col-md-4 col-sm-6 col-xs-12">            
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-skype"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Datas agendadas</span>
                  <span class="info-box-number"><?php echo $total_agendada;?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <div class="col-md-4 col-sm-6 col-xs-12">            
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-skype"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Consultas realizadas</span>
                  <span class="info-box-number"><?php echo $total_realizadas; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Usuarios no site</span>
                  <span class="info-box-number"><?php echo $totalClientes;?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

         

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->

             <?php if ($ultimosClientes > 0 ) { ?>
              <div class="row">
                <div class="col-md-12">
                  <!-- USERS LIST -->
                  <div class="box box-danger">
                    <div class="box-header with-border">
                      <h3 class="box-title">Últimos cadastros</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">

                      <?php foreach($ultimosClientes as $clientes) {

                          if($clientes['facebookid'] != ''){
                              if(file_exists('dist/img/'.$clientes['foto_cliente'])){
                                 $imagemCli = 'dist/img/'.$clientes['foto_cliente'];
                              }else{
                                 
                            $imagemCli = $clientes['foto_cliente'];
                              }
                          } // Imagem do face
                          else{

                            if($clientes['foto_cliente'] == ''){
                              $imagemCli = 'dist/img/nophoto.jpg';
                            }else{

                            if(file_exists('dist/img/'.$clientes['foto_cliente'])){
                              $imagemCli = 'dist/img/'.$clientes['foto_cliente'];
                            }else{
                              $imagemCli = 'dist/img/nophoto.jpg';
                            }
                          }
                          }
                          list($data,$hora) = explode(' ',$clientes['data_cadastro']);
                          list($ano,$mes,$dia) = explode('-', $data);
                          $data_cadastro = $dia.'/'.$mes.'/'.$ano;
                         ?>

                        <li>
                          <img src="<?php echo $imagemCli;?>" >
                          <a class="users-list-name" href="#"><?php echo $clientes['nome_cliente'];?></a>
                          <span class="users-list-date"><?php echo  $data_cadastro;?></span>
                        </li>

                      <?php } ;?>
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                  </div><!--/.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
              <?php } ?>
              <!-- TABLE: LATEST ORDERS -->
							<?php if($usuario->nivel_consultor == "A") {?>
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Últimos Pagamentos</h3>

                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Data</th>
                          <th>Descrição</th>
                          <th>Transação</th>      
													<th>Forma de pagamento</th>  
													<th>Status</th>              
                        </tr>
                      </thead>
                      <tbody>
												<?php 
												
												if($infos){
												foreach ($infos as $info ){
														foreach($info as $extrato){
													
															$classTR = '';
															$titleTR = '';
															switch($extrato['status']){

																case 0:
																	if($extrato['tipo'] == 'C'){
																		$classTR = 'class="alert alert-danger"';
																		$titleTR = 'title="Transação abandonada"';
																		$status_str = '<span class="label label-warning">'.$extrato['str_status'].'</span>';

																	}else{
																		$status_str = '<span class="label alert-info">Aguardando a consulta</span>';
																	}
																	break;

																case 'x':
																	$classTR = 'class="alert alert-danger"';
																	$titleTR = 'title="Transação abandonada"';
																	$status_str = '<span class="label label-success">'.$extrato['status'].'</span>';
																	break;
																case 1:
																		if($extrato['tipo'] == 'C'){
																			$status_str = '<span class="label label-warning">'.$extrato['str_status'].'</span>';
																			$titleTR = 'title="'.$extrato['transaction_id'].'"';

																		}else{
																			if($extrato['nota_agendamento'] >= 0 && $extrato['nota_agendamento'] <= 3 ){
																				$img_estrela = '0.png';
																			}elseif($extrato['nota_agendamento'] > 3 && $extrato['nota_agendamento'] <= 6){

																				$img_estrela = '1.png';
																			}

																			elseif($extrato['nota_agendamento'] > 6 && $extrato['nota_agendamento'] <= 8){

																				$img_estrela = '2.png';
																			}else{
																				$img_estrela = '3.png';
																			}

																			$status_str  = '<span class="label label-success"> Finalizada</span>';

																		}
																		break;

																		case 3:
																			if($extrato['tipo'] == 'C'){
																				$titleTR = 'title="'.$extrato['transaction_id'].'"';
																				$status_str = '<span class="label label-success">'.$extrato['str_status'].'</span>';

																			}
																	break;

																		default:
																			if($extrato['tipo'] == 'C'){
																				$titleTR = 'title="'.$extrato['transaction_id'].'"';
																				$status_str = '<span class="label label-danger">'.$extrato['str_status'].'</span>';

																			}
																			break;

															}
												?>
												<tr>
													<td nowrap><?php echo $extrato['data'] ;?></td>
													<td><?php echo $extrato['info'] ;?></td>
													<td><?php echo $extrato['transaction_id'];?></td>
														<td><?php echo  utf8_encode( $extrato['forma_pagamento']);?></td>
															<td><?php echo  utf8_encode( $status_str);?></td>
														
												</tr>
												<?
											}}}?>
                      
                        
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <!-- <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                </div><!-- /.box-footer --> 
              </div><!-- /.box -->
							<?php  } ?>


          </div><!-- /.row -->
        </div>
      </section>
          <!-- jvectormap -->


	<!-- AdminLTE for demo purposes -->
