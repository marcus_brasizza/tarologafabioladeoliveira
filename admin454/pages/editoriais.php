
<?php
if ($_POST == true) {

	
	if($_POST['id_editorial'] != ''){
		$id_editorial = $_POST['id_editorial'];		

		$banco->update ( 'editorial', $_POST, " id_editorial = {$id_editorial}" );
		unset($_POST['id_editorial']);
	}else{
		unset($_POST['id_editorial']);
	$banco->insert ( 'editorial', $_POST );
	}
	echo "<script>document.location.href = '?action-grano-filter=editoriais'</script>";
	exit ();
}

if (isset ( $_GET ['status'] )) {
	if ($_GET ['status'] == 0) {
		$status = 0;
	} else {
		$status = 1;
	}
	$bind = array (
			'status_editorial' => $status
	);
	$id_editorial = base64_decode($_GET ['id_editorial']);
	$banco->update ( 'editorial', $bind, " id_editorial = {$id_editorial}" );
	echo "<script>document.location.href = '?action-grano-filter=editoriais'</script>";
	exit ();
}


if (isset ( $_GET ['edit_editorial'] )) {

	$id_editorial = base64_decode($_GET ['edit_editorial']);
	$bind = array('id_editorial' => $id_editorial);
	$consultorEdit = $banco->select ( 'editorial' , 'id_editorial = :id_editorial' , $bind);
	if($consultorEdit){
		$consultorEdit = $consultorEdit[0];

	}else{
		$consultorEdit = array();
	}

	foreach($consultorEdit as $idx=>$consultor){
		$$idx = $consultor;
	}




}

$pagina_atual = 'editoriais';
$editoriais = $banco->select ( 'editorial' );

?>

<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Editoriais</h3>

			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>

						<th>Nome</th>						
						<th>Status</th>

					</tr>
					<?php

					if ($editoriais) {
						foreach ( $editoriais as $editorial ) {

							$status = ($editorial ['status_editorial'] == 1) ? '<span class="label label-success">Atvio</span>' : '<span class="label label-danger">Inativo</span>';
							?>

					<tr>

						<td><a href="?action-grano-filter=editoriais&edit_editorial=<?php echo base64_encode($editorial['id_editorial']);?>"><?php echo $editorial['nome_editorial']; ?></a></td>						
						<td><a
							href="?action-grano-filter=editoriais&amp;id_editorial=<?php echo base64_encode($editorial['id_editorial']);?>&amp;status=<?php echo ($editorial['status_editorial'] == 1 ? '0':'1') ;?>"><?php echo $status ;?>
						</a></td>
					</tr>

					<?php
						}
					}
					;
					?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer bg-gray color-palette">
		<h3 class="box-title">Editorial</h3>

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<!-- text input -->
			
			<input		type='hidden' name='id_editorial' id=''id_editorial'' value="<?php echo $id_editorial;?>">
			<div class="form-group col-xs-5">
				<label>Nome do editorial</label> <input type="text"
					class="form-control" name='nome_editorial' placeholder="Nome do editorial" value="<?php echo $nome_editorial;?>">
			</div>
			


			<div class="clearfix"></div>

			

			<div class="form-group col-xs-3">
				<label>Status</label>
				<div class="radio">
					<label> <input type="radio" name="status_editorial"
						id="status_consultorA" value="1" <?php echo $status_editorial == '1' ? 'checked' : '' ; ?>> Ativo
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="status_editorial"
						id="status_consultorI" value="0" <?php echo $status_editorial == '0' ? 'checked' : '' ; ?>> Inativo
					</label>
				</div>

			</div>
			
			
			

			<div class="clearfix"></div>

			
			<div>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</form>
	</div>

</section>

