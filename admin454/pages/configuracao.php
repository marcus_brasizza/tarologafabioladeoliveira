<?php
if ($_POST == true) {
	
	$token_pagseguro  = filter_input(INPUT_POST, 'token_pagseguro', FILTER_SANITIZE_SPECIAL_CHARS);
	$email_pagseguro  = filter_input(INPUT_POST, 'email_pagseguro', FILTER_SANITIZE_SPECIAL_CHARS);
	$valor_consulta  = filter_input(INPUT_POST, 'valor_consulta', FILTER_SANITIZE_SPECIAL_CHARS);
	$logotipo_cliente  = filter_input(INPUT_POST, 'logotipo_cliente', FILTER_SANITIZE_SPECIAL_CHARS);
	
	$url_pagseguro_homolog  = filter_input(INPUT_POST, 'url_pagseguro_homolog', FILTER_SANITIZE_SPECIAL_CHARS);
	
	$url_pagseguro_prod  = filter_input(INPUT_POST, 'url_pagseguro_prod', FILTER_SANITIZE_SPECIAL_CHARS);
	
	$ambiente  = filter_input(INPUT_POST, 'ambiente', FILTER_SANITIZE_SPECIAL_CHARS);
	
	$telefone_contato  = filter_input(INPUT_POST, 'telefone_contato', FILTER_SANITIZE_SPECIAL_CHARS);
	
	$valor_consulta = str_replace(array('R$','_', ' '),'' , $valor_consulta);
	$valor_consulta = str_replace(',','.',$valor_consulta);
	
	foreach($_POST as $idx => $valores){
		
		$arrayConfig[$idx] = $$idx;
	}
	$sql = "Truncate configuracoes";
	$banco->run($sql);
	$banco->insert ( 'configuracoes', $arrayConfig );
	
	echo "<script>document.location.href = 'central.php?action-grano-filter=configuracao'</script>";
	exit ();
}





$pagina_atual = 'configurações';
$configuracoes = $banco->select ( 'configuracoes' );
$configuracoes= $configuracoes[0];

$logotipo_cliente = $configuracoes['logotipo_cliente'];
$token_pagseguro = $configuracoes['token_pagseguro'];
$email_pagseguro = $configuracoes['email_pagseguro'];

$telefone_contato = $configuracoes['telefone_contato'];

$url_pagseguro_homolog = $configuracoes['url_pagseguro_homolog'];
$ambiente = $configuracoes['ambiente'];
$url_pagseguro_prod = $configuracoes['url_pagseguro_prod'];
$valor_consulta = number_format($configuracoes['valor_consulta'],2,',','');

?>
<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box-footer bg-gray color-palette">
			
	
		<h3 class="box-title">Configurações</h3>

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<!-- text input -->
			<input type='hidden' name='logotipo_cliente' id='logotipo_cliente' value="<?php echo $logotipo_cliente;?>" required>
			<input type="hidden"	 name='url_pagseguro_homolog' id='url_pagseguro_homolog' value="<?php echo $url_pagseguro_homolog;?>">
			<input type="hidden"	 name='url_pagseguro_prod'  id='url_pagseguro_prod'  value="<?php echo $url_pagseguro_prod;?>">
			<input type="hidden"	 name='ambiente' id='ambiente' value="<?php echo $ambiente;?>">
			
			<div class="form-group col-xs-5">
				<label>Token pagseguro:</label> <input type="text"
					class="form-control" name='token_pagseguro' placeholder="Token" value="<?php echo $token_pagseguro;?>" required>
			</div>
			
			<div class="clearfix"></div>
			<div class="form-group col-xs-5">
				<label>E-mail pagseguro</label> <input type="email"
					class="form-control" name='email_pagseguro'
					placeholder="E-mail do pagseguro" value="<?php echo $email_pagseguro;?>" required>
			</div>
			<div class="clearfix"></div>

		
		<div class="form-group col-xs-5">
				<label>Telefone para contato</label> <input type="text"
					class="form-control" name='telefone_contato'
					placeholder="Telefone para contato" value="<?php echo $telefone_contato;?>" required>
			</div>
			<div class="clearfix"></div>
		

			<div class="form-group col-xs-5">
				<label for="exampleInputEmail1">Valor fixo da consulta</label> <input
					type="text" class="form-control" name='valor_consulta'
					id="valor_consulta"   value="<?php echo $valor_consulta;?>" required>
			</div>
			
			<div class="clearfix"></div>
			<div class="form-group">
				<label for="exampleInputFile">Logotipo da empresa - <small> Arraste sua imagem aqui</small></label>
				<article>
					<div class="form-group col-xs-5 bg-gray color-palette">
						<div id="holder"></div>
					</div>
					<p id="upload" class="hidden">
						<label>Drag &amp; drop not supported, but you can still upload via
							this input field:<br> <input type="file">
						</label>
					</p>
					<p id="filereader">File API &amp; FileReader API not supported</p>
					<p id="formdata">XHR2's FormData is not supported</p>
					<p id="progress">XHR2's upload progress isn't supported</p>
				</article>
				<div >
					<ul class="users-list clearfix">
                        <li>
                          <?php if(file_exists("dist/img/".$logotipo_cliente)){ ?>

						<img src="dist/img/<?php echo $logotipo_cliente ; ?>" id='avatar_consultor' alt="">
					<?php } else{ ?>
						<img src="dist/img/nophoto.jpg" id='avatar_consultor' alt="">
					<?php  } ?>
                        </li>
                        </ul>

					</p>
				</div>
			</div>
			<div class="clearfix"></div>

			
		
			<div>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</form>
	</div>

</section>
<script type="text/javascript">   


      var holder = document.getElementById('holder'),
      tests = {
        filereader: typeof FileReader != 'undefined',
        dnd: 'draggable' in document.createElement('span'),
        formdata: !!window.FormData,
        progress: "upload" in new XMLHttpRequest
      },
      support = {
        filereader: document.getElementById('filereader'),
        formdata: document.getElementById('formdata'),
        progress: document.getElementById('progress')
      },
      acceptedTypes = {
        'image/png': true,
        'image/jpeg': true,
        'image/gif': true
      },
      progress = document.getElementById('uploadprogress'),
      fileupload = document.getElementById('upload');

  "filereader formdata progress".split(' ').forEach(function (api) {
    if (tests[api] === false) {
      support[api].className = 'fail';
    } else {
      // FFS. I could have done el.hidden = true, but IE doesn't support
      // hidden, so I tried to create a polyfill that would extend the
      // Element.prototype, but then IE10 doesn't even give me access
      // to the Element object. Brilliant.
      support[api].className = 'hidden';
    }
  });

  function previewfile(file) {

    if (tests.filereader === true && acceptedTypes[file.type] === true) {
      var reader = new FileReader();
      reader.onload = function (event) {
        var image = new Image();
        image.src = event.target.result;
        image.width = 250; // a fake resize
        holder.appendChild(image);
      };

      reader.readAsDataURL(file);
      return true;
    }  else {
      alert('Arquivo não suportado');
      return false;

    }
  }

  function readfiles(files) {
      debugger;
      var formData = tests.formdata ? new FormData() : null;
      for (var i = 0; i < files.length; i++) {
        if (tests.formdata) formData.append('file', files[i]);
          if(!previewfile(files[i])){
            return false;
          }
      }

      // now post a new XHR request
      if (tests.formdata) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                loadEnd((xhr.responseText))
            }
        }
        holder.innerHTML = '';
        xhr.open('POST', 'process/upload_galeria.php?foto_consultor_antiga='+$("#logotipo_cliente").val());
        xhr.onload = function() {
          console.log("INICIANDO O UPLOAD");
        };
      retorno =   xhr.send(formData);



      }
  }

  function loadEnd(text) {
    holder.innerHTML = '';

  	$("#logotipo_cliente").val(text);
  	$("#avatar_consultor").attr('src' , 'dist/img/'+text);

  }



  if (tests.dnd) {
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files);
    }
  } else {
    fileupload.className = 'hidden';
    fileupload.querySelector('input').onchange = function () {
      readfiles(this.files);
    };
  }

  ($(function(){

	  $("#valor_consulta").inputmask('R$ 999,99' , { numericInput: true });
	  }))

    </script>

