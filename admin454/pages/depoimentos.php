<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script
	src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<?php
$usuario = json_decode ( $_SESSION ['CM_GRANO_USER'] ['user'] );
if ($_POST == true) {

	
	if($_POST['id_depoimento'] != ''){
		$id_depoimento = $_POST['id_depoimento'];
		

		$banco->update ( 'depoimentos_clientes', $_POST, " id_depoimento = {$id_depoimento}" );
		unset($_POST['id_consultor']);

	}else{
		unset($_POST['id_depoimento']);
	$banco->insert ( 'depoimentos_clientes', $_POST );
	}
	
	
		echo "<script>document.location.href = 'central.php?action-grano-filter=depoimentos'</script>";

	exit ();
}

if (isset ( $_GET ['status'] )) {
	if ($_GET ['status'] == 0) {
		$status = 0;
	} else {
		$status = 1;
	}
	$bind = array (
			'status_depoimento' => $status
	);
	$id_depoimento = base64_decode($_GET ['depoimento']);
	$banco->update ( 'depoimentos_clientes', $bind, " id_depoimento = {$id_depoimento}" );
	

	echo "<script>document.location.href = 'central.php?action-grano-filter=depoimentos'</script>";

	exit ();
}


if (isset ( $_GET ['edit_depoimento'] )) {

	$id_depoimento = base64_decode($_GET ['edit_depoimento']);
	$bind = array('id_depoimento' => $id_depoimento);
	$depoimentoEdit = $banco->select ( 'depoimentos_clientes' , 'id_depoimento = :id_depoimento' , $bind);

	if($depoimentoEdit){
		$depoimentoEdit = $depoimentoEdit[0];

	}else{
		$depoimentoEdit = array();
	}

	foreach($depoimentoEdit as $idx=>$depoimento){
		$$idx = $depoimento;
	}




}

$pagina_atual = 'depoimentos';


	
	$depoimentos = $banco->select ( 'depoimentos_clientes' , '1 = 1 order by data_depoimento DESC' ); 




?>

<h1>
	Framework Grano <small>Version 2.0</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Inicial</a></li>
	<li class="active"><?php echo $pagina_atual;?></li>
</ol>


<!-- Main content -->
<section class="content">


	<div class="box-body">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Consultores</h3>

			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>

						<th>Depoimento</th>
						
						<th>Status</th>

					</tr>
					<?php

					if ($depoimentos) {
						foreach ( $depoimentos as $depoimento ) {

							$status = ($depoimento ['status_depoimento'] == 1) ? '<span class="label label-success">Ativo</span>' : '<span class="label label-danger">Inativo</span>';
							?>

					<tr>

						<td><a href="?action-grano-filter=depoimentos&edit_depoimento=<?php echo base64_encode($depoimento['id_depoimento']);?>"><?php echo  substr($depoimento['texto_depoimento'] ,0,100); ?></a></td>
						
						<td><a
							href="?action-grano-filter=depoimentos&amp;depoimento=<?php echo base64_encode($depoimento['id_depoimento']);?>&amp;status=<?php echo ($depoimento['status_depoimento'] == 1 ? '0':'1') ;?>"><?php echo $status ;?>
						</a></td>
					</tr>

					<?php

					}
					;
					?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<?php } ?>
	<!-- /.box-body -->
	<div class="box-footer bg-gray color-palette">
	

		<form data-toggle="validator" role="form" id='meuFormulario'method="POST" action="">
			<!-- text input -->
		
			<input		type='hidden' name='id_depoimento' id='id_depoimento' value="<?php echo $id_depoimento;?>">
			
			<div class="clearfix"></div>

			<h3 class="box-title">
			Depoimento: 
			</h3>
			<!-- tools box -->



			<div class="box-body pad">
				<textarea class="textarea" name="texto_depoimento"
					placeholder="Escreva algo"
					style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
					cols="" rows=""><?php echo $texto_depoimento ; ?></textarea>

			</div>
			<div>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</form>
	</div>

</section>
<script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
    	  $("[data-mask]").inputmask();
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();



      });
    </script>