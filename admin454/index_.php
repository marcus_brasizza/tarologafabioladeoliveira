<?php
@session_start();

$msg = '';
if($_POST){

	include_once('../conf/config.php');

	$usuario  = filter_input(INPUT_POST, 'user-cmaster', FILTER_DEFAULT);
	$senha  = md5(filter_input(INPUT_POST, 'pass-cmaster', FILTER_DEFAULT));

	$bind = array(
			":usuario" => $usuario,
			":senha" =>($senha)
	);
	$dados = $db->select("consultor", "email_consultor =  :usuario and senha_consultor = :senha", $bind);
	$dados = $dados[0];
	if($dados ){
		
		$_SESSION['CM_GRANO_USER']['user'] = json_encode($dados);
		
		echo "<pre>";
		print_r($_SESSION['CM_GRANO_USER']['user']);
	
		header('location:central.php');
		exit;

	}else{

		$msg = "<span class='label label-important'>Usuário ou senha inválidos.</span>";

	}

}else{
	session_destroy();
}

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Grano Administrativo</title>

	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="bootstrap/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="bootstrap/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

			<style type="text/css">
			body { background: url(img/bg-login.jpg) !important; }
		</style>



</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">

			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<a href="../"><i class="halflings-icon home"></i></a>
					<!--	<a href="#"><i class="halflings-icon cog"></i></a> //-->
					</div>
					<h2>Faça o seu login</h2>
					<form class="form-horizontal" action="index.php" method="post">
						<fieldset>

							<div class="input-prepend" title="Usuário">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="user-cmaster" id="user-cmaster" type="text" placeholder="email"  autocomplete="off"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Senha">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="pass-cmaster" id="pass-cmaster" type="password" placeholder="senha"  autocomplete="off"/>
							</div>
							<div class="clearfix"></div>


							<div class="button-login">
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
							<div class="clearfix"></div>

							 <?php echo $msg;?>
					</form>
					<hr>

				</div><!--/span-->
			</div><!--/row-->


	</div><!--/.fluid-container-->

		</div><!--/fluid-row-->

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>


		<script src="js/retina.js"></script>


	<!-- end: JavaScript-->

</body>
</html>
