<?php
session_start();
include_once ('../conf/config.php');
if (! isset($_SESSION['CM_GRANO_USER']['user'])) {
    header('location:index.php');
    exit();
} else {
    
    $usuario = json_decode($_SESSION['CM_GRANO_USER']['user']);
    $dados = $db->select("consultor", "id_consultor = " . $usuario->id_consultor);
    $dados = $dados[0];
    
    if ($dados) {
        
        $_SESSION['CM_GRANO_USER']['user'] = json_encode($dados);
    }
}

$usuario = json_decode($_SESSION['CM_GRANO_USER']['user']);

$dirIMG = 'dist/img/';
if (! file_exists($dirIMG . $usuario->foto_consultor)) {
    $imagemCliente = $dirIMG . 'avatar5.png';
} else {
    $imagemCliente = $dirIMG . $usuario->foto_consultor;
}
?>
<!DOCTYPE html>
<html>
<head>
<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Sistema Administrativo GRANO</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
#holder {
	border: 10px dashed #ccc; -
	width: 400px;
	min-height: 400px;
	margin: 20px auto;
}

#holder.hover {
	border: 10px dashed #0c0;
}

#holder img {
	display: block;
	margin: 10px auto;
}

#holder p {
	margin: 10px;
	font-size: 14px;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">

			<!-- Logo -->
			<a href="central.php" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>GRANO </b></span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Grano</b> Framework</span>
			</a>

			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only"></span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<!-- <li class="dropdown messages-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="fa fa-envelope-o"></i> <span class="label label-success">99</span>
						</a>
							<ul class="dropdown-menu">
								<li class="header">You have 4 messages</li>
								<li>
								
									<ul class="menu">

										<li><a href="#">
												<div class="pull-left">
													<img src="dist/img/user4-128x128.jpg" class="img-circle"
														alt="User Image">
												</div>
												<h4>
													Reviewers <small><i class="fa fa-clock-o"></i> 2 days</small>
												</h4>
												<p>Why not buy a new awesome theme?</p>
										</a></li>
									</ul>
								</li>

							</ul></li> -->
						<!-- Notifications: style can be found in dropdown.less -->
						<!-- <li class="dropdown notifications-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="fa fa-bell-o"></i> <span class="label label-warning">99</span>
						</a>
							<ul class="dropdown-menu">
								<li class="header">You have 10 notifications</li>
								<li>
								
									<ul class="menu">



										<li><a href="#"> <i class="fa fa-shopping-cart text-green"></i>
												O CLIENTE X agendou um horario com voce
										</a></li>

									</ul>
								</li>
							</ul></li> -->

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <img
								src="<?php echo $imagemCliente ; ?>" class="user-image"
								alt="User Image"> <span class="hidden-xs"><?php echo  $usuario->nome_consultor. ' '. $usuario->sobrenome_consultor;?></span>
						</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header"><img
									src="<?php echo $imagemCliente ; ?>" class="img-circle"
									alt="User Image">
									<p>
                      <?php $usuario->nome_consultor. ' '. $usuario->sobrenome_consultor;?>
                      <small>Skype: </small><strong><?php echo $usuario->skype_consultor ; ?></strong>

									</p></li>
								<!-- Menu Body -->
								<li class="user-body">

									<div class="col-xs-7 text-center">
										<a
											href="?action-grano-filter=consultas&amp;id_consultor=<?php echo base64_encode($usuario->id_consultor);?>">Minhas
											consultas</a>
									</div>

								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a
											href="?action-grano-filter=consultores&amp;edit_consultor=<?php echo base64_encode($usuario->id_consultor);?>"
											class="btn btn-default btn-flat">Meu perfil</a>
									</div>
									<div class="pull-right">
										<a href="index.php?logout=true"
											class="btn btn-default btn-flat">Sair</a>
									</div>
								</li>
							</ul></li>
						<!-- Control Sidebar Toggle Button -->
						<!-- <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li> -->
					</ul>
				</div>

			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?php echo $imagemCliente ; ?>" class="img-circle"
							alt="User Image">
					</div>
					<div class="pull-left info">
						<p><?php echo $usuario->nome_consultor . ' ' .$usuario->sobrenome_consultor ;?> </p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>

				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Navegação principal</li>

          	<?php if($usuario->nivel_consultor == "A") { ?>
          	
          		
          	<li class="treeview"><a
						href="?action-grano-filter=configuracao"> <i class="fa fa-gear"></i>
							<span>Configurações</span>
					</a></li>

					<li class="treeview"><a href="?action-grano-filter=consultores"> <i
							class="fa fa-skype"></i> <span>Cadastro de consultores</span>
					</a></li>

					<li class="treeview"><a href="#"> <i class="fa fa-share"></i> <span>Gerenciais</span>
							<i class="fa fa-angle-left pull-right"></i>
					</a>
						<ul class="treeview-menu">
							<li><a href="javascript:void(0)">Gerenciar Blog <i
									class="fa fa-angle-left pull-right"></i></a>

								<ul class="treeview-menu">
									<li><a href="?action-grano-filter=blog"><i
											class="fa fa-language"></i>Blog</a></li>
									<li><a href="?action-grano-filter=editoriais"><i
											class="fa fa-commenting-o"></i>Gerenciar editoriais</a></li>
								</ul></li>

						<li><a href="?action-grano-filter=eventos"><i
									class="fa fa-calendar-times-o"></i>Eventos</a></li>
									
						<li><a href="?action-grano-filter=depoimentos"><i
									class="fa fa-commenting"></i>Depoimentos do site</a></li>				
							<li><a href="?action-grano-filter=texto_home"><i
									class="fa fa-home"></i>Texto da página inicial</a></li>
							<li><a href="?action-grano-filter=texto_sobre"><i
									class="fa fa-hand-lizard-o"></i>Texto da página 'Sobre'</a></li>

							<li><a href="?action-grano-filter=cartas_tarot"><i
									class="fa fa-heart-o"></i>Cartas de tarot</a></li>
						</ul></li>
           <?php }else{ ?>
           	
           	<li class="treeview"><a
						href="?action-grano-filter=consultor_agenda&consultor=<?php echo base64_encode($usuario->id_consultor);?>">
							<i class="fa fa-skype"></i> <span>Minha agenda</span>
					</a></li>
          <?php
        }
        
        ?>
          

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
          <?php

        function checkPage($pagina, $db)
        {
            if (file_exists('pages/' . $pagina . '.php')) {
                
                $banco = $db;
                require_once ('pages/' . $pagina . '.php');
            } else {
                require_once ('pages/erro404.php');
            }
        }
        $granoAction = filter_input(INPUT_GET, 'action-grano-filter', FILTER_SANITIZE_SPECIAL_CHARS);
        
        if ($granoAction) {
            switch ($granoAction) {
                case 'consultores':
                    checkPage('consultores', $db);
                    break;
                
                case 'configuracao':
                    checkPage('configuracao', $db);
                    break;
                
                case 'consultas':
                    checkPage('consultas', $db);
                    break;
                
                case 'consultor_agenda':
                    checkPage('consultor_agenda', $db);
                    break;
                case 'cartas_tarot':
                    checkPage('cartas_tarot', $db);
                    break;
                case 'carta_edicao':
                    checkPage('carta_edicao', $db);
                    break;
                case 'blog':
                    checkPage('blog', $db);
                    break;
                case 'editoriais':
                    checkPage('editoriais', $db);
                    break;
                case 'texto_home':
                    checkPage('texto_home', $db);
                    break;                
                case 'texto_sobre':
                    checkPage('texto_sobre', $db);
                    break;
                    
                    case 'eventos':
                        checkPage('eventos', $db);
                        break;

                          case 'depoimentos':
                        checkPage('depoimentos', $db);
                        break;
                        
                        
                default:
                    require_once ('pages/erro404.php');
                    break;
            }
        } else {
            checkPage('central_meio', $db);
        }
        ?>
          
        </section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.0.0
			</div>
			<strong>Copyright &copy; 2016 <a
				href="http://www.granoestudio.com.br/" target="_blank">Grano Studio</a>.
			</strong>
		</footer>




	</div>
	</aside>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>

	</div>
	<!-- ./wrapper -->


	<script src="plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<!-- SlimScroll 1.3.0 -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="dist/js/demo.js"></script>

	<script src="plugins/input-mask/jquery.inputmask.js"></script>
	<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>


	<script>
	localStorage.removeItem('usuario');

	localStorage.setItem('usuario', '<?php echo  $_SESSION ['CM_GRANO_USER'] ['user'];?>');

	</script>
</body>
</html>
