/* ===================================================
 * bootstrap-transition.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#transitions
 * ===================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


  /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
   * ======================================================= */

  $(function () {

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }

    })()

  })

}(window.jQuery);/* ==========================================================
 * bootstrap-alert.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#alerts
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* ALERT CLASS DEFINITION
  * ====================== */

  var dismiss = '[data-dismiss="alert"]'
    , Alert = function (el) {
        $(el).on('click', dismiss, this.close)
      }

  Alert.prototype.close = function (e) {
    var $this = $(this)
      , selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = $(selector)

    e && e.preventDefault()

    $parent.length || ($parent = $this.hasClass('alert') ? $this : $this.parent())

    $parent.trigger(e = $.Event('close'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      $parent
        .trigger('closed')
        .remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent.on($.support.transition.end, removeElement) :
      removeElement()
  }


 /* ALERT PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.alert

  $.fn.alert = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('alert')
      if (!data) $this.data('alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.alert.Constructor = Alert


 /* ALERT NO CONFLICT
  * ================= */

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


 /* ALERT DATA-API
  * ============== */

  $(document).on('click.alert.data-api', dismiss, Alert.prototype.close)

}(window.jQuery);/* ============================================================
 * bootstrap-button.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#buttons
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* BUTTON PUBLIC CLASS DEFINITION
  * ============================== */

  var Button = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.button.defaults, options)
  }

  Button.prototype.setState = function (state) {
    var d = 'disabled'
      , $el = this.$element
      , data = $el.data()
      , val = $el.is('input') ? 'val' : 'html'

    state = state + 'Text'
    data.resetText || $el.data('resetText', $el[val]())

    $el[val](data[state] || this.options[state])

    // push to event loop to allow forms to submit
    setTimeout(function () {
      state == 'loadingText' ?
        $el.addClass(d).attr(d, d) :
        $el.removeClass(d).removeAttr(d)
    }, 0)
  }

  Button.prototype.toggle = function () {
    var $parent = this.$element.closest('[data-toggle="buttons-radio"]')

    $parent && $parent
      .find('.active')
      .removeClass('active')

    this.$element.toggleClass('active')
  }


 /* BUTTON PLUGIN DEFINITION
  * ======================== */

  var old = $.fn.button

  $.fn.button = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('button')
        , options = typeof option == 'object' && option
      if (!data) $this.data('button', (data = new Button(this, options)))
      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.button.defaults = {
    loadingText: 'loading...'
  }

  $.fn.button.Constructor = Button


 /* BUTTON NO CONFLICT
  * ================== */

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


 /* BUTTON DATA-API
  * =============== */

  $(document).on('click.button.data-api', '[data-toggle^=button]', function (e) {
    var $btn = $(e.target)
    if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
    $btn.button('toggle')
  })

}(window.jQuery);/* ==========================================================
 * bootstrap-carousel.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#carousel
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* CAROUSEL CLASS DEFINITION
  * ========================= */

  var Carousel = function (element, options) {
    this.$element = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options = options
    this.options.pause == 'hover' && this.$element
      .on('mouseenter', $.proxy(this.pause, this))
      .on('mouseleave', $.proxy(this.cycle, this))
  }

  Carousel.prototype = {

    cycle: function (e) {
      if (!e) this.paused = false
      if (this.interval) clearInterval(this.interval);
      this.options.interval
        && !this.paused
        && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
      return this
    }

  , getActiveIndex: function () {
      this.$active = this.$element.find('.item.active')
      this.$items = this.$active.parent().children()
      return this.$items.index(this.$active)
    }

  , to: function (pos) {
      var activeIndex = this.getActiveIndex()
        , that = this

      if (pos > (this.$items.length - 1) || pos < 0) return

      if (this.sliding) {
        return this.$element.one('slid', function () {
          that.to(pos)
        })
      }

      if (activeIndex == pos) {
        return this.pause().cycle()
      }

      return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
    }

  , pause: function (e) {
      if (!e) this.paused = true
      if (this.$element.find('.next, .prev').length && $.support.transition.end) {
        this.$element.trigger($.support.transition.end)
        this.cycle(true)
      }
      clearInterval(this.interval)
      this.interval = null
      return this
    }

  , next: function () {
      if (this.sliding) return
      return this.slide('next')
    }

  , prev: function () {
      if (this.sliding) return
      return this.slide('prev')
    }

  , slide: function (type, next) {
      var $active = this.$element.find('.item.active')
        , $next = next || $active[type]()
        , isCycling = this.interval
        , direction = type == 'next' ? 'left' : 'right'
        , fallback  = type == 'next' ? 'first' : 'last'
        , that = this
        , e

      this.sliding = true

      isCycling && this.pause()

      $next = $next.length ? $next : this.$element.find('.item')[fallback]()

      e = $.Event('slide', {
        relatedTarget: $next[0]
      , direction: direction
      })

      if ($next.hasClass('active')) return

      if (this.$indicators.length) {
        this.$indicators.find('.active').removeClass('active')
        this.$element.one('slid', function () {
          var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
          $nextIndicator && $nextIndicator.addClass('active')
        })
      }

      if ($.support.transition && this.$element.hasClass('slide')) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $next.addClass(type)
        $next[0].offsetWidth // force reflow
        $active.addClass(direction)
        $next.addClass(direction)
        this.$element.one($.support.transition.end, function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () { that.$element.trigger('slid') }, 0)
        })
      } else {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $active.removeClass('active')
        $next.addClass('active')
        this.sliding = false
        this.$element.trigger('slid')
      }

      isCycling && this.cycle()

      return this
    }

  }


 /* CAROUSEL PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.carousel

  $.fn.carousel = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('carousel')
        , options = $.extend({}, $.fn.carousel.defaults, typeof option == 'object' && option)
        , action = typeof option == 'string' ? option : options.slide
      if (!data) $this.data('carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  $.fn.carousel.defaults = {
    interval: 5000
  , pause: 'hover'
  }

  $.fn.carousel.Constructor = Carousel


 /* CAROUSEL NO CONFLICT
  * ==================== */

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }

 /* CAROUSEL DATA-API
  * ================= */

  $(document).on('click.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
    var $this = $(this), href
      , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      , options = $.extend({}, $target.data(), $this.data())
      , slideIndex

    $target.carousel(options)

    if (slideIndex = $this.attr('data-slide-to')) {
      $target.data('carousel').pause().to(slideIndex).cycle()
    }

    e.preventDefault()
  })

}(window.jQuery);/* =============================================================
 * bootstrap-collapse.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#collapse
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* COLLAPSE PUBLIC CLASS DEFINITION
  * ================================ */

  var Collapse = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.collapse.defaults, options)

    if (this.options.parent) {
      this.$parent = $(this.options.parent)
    }

    this.options.toggle && this.toggle()
  }

  Collapse.prototype = {

    constructor: Collapse

  , dimension: function () {
      var hasWidth = this.$element.hasClass('width')
      return hasWidth ? 'width' : 'height'
    }

  , show: function () {
      var dimension
        , scroll
        , actives
        , hasData

      if (this.transitioning || this.$element.hasClass('in')) return

      dimension = this.dimension()
      scroll = $.camelCase(['scroll', dimension].join('-'))
      actives = this.$parent && this.$parent.find('> .accordion-group > .in')

      if (actives && actives.length) {
        hasData = actives.data('collapse')
        if (hasData && hasData.transitioning) return
        actives.collapse('hide')
        hasData || actives.data('collapse', null)
      }

      this.$element[dimension](0)
      this.transition('addClass', $.Event('show'), 'shown')
      $.support.transition && this.$element[dimension](this.$element[0][scroll])
    }

  , hide: function () {
      var dimension
      if (this.transitioning || !this.$element.hasClass('in')) return
      dimension = this.dimension()
      this.reset(this.$element[dimension]())
      this.transition('removeClass', $.Event('hide'), 'hidden')
      this.$element[dimension](0)
    }

  , reset: function (size) {
      var dimension = this.dimension()

      this.$element
        .removeClass('collapse')
        [dimension](size || 'auto')
        [0].offsetWidth

      this.$element[size !== null ? 'addClass' : 'removeClass']('collapse')

      return this
    }

  , transition: function (method, startEvent, completeEvent) {
      var that = this
        , complete = function () {
            if (startEvent.type == 'show') that.reset()
            that.transitioning = 0
            that.$element.trigger(completeEvent)
          }

      this.$element.trigger(startEvent)

      if (startEvent.isDefaultPrevented()) return

      this.transitioning = 1

      this.$element[method]('in')

      $.support.transition && this.$element.hasClass('collapse') ?
        this.$element.one($.support.transition.end, complete) :
        complete()
    }

  , toggle: function () {
      this[this.$element.hasClass('in') ? 'hide' : 'show']()
    }

  }


 /* COLLAPSE PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.collapse

  $.fn.collapse = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('collapse')
        , options = $.extend({}, $.fn.collapse.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.collapse.defaults = {
    toggle: true
  }

  $.fn.collapse.Constructor = Collapse


 /* COLLAPSE NO CONFLICT
  * ==================== */

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


 /* COLLAPSE DATA-API
  * ================= */

  $(document).on('click.collapse.data-api', '[data-toggle=collapse]', function (e) {
    var $this = $(this), href
      , target = $this.attr('data-target')
        || e.preventDefault()
        || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
      , option = $(target).data('collapse') ? 'toggle' : $this.data()
    $this[$(target).hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
    $(target).collapse(option)
  })

}(window.jQuery);/* ============================================================
 * bootstrap-dropdown.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#dropdowns
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle=dropdown]'
    , Dropdown = function (element) {
        var $el = $(element).on('click.dropdown.data-api', this.toggle)
        $('html').on('click.dropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  Dropdown.prototype = {

    constructor: Dropdown

  , toggle: function (e) {
      var $this = $(this)
        , $parent
        , isActive

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      clearMenus()

      if (!isActive) {
        $parent.toggleClass('open')
      }

      $this.focus()

      return false
    }

  , keydown: function (e) {
      var $this
        , $items
        , $active
        , $parent
        , isActive
        , index

      if (!/(38|40|27)/.test(e.keyCode)) return

      $this = $(this)

      e.preventDefault()
      e.stopPropagation()

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      if (!isActive || (isActive && e.keyCode == 27)) {
        if (e.which == 27) $parent.find(toggle).focus()
        return $this.click()
      }

      $items = $('[role=menu] li:not(.divider):visible a', $parent)

      if (!$items.length) return

      index = $items.index($items.filter(':focus'))

      if (e.keyCode == 38 && index > 0) index--                                        // up
      if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
      if (!~index) index = 0

      $items
        .eq(index)
        .focus()
    }

  }

  function clearMenus() {
    $(toggle).each(function () {
      getParent($(this)).removeClass('open')
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = selector && $(selector)

    if (!$parent || !$parent.length) $parent = $this.parent()

    return $parent
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  var old = $.fn.dropdown

  $.fn.dropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('dropdown')
      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


 /* DROPDOWN NO CONFLICT
  * ==================== */

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(document)
    .on('click.dropdown.data-api', clearMenus)
    .on('click.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.dropdown-menu', function (e) { e.stopPropagation() })
    .on('click.dropdown.data-api'  , toggle, Dropdown.prototype.toggle)
    .on('keydown.dropdown.data-api', toggle + ', [role=menu]' , Dropdown.prototype.keydown)

}(window.jQuery);
/* =========================================================
 * bootstrap-modal.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#modals
 * =========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */


!function ($) {

  "use strict"; // jshint ;_;


 /* MODAL CLASS DEFINITION
  * ====================== */

  var Modal = function (element, options) {
    this.options = options
    this.$element = $(element)
      .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
    this.options.remote && this.$element.find('.modal-body').load(this.options.remote)
  }

  Modal.prototype = {

      constructor: Modal

    , toggle: function () {
        return this[!this.isShown ? 'show' : 'hide']()
      }

    , show: function () {
        var that = this
          , e = $.Event('show')

        this.$element.trigger(e)

        if (this.isShown || e.isDefaultPrevented()) return

        this.isShown = true

        this.escape()

        this.backdrop(function () {
          var transition = $.support.transition && that.$element.hasClass('fade')

          if (!that.$element.parent().length) {
            that.$element.appendTo(document.body) //don't move modals dom position
          }

          that.$element.show()

          if (transition) {
            that.$element[0].offsetWidth // force reflow
          }

          that.$element
            .addClass('in')
            .attr('aria-hidden', false)

          that.enforceFocus()

          transition ?
            that.$element.one($.support.transition.end, function () { that.$element.focus().trigger('shown') }) :
            that.$element.focus().trigger('shown')

        })
      }

    , hide: function (e) {
        e && e.preventDefault()

        var that = this

        e = $.Event('hide')

        this.$element.trigger(e)

        if (!this.isShown || e.isDefaultPrevented()) return

        this.isShown = false

        this.escape()

        $(document).off('focusin.modal')

        this.$element
          .removeClass('in')
          .attr('aria-hidden', true)

        $.support.transition && this.$element.hasClass('fade') ?
          this.hideWithTransition() :
          this.hideModal()
      }

    , enforceFocus: function () {
        var that = this
        $(document).on('focusin.modal', function (e) {
          if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
            that.$element.focus()
          }
        })
      }

    , escape: function () {
        var that = this
        if (this.isShown && this.options.keyboard) {
          this.$element.on('keyup.dismiss.modal', function ( e ) {
            e.which == 27 && that.hide()
          })
        } else if (!this.isShown) {
          this.$element.off('keyup.dismiss.modal')
        }
      }

    , hideWithTransition: function () {
        var that = this
          , timeout = setTimeout(function () {
              that.$element.off($.support.transition.end)
              that.hideModal()
            }, 500)

        this.$element.one($.support.transition.end, function () {
          clearTimeout(timeout)
          that.hideModal()
        })
      }

    , hideModal: function () {
        var that = this
        this.$element.hide()
        this.backdrop(function () {
          that.removeBackdrop()
          that.$element.trigger('hidden')
        })
      }

    , removeBackdrop: function () {
        this.$backdrop && this.$backdrop.remove()
        this.$backdrop = null
      }

    , backdrop: function (callback) {
        var that = this
          , animate = this.$element.hasClass('fade') ? 'fade' : ''

        if (this.isShown && this.options.backdrop) {
          var doAnimate = $.support.transition && animate

          this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
            .appendTo(document.body)

          this.$backdrop.click(
            this.options.backdrop == 'static' ?
              $.proxy(this.$element[0].focus, this.$element[0])
            : $.proxy(this.hide, this)
          )

          if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

          this.$backdrop.addClass('in')

          if (!callback) return

          doAnimate ?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (!this.isShown && this.$backdrop) {
          this.$backdrop.removeClass('in')

          $.support.transition && this.$element.hasClass('fade')?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (callback) {
          callback()
        }
      }
  }


 /* MODAL PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.modal

  $.fn.modal = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('modal')
        , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option]()
      else if (options.show) data.show()
    })
  }

  $.fn.modal.defaults = {
      backdrop: true
    , keyboard: true
    , show: true
  }

  $.fn.modal.Constructor = Modal


 /* MODAL NO CONFLICT
  * ================= */

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


 /* MODAL DATA-API
  * ============== */

  $(document).on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this = $(this)
      , href = $this.attr('href')
      , $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
      , option = $target.data('modal') ? 'toggle' : $.extend({ remote:!/#/.test(href) && href }, $target.data(), $this.data())

    e.preventDefault()

    $target
      .modal(option)
      .one('hide', function () {
        $this.focus()
      })
  })

}(window.jQuery);
/* ===========================================================
 * bootstrap-tooltip.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#tooltips
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TOOLTIP PUBLIC CLASS DEFINITION
  * =============================== */

  var Tooltip = function (element, options) {
    this.init('tooltip', element, options)
  }

  Tooltip.prototype = {

    constructor: Tooltip

  , init: function (type, element, options) {
      var eventIn
        , eventOut
        , triggers
        , trigger
        , i

      this.type = type
      this.$element = $(element)
      this.options = this.getOptions(options)
      this.enabled = true

      triggers = this.options.trigger.split(' ')

      for (i = triggers.length; i--;) {
        trigger = triggers[i]
        if (trigger == 'click') {
          this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
        } else if (trigger != 'manual') {
          eventIn = trigger == 'hover' ? 'mouseenter' : 'focus'
          eventOut = trigger == 'hover' ? 'mouseleave' : 'blur'
          this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
          this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
        }
      }

      this.options.selector ?
        (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
        this.fixTitle()
    }

  , getOptions: function (options) {
      options = $.extend({}, $.fn[this.type].defaults, this.$element.data(), options)

      if (options.delay && typeof options.delay == 'number') {
        options.delay = {
          show: options.delay
        , hide: options.delay
        }
      }

      return options
    }

  , enter: function (e) {
      var defaults = $.fn[this.type].defaults
        , options = {}
        , self

      this._options && $.each(this._options, function (key, value) {
        if (defaults[key] != value) options[key] = value
      }, this)

      self = $(e.currentTarget)[this.type](options).data(this.type)

      if (!self.options.delay || !self.options.delay.show) return self.show()

      clearTimeout(this.timeout)
      self.hoverState = 'in'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'in') self.show()
      }, self.options.delay.show)
    }

  , leave: function (e) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (this.timeout) clearTimeout(this.timeout)
      if (!self.options.delay || !self.options.delay.hide) return self.hide()

      self.hoverState = 'out'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'out') self.hide()
      }, self.options.delay.hide)
    }

  , show: function () {
      var $tip
        , pos
        , actualWidth
        , actualHeight
        , placement
        , tp
        , e = $.Event('show')

      if (this.hasContent() && this.enabled) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $tip = this.tip()
        this.setContent()

        if (this.options.animation) {
          $tip.addClass('fade')
        }

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        $tip
          .detach()
          .css({ top: 0, left: 0, display: 'block' })

        this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)

        pos = this.getPosition()

        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        switch (placement) {
          case 'bottom':
            tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'top':
            tp = {top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'left':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}
            break
          case 'right':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
            break
        }

        this.applyPlacement(tp, placement)
        this.$element.trigger('shown')
      }
    }

  , applyPlacement: function(offset, placement){
      var $tip = this.tip()
        , width = $tip[0].offsetWidth
        , height = $tip[0].offsetHeight
        , actualWidth
        , actualHeight
        , delta
        , replace

      $tip
        .offset(offset)
        .addClass(placement)
        .addClass('in')

      actualWidth = $tip[0].offsetWidth
      actualHeight = $tip[0].offsetHeight

      if (placement == 'top' && actualHeight != height) {
        offset.top = offset.top + height - actualHeight
        replace = true
      }

      if (placement == 'bottom' || placement == 'top') {
        delta = 0

        if (offset.left < 0){
          delta = offset.left * -2
          offset.left = 0
          $tip.offset(offset)
          actualWidth = $tip[0].offsetWidth
          actualHeight = $tip[0].offsetHeight
        }

        this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
      } else {
        this.replaceArrow(actualHeight - height, actualHeight, 'top')
      }

      if (replace) $tip.offset(offset)
    }

  , replaceArrow: function(delta, dimension, position){
      this
        .arrow()
        .css(position, delta ? (50 * (1 - delta / dimension) + "%") : '')
    }

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()

      $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
      $tip.removeClass('fade in top bottom left right')
    }

  , hide: function () {
      var that = this
        , $tip = this.tip()
        , e = $.Event('hide')

      this.$element.trigger(e)
      if (e.isDefaultPrevented()) return

      $tip.removeClass('in')

      function removeWithAnimation() {
        var timeout = setTimeout(function () {
          $tip.off($.support.transition.end).detach()
        }, 500)

        $tip.one($.support.transition.end, function () {
          clearTimeout(timeout)
          $tip.detach()
        })
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        removeWithAnimation() :
        $tip.detach()

      this.$element.trigger('hidden')

      return this
    }

  , fixTitle: function () {
      var $e = this.$element
      if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
        $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
      }
    }

  , hasContent: function () {
      return this.getTitle()
    }

  , getPosition: function () {
      var el = this.$element[0]
      return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
        width: el.offsetWidth
      , height: el.offsetHeight
      }, this.$element.offset())
    }

  , getTitle: function () {
      var title
        , $e = this.$element
        , o = this.options

      title = $e.attr('data-original-title')
        || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

      return title
    }

  , tip: function () {
      return this.$tip = this.$tip || $(this.options.template)
    }

  , arrow: function(){
      return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }

  , validate: function () {
      if (!this.$element[0].parentNode) {
        this.hide()
        this.$element = null
        this.options = null
      }
    }

  , enable: function () {
      this.enabled = true
    }

  , disable: function () {
      this.enabled = false
    }

  , toggleEnabled: function () {
      this.enabled = !this.enabled
    }

  , toggle: function (e) {
      var self = e ? $(e.currentTarget)[this.type](this._options).data(this.type) : this
      self.tip().hasClass('in') ? self.hide() : self.show()
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  }


 /* TOOLTIP PLUGIN DEFINITION
  * ========================= */

  var old = $.fn.tooltip

  $.fn.tooltip = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tooltip')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tooltip.Constructor = Tooltip

  $.fn.tooltip.defaults = {
    animation: true
  , placement: 'top'
  , selector: false
  , template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  , trigger: 'hover focus'
  , title: ''
  , delay: 0
  , html: false
  , container: false
  }


 /* TOOLTIP NO CONFLICT
  * =================== */

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(window.jQuery);
/* ===========================================================
 * bootstrap-popover.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#popovers
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* POPOVER PUBLIC CLASS DEFINITION
  * =============================== */

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }


  /* NOTE: POPOVER EXTENDS BOOTSTRAP-TOOLTIP.js
     ========================================== */

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype, {

    constructor: Popover

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()
        , content = this.getContent()

      $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
      $tip.find('.popover-content')[this.options.html ? 'html' : 'text'](content)

      $tip.removeClass('fade top bottom left right in')
    }

  , hasContent: function () {
      return this.getTitle() || this.getContent()
    }

  , getContent: function () {
      var content
        , $e = this.$element
        , o = this.options

      content = (typeof o.content == 'function' ? o.content.call($e[0]) :  o.content)
        || $e.attr('data-content')

      return content
    }

  , tip: function () {
      if (!this.$tip) {
        this.$tip = $(this.options.template)
      }
      return this.$tip
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  })


 /* POPOVER PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.popover

  $.fn.popover = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('popover')
        , options = typeof option == 'object' && option
      if (!data) $this.data('popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.popover.Constructor = Popover

  $.fn.popover.defaults = $.extend({} , $.fn.tooltip.defaults, {
    placement: 'right'
  , trigger: 'click'
  , content: ''
  , template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


 /* POPOVER NO CONFLICT
  * =================== */

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(window.jQuery);
/* =============================================================
 * bootstrap-scrollspy.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#scrollspy
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* SCROLLSPY CLASS DEFINITION
  * ========================== */

  function ScrollSpy(element, options) {
    var process = $.proxy(this.process, this)
      , $element = $(element).is('body') ? $(window) : $(element)
      , href
    this.options = $.extend({}, $.fn.scrollspy.defaults, options)
    this.$scrollElement = $element.on('scroll.scroll-spy.data-api', process)
    this.selector = (this.options.target
      || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      || '') + ' .nav li > a'
    this.$body = $('body')
    this.refresh()
    this.process()
  }

  ScrollSpy.prototype = {

      constructor: ScrollSpy

    , refresh: function () {
        var self = this
          , $targets

        this.offsets = $([])
        this.targets = $([])

        $targets = this.$body
          .find(this.selector)
          .map(function () {
            var $el = $(this)
              , href = $el.data('target') || $el.attr('href')
              , $href = /^#\w/.test(href) && $(href)
            return ( $href
              && $href.length
              && [[ $href.position().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href ]] ) || null
          })
          .sort(function (a, b) { return a[0] - b[0] })
          .each(function () {
            self.offsets.push(this[0])
            self.targets.push(this[1])
          })
      }

    , process: function () {
        var scrollTop = this.$scrollElement.scrollTop() + this.options.offset
          , scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight
          , maxScroll = scrollHeight - this.$scrollElement.height()
          , offsets = this.offsets
          , targets = this.targets
          , activeTarget = this.activeTarget
          , i

        if (scrollTop >= maxScroll) {
          return activeTarget != (i = targets.last()[0])
            && this.activate ( i )
        }

        for (i = offsets.length; i--;) {
          activeTarget != targets[i]
            && scrollTop >= offsets[i]
            && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
            && this.activate( targets[i] )
        }
      }

    , activate: function (target) {
        var active
          , selector

        this.activeTarget = target

        $(this.selector)
          .parent('.active')
          .removeClass('active')

        selector = this.selector
          + '[data-target="' + target + '"],'
          + this.selector + '[href="' + target + '"]'

        active = $(selector)
          .parent('li')
          .addClass('active')

        if (active.parent('.dropdown-menu').length)  {
          active = active.closest('li.dropdown').addClass('active')
        }

        active.trigger('activate')
      }

  }


 /* SCROLLSPY PLUGIN DEFINITION
  * =========================== */

  var old = $.fn.scrollspy

  $.fn.scrollspy = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('scrollspy')
        , options = typeof option == 'object' && option
      if (!data) $this.data('scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.scrollspy.Constructor = ScrollSpy

  $.fn.scrollspy.defaults = {
    offset: 10
  }


 /* SCROLLSPY NO CONFLICT
  * ===================== */

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


 /* SCROLLSPY DATA-API
  * ================== */

  $(window).on('load', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      $spy.scrollspy($spy.data())
    })
  })

}(window.jQuery);/* ========================================================
 * bootstrap-tab.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#tabs
 * ========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TAB CLASS DEFINITION
  * ==================== */

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.prototype = {

    constructor: Tab

  , show: function () {
      var $this = this.element
        , $ul = $this.closest('ul:not(.dropdown-menu)')
        , selector = $this.attr('data-target')
        , previous
        , $target
        , e

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      if ( $this.parent('li').hasClass('active') ) return

      previous = $ul.find('.active:last a')[0]

      e = $.Event('show', {
        relatedTarget: previous
      })

      $this.trigger(e)

      if (e.isDefaultPrevented()) return

      $target = $(selector)

      this.activate($this.parent('li'), $ul)
      this.activate($target, $target.parent(), function () {
        $this.trigger({
          type: 'shown'
        , relatedTarget: previous
        })
      })
    }

  , activate: function ( element, container, callback) {
      var $active = container.find('> .active')
        , transition = callback
            && $.support.transition
            && $active.hasClass('fade')

      function next() {
        $active
          .removeClass('active')
          .find('> .dropdown-menu > .active')
          .removeClass('active')

        element.addClass('active')

        if (transition) {
          element[0].offsetWidth // reflow for transition
          element.addClass('in')
        } else {
          element.removeClass('fade')
        }

        if ( element.parent('.dropdown-menu') ) {
          element.closest('li.dropdown').addClass('active')
        }

        callback && callback()
      }

      transition ?
        $active.one($.support.transition.end, next) :
        next()

      $active.removeClass('in')
    }
  }


 /* TAB PLUGIN DEFINITION
  * ===================== */

  var old = $.fn.tab

  $.fn.tab = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tab')
      if (!data) $this.data('tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tab.Constructor = Tab


 /* TAB NO CONFLICT
  * =============== */

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


 /* TAB DATA-API
  * ============ */

  $(document).on('click.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

}(window.jQuery);/* =============================================================
/*!
* typeahead.js 0.11.1
* https://github.com/twitter/typeahead.js
* Copyright 2013-2015 Twitter, Inc. and other contributors; Licensed MIT
*/

(function(root, factory) {
   if (typeof define === "function" && define.amd) {
       define("bloodhound", [ "jquery" ], function(a0) {
           return root["Bloodhound"] = factory(a0);
       });
   } else if (typeof exports === "object") {
       module.exports = factory(require("jquery"));
   } else {
       root["Bloodhound"] = factory(jQuery);
   }
})(this, function($) {
   var _ = function() {
       "use strict";
       return {
           isMsie: function() {
               return /(msie|trident)/i.test(navigator.userAgent) ? navigator.userAgent.match(/(msie |rv:)(\d+(.\d+)?)/i)[2] : false;
           },
           isBlankString: function(str) {
               return !str || /^\s*$/.test(str);
           },
           escapeRegExChars: function(str) {
               return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
           },
           isString: function(obj) {
               return typeof obj === "string";
           },
           isNumber: function(obj) {
               return typeof obj === "number";
           },
           isArray: $.isArray,
           isFunction: $.isFunction,
           isObject: $.isPlainObject,
           isUndefined: function(obj) {
               return typeof obj === "undefined";
           },
           isElement: function(obj) {
               return !!(obj && obj.nodeType === 1);
           },
           isJQuery: function(obj) {
               return obj instanceof $;
           },
           toStr: function toStr(s) {
               return _.isUndefined(s) || s === null ? "" : s + "";
           },
           bind: $.proxy,
           each: function(collection, cb) {
               $.each(collection, reverseArgs);
               function reverseArgs(index, value) {
                   return cb(value, index);
               }
           },
           map: $.map,
           filter: $.grep,
           every: function(obj, test) {
               var result = true;
               if (!obj) {
                   return result;
               }
               $.each(obj, function(key, val) {
                   if (!(result = test.call(null, val, key, obj))) {
                       return false;
                   }
               });
               return !!result;
           },
           some: function(obj, test) {
               var result = false;
               if (!obj) {
                   return result;
               }
               $.each(obj, function(key, val) {
                   if (result = test.call(null, val, key, obj)) {
                       return false;
                   }
               });
               return !!result;
           },
           mixin: $.extend,
           identity: function(x) {
               return x;
           },
           clone: function(obj) {
               return $.extend(true, {}, obj);
           },
           getIdGenerator: function() {
               var counter = 0;
               return function() {
                   return counter++;
               };
           },
           templatify: function templatify(obj) {
               return $.isFunction(obj) ? obj : template;
               function template() {
                   return String(obj);
               }
           },
           defer: function(fn) {
               setTimeout(fn, 0);
           },
           debounce: function(func, wait, immediate) {
               var timeout, result;
               return function() {
                   var context = this, args = arguments, later, callNow;
                   later = function() {
                       timeout = null;
                       if (!immediate) {
                           result = func.apply(context, args);
                       }
                   };
                   callNow = immediate && !timeout;
                   clearTimeout(timeout);
                   timeout = setTimeout(later, wait);
                   if (callNow) {
                       result = func.apply(context, args);
                   }
                   return result;
               };
           },
           throttle: function(func, wait) {
               var context, args, timeout, result, previous, later;
               previous = 0;
               later = function() {
                   previous = new Date();
                   timeout = null;
                   result = func.apply(context, args);
               };
               return function() {
                   var now = new Date(), remaining = wait - (now - previous);
                   context = this;
                   args = arguments;
                   if (remaining <= 0) {
                       clearTimeout(timeout);
                       timeout = null;
                       previous = now;
                       result = func.apply(context, args);
                   } else if (!timeout) {
                       timeout = setTimeout(later, remaining);
                   }
                   return result;
               };
           },
           stringify: function(val) {
               return _.isString(val) ? val : JSON.stringify(val);
           },
           noop: function() {}
       };
   }();
   var VERSION = "0.11.1";
   var tokenizers = function() {
       "use strict";
       return {
           nonword: nonword,
           whitespace: whitespace,
           obj: {
               nonword: getObjTokenizer(nonword),
               whitespace: getObjTokenizer(whitespace)
           }
       };
       function whitespace(str) {
           str = _.toStr(str);
           return str ? str.split(/\s+/) : [];
       }
       function nonword(str) {
           str = _.toStr(str);
           return str ? str.split(/\W+/) : [];
       }
       function getObjTokenizer(tokenizer) {
           return function setKey(keys) {
               keys = _.isArray(keys) ? keys : [].slice.call(arguments, 0);
               return function tokenize(o) {
                   var tokens = [];
                   _.each(keys, function(k) {
                       tokens = tokens.concat(tokenizer(_.toStr(o[k])));
                   });
                   return tokens;
               };
           };
       }
   }();
   var LruCache = function() {
       "use strict";
       function LruCache(maxSize) {
           this.maxSize = _.isNumber(maxSize) ? maxSize : 100;
           this.reset();
           if (this.maxSize <= 0) {
               this.set = this.get = $.noop;
           }
       }
       _.mixin(LruCache.prototype, {
           set: function set(key, val) {
               var tailItem = this.list.tail, node;
               if (this.size >= this.maxSize) {
                   this.list.remove(tailItem);
                   delete this.hash[tailItem.key];
                   this.size--;
               }
               if (node = this.hash[key]) {
                   node.val = val;
                   this.list.moveToFront(node);
               } else {
                   node = new Node(key, val);
                   this.list.add(node);
                   this.hash[key] = node;
                   this.size++;
               }
           },
           get: function get(key) {
               var node = this.hash[key];
               if (node) {
                   this.list.moveToFront(node);
                   return node.val;
               }
           },
           reset: function reset() {
               this.size = 0;
               this.hash = {};
               this.list = new List();
           }
       });
       function List() {
           this.head = this.tail = null;
       }
       _.mixin(List.prototype, {
           add: function add(node) {
               if (this.head) {
                   node.next = this.head;
                   this.head.prev = node;
               }
               this.head = node;
               this.tail = this.tail || node;
           },
           remove: function remove(node) {
               node.prev ? node.prev.next = node.next : this.head = node.next;
               node.next ? node.next.prev = node.prev : this.tail = node.prev;
           },
           moveToFront: function(node) {
               this.remove(node);
               this.add(node);
           }
       });
       function Node(key, val) {
           this.key = key;
           this.val = val;
           this.prev = this.next = null;
       }
       return LruCache;
   }();
   var PersistentStorage = function() {
       "use strict";
       var LOCAL_STORAGE;
       try {
           LOCAL_STORAGE = window.localStorage;
           LOCAL_STORAGE.setItem("~~~", "!");
           LOCAL_STORAGE.removeItem("~~~");
       } catch (err) {
           LOCAL_STORAGE = null;
       }
       function PersistentStorage(namespace, override) {
           this.prefix = [ "__", namespace, "__" ].join("");
           this.ttlKey = "__ttl__";
           this.keyMatcher = new RegExp("^" + _.escapeRegExChars(this.prefix));
           this.ls = override || LOCAL_STORAGE;
           !this.ls && this._noop();
       }
       _.mixin(PersistentStorage.prototype, {
           _prefix: function(key) {
               return this.prefix + key;
           },
           _ttlKey: function(key) {
               return this._prefix(key) + this.ttlKey;
           },
           _noop: function() {
               this.get = this.set = this.remove = this.clear = this.isExpired = _.noop;
           },
           _safeSet: function(key, val) {
               try {
                   this.ls.setItem(key, val);
               } catch (err) {
                   if (err.name === "QuotaExceededError") {
                       this.clear();
                       this._noop();
                   }
               }
           },
           get: function(key) {
               if (this.isExpired(key)) {
                   this.remove(key);
               }
               return decode(this.ls.getItem(this._prefix(key)));
           },
           set: function(key, val, ttl) {
               if (_.isNumber(ttl)) {
                   this._safeSet(this._ttlKey(key), encode(now() + ttl));
               } else {
                   this.ls.removeItem(this._ttlKey(key));
               }
               return this._safeSet(this._prefix(key), encode(val));
           },
           remove: function(key) {
               this.ls.removeItem(this._ttlKey(key));
               this.ls.removeItem(this._prefix(key));
               return this;
           },
           clear: function() {
               var i, keys = gatherMatchingKeys(this.keyMatcher);
               for (i = keys.length; i--; ) {
                   this.remove(keys[i]);
               }
               return this;
           },
           isExpired: function(key) {
               var ttl = decode(this.ls.getItem(this._ttlKey(key)));
               return _.isNumber(ttl) && now() > ttl ? true : false;
           }
       });
       return PersistentStorage;
       function now() {
           return new Date().getTime();
       }
       function encode(val) {
           return JSON.stringify(_.isUndefined(val) ? null : val);
       }
       function decode(val) {
           return $.parseJSON(val);
       }
       function gatherMatchingKeys(keyMatcher) {
           var i, key, keys = [], len = LOCAL_STORAGE.length;
           for (i = 0; i < len; i++) {
               if ((key = LOCAL_STORAGE.key(i)).match(keyMatcher)) {
                   keys.push(key.replace(keyMatcher, ""));
               }
           }
           return keys;
       }
   }();
   var Transport = function() {
       "use strict";
       var pendingRequestsCount = 0, pendingRequests = {}, maxPendingRequests = 6, sharedCache = new LruCache(10);
       function Transport(o) {
           o = o || {};
           this.cancelled = false;
           this.lastReq = null;
           this._send = o.transport;
           this._get = o.limiter ? o.limiter(this._get) : this._get;
           this._cache = o.cache === false ? new LruCache(0) : sharedCache;
       }
       Transport.setMaxPendingRequests = function setMaxPendingRequests(num) {
           maxPendingRequests = num;
       };
       Transport.resetCache = function resetCache() {
           sharedCache.reset();
       };
       _.mixin(Transport.prototype, {
           _fingerprint: function fingerprint(o) {
               o = o || {};
               return o.url + o.type + $.param(o.data || {});
           },
           _get: function(o, cb) {
               var that = this, fingerprint, jqXhr;
               fingerprint = this._fingerprint(o);
               if (this.cancelled || fingerprint !== this.lastReq) {
                   return;
               }
               if (jqXhr = pendingRequests[fingerprint]) {
                   jqXhr.done(done).fail(fail);
               } else if (pendingRequestsCount < maxPendingRequests) {
                   pendingRequestsCount++;
                   pendingRequests[fingerprint] = this._send(o).done(done).fail(fail).always(always);
               } else {
                   this.onDeckRequestArgs = [].slice.call(arguments, 0);
               }
               function done(resp) {
                   cb(null, resp);
                   that._cache.set(fingerprint, resp);
               }
               function fail() {
                   cb(true);
               }
               function always() {
                   pendingRequestsCount--;
                   delete pendingRequests[fingerprint];
                   if (that.onDeckRequestArgs) {
                       that._get.apply(that, that.onDeckRequestArgs);
                       that.onDeckRequestArgs = null;
                   }
               }
           },
           get: function(o, cb) {
               var resp, fingerprint;
               cb = cb || $.noop;
               o = _.isString(o) ? {
                   url: o
               } : o || {};
               fingerprint = this._fingerprint(o);
               this.cancelled = false;
               this.lastReq = fingerprint;
               if (resp = this._cache.get(fingerprint)) {
                   cb(null, resp);
               } else {
                   this._get(o, cb);
               }
           },
           cancel: function() {
               this.cancelled = true;
           }
       });
       return Transport;
   }();
   var SearchIndex = window.SearchIndex = function() {
       "use strict";
       var CHILDREN = "c", IDS = "i";
       function SearchIndex(o) {
           o = o || {};
           if (!o.datumTokenizer || !o.queryTokenizer) {
               $.error("datumTokenizer and queryTokenizer are both required");
           }
           this.identify = o.identify || _.stringify;
           this.datumTokenizer = o.datumTokenizer;
           this.queryTokenizer = o.queryTokenizer;
           this.reset();
       }
       _.mixin(SearchIndex.prototype, {
           bootstrap: function bootstrap(o) {
               this.datums = o.datums;
               this.trie = o.trie;
           },
           add: function(data) {
               var that = this;
               data = _.isArray(data) ? data : [ data ];
               _.each(data, function(datum) {
                   var id, tokens;
                   that.datums[id = that.identify(datum)] = datum;
                   tokens = normalizeTokens(that.datumTokenizer(datum));
                   _.each(tokens, function(token) {
                       var node, chars, ch;
                       node = that.trie;
                       chars = token.split("");
                       while (ch = chars.shift()) {
                           node = node[CHILDREN][ch] || (node[CHILDREN][ch] = newNode());
                           node[IDS].push(id);
                       }
                   });
               });
           },
           get: function get(ids) {
               var that = this;
               return _.map(ids, function(id) {
                   return that.datums[id];
               });
           },
           search: function search(query) {
               var that = this, tokens, matches;
               tokens = normalizeTokens(this.queryTokenizer(query));
               _.each(tokens, function(token) {
                   var node, chars, ch, ids;
                   if (matches && matches.length === 0) {
                       return false;
                   }
                   node = that.trie;
                   chars = token.split("");
                   while (node && (ch = chars.shift())) {
                       node = node[CHILDREN][ch];
                   }
                   if (node && chars.length === 0) {
                       ids = node[IDS].slice(0);
                       matches = matches ? getIntersection(matches, ids) : ids;
                   } else {
                       matches = [];
                       return false;
                   }
               });
               return matches ? _.map(unique(matches), function(id) {
                   return that.datums[id];
               }) : [];
           },
           all: function all() {
               var values = [];
               for (var key in this.datums) {
                   values.push(this.datums[key]);
               }
               return values;
           },
           reset: function reset() {
               this.datums = {};
               this.trie = newNode();
           },
           serialize: function serialize() {
               return {
                   datums: this.datums,
                   trie: this.trie
               };
           }
       });
       return SearchIndex;
       function normalizeTokens(tokens) {
           tokens = _.filter(tokens, function(token) {
               return !!token;
           });
           tokens = _.map(tokens, function(token) {
               return token.toLowerCase();
           });
           return tokens;
       }
       function newNode() {
           var node = {};
           node[IDS] = [];
           node[CHILDREN] = {};
           return node;
       }
       function unique(array) {
           var seen = {}, uniques = [];
           for (var i = 0, len = array.length; i < len; i++) {
               if (!seen[array[i]]) {
                   seen[array[i]] = true;
                   uniques.push(array[i]);
               }
           }
           return uniques;
       }
       function getIntersection(arrayA, arrayB) {
           var ai = 0, bi = 0, intersection = [];
           arrayA = arrayA.sort();
           arrayB = arrayB.sort();
           var lenArrayA = arrayA.length, lenArrayB = arrayB.length;
           while (ai < lenArrayA && bi < lenArrayB) {
               if (arrayA[ai] < arrayB[bi]) {
                   ai++;
               } else if (arrayA[ai] > arrayB[bi]) {
                   bi++;
               } else {
                   intersection.push(arrayA[ai]);
                   ai++;
                   bi++;
               }
           }
           return intersection;
       }
   }();
   var Prefetch = function() {
       "use strict";
       var keys;
       keys = {
           data: "data",
           protocol: "protocol",
           thumbprint: "thumbprint"
       };
       function Prefetch(o) {
           this.url = o.url;
           this.ttl = o.ttl;
           this.cache = o.cache;
           this.prepare = o.prepare;
           this.transform = o.transform;
           this.transport = o.transport;
           this.thumbprint = o.thumbprint;
           this.storage = new PersistentStorage(o.cacheKey);
       }
       _.mixin(Prefetch.prototype, {
           _settings: function settings() {
               return {
                   url: this.url,
                   type: "GET",
                   dataType: "json"
               };
           },
           store: function store(data) {
               if (!this.cache) {
                   return;
               }
               this.storage.set(keys.data, data, this.ttl);
               this.storage.set(keys.protocol, location.protocol, this.ttl);
               this.storage.set(keys.thumbprint, this.thumbprint, this.ttl);
           },
           fromCache: function fromCache() {
               var stored = {}, isExpired;
               if (!this.cache) {
                   return null;
               }
               stored.data = this.storage.get(keys.data);
               stored.protocol = this.storage.get(keys.protocol);
               stored.thumbprint = this.storage.get(keys.thumbprint);
               isExpired = stored.thumbprint !== this.thumbprint || stored.protocol !== location.protocol;
               return stored.data && !isExpired ? stored.data : null;
           },
           fromNetwork: function(cb) {
               var that = this, settings;
               if (!cb) {
                   return;
               }
               settings = this.prepare(this._settings());
               this.transport(settings).fail(onError).done(onResponse);
               function onError() {
                   cb(true);
               }
               function onResponse(resp) {
                   cb(null, that.transform(resp));
               }
           },
           clear: function clear() {
               this.storage.clear();
               return this;
           }
       });
       return Prefetch;
   }();
   var Remote = function() {
       "use strict";
       function Remote(o) {
           this.url = o.url;
           this.prepare = o.prepare;
           this.transform = o.transform;
           this.transport = new Transport({
               cache: o.cache,
               limiter: o.limiter,
               transport: o.transport
           });
       }
       _.mixin(Remote.prototype, {
           _settings: function settings() {
               return {
                   url: this.url,
                   type: "GET",
                   dataType: "json"
               };
           },
           get: function get(query, cb) {
               var that = this, settings;
               if (!cb) {
                   return;
               }
               query = query || "";
               settings = this.prepare(query, this._settings());
               return this.transport.get(settings, onResponse);
               function onResponse(err, resp) {
                   err ? cb([]) : cb(that.transform(resp));
               }
           },
           cancelLastRequest: function cancelLastRequest() {
               this.transport.cancel();
           }
       });
       return Remote;
   }();
   var oParser = function() {
       "use strict";
       return function parse(o) {
           var defaults, sorter;
           defaults = {
               initialize: true,
               identify: _.stringify,
               datumTokenizer: null,
               queryTokenizer: null,
               sufficient: 5,
               sorter: null,
               local: [],
               prefetch: null,
               remote: null
           };
           o = _.mixin(defaults, o || {});
           !o.datumTokenizer && $.error("datumTokenizer is required");
           !o.queryTokenizer && $.error("queryTokenizer is required");
           sorter = o.sorter;
           o.sorter = sorter ? function(x) {
               return x.sort(sorter);
           } : _.identity;
           o.local = _.isFunction(o.local) ? o.local() : o.local;
           o.prefetch = parsePrefetch(o.prefetch);
           o.remote = parseRemote(o.remote);
           return o;
       };
       function parsePrefetch(o) {
           var defaults;
           if (!o) {
               return null;
           }
           defaults = {
               url: null,
               ttl: 24 * 60 * 60 * 1e3,
               cache: true,
               cacheKey: null,
               thumbprint: "",
               prepare: _.identity,
               transform: _.identity,
               transport: null
           };
           o = _.isString(o) ? {
               url: o
           } : o;
           o = _.mixin(defaults, o);
           !o.url && $.error("prefetch requires url to be set");
           o.transform = o.filter || o.transform;
           o.cacheKey = o.cacheKey || o.url;
           o.thumbprint = VERSION + o.thumbprint;
           o.transport = o.transport ? callbackToDeferred(o.transport) : $.ajax;
           return o;
       }
       function parseRemote(o) {
           var defaults;
           if (!o) {
               return;
           }
           defaults = {
               url: null,
               cache: true,
               prepare: null,
               replace: null,
               wildcard: null,
               limiter: null,
               rateLimitBy: "debounce",
               rateLimitWait: 300,
               transform: _.identity,
               transport: null
           };
           o = _.isString(o) ? {
               url: o
           } : o;
           o = _.mixin(defaults, o);
           !o.url && $.error("remote requires url to be set");
           o.transform = o.filter || o.transform;
           o.prepare = toRemotePrepare(o);
           o.limiter = toLimiter(o);
           o.transport = o.transport ? callbackToDeferred(o.transport) : $.ajax;
           delete o.replace;
           delete o.wildcard;
           delete o.rateLimitBy;
           delete o.rateLimitWait;
           return o;
       }
       function toRemotePrepare(o) {
           var prepare, replace, wildcard;
           prepare = o.prepare;
           replace = o.replace;
           wildcard = o.wildcard;
           if (prepare) {
               return prepare;
           }
           if (replace) {
               prepare = prepareByReplace;
           } else if (o.wildcard) {
               prepare = prepareByWildcard;
           } else {
               prepare = idenityPrepare;
           }
           return prepare;
           function prepareByReplace(query, settings) {
               settings.url = replace(settings.url, query);
               return settings;
           }
           function prepareByWildcard(query, settings) {
               settings.url = settings.url.replace(wildcard, encodeURIComponent(query));
               return settings;
           }
           function idenityPrepare(query, settings) {
               return settings;
           }
       }
       function toLimiter(o) {
           var limiter, method, wait;
           limiter = o.limiter;
           method = o.rateLimitBy;
           wait = o.rateLimitWait;
           if (!limiter) {
               limiter = /^throttle$/i.test(method) ? throttle(wait) : debounce(wait);
           }
           return limiter;
           function debounce(wait) {
               return function debounce(fn) {
                   return _.debounce(fn, wait);
               };
           }
           function throttle(wait) {
               return function throttle(fn) {
                   return _.throttle(fn, wait);
               };
           }
       }
       function callbackToDeferred(fn) {
           return function wrapper(o) {
               var deferred = $.Deferred();
               fn(o, onSuccess, onError);
               return deferred;
               function onSuccess(resp) {
                   _.defer(function() {
                       deferred.resolve(resp);
                   });
               }
               function onError(err) {
                   _.defer(function() {
                       deferred.reject(err);
                   });
               }
           };
       }
   }();
   var Bloodhound = function() {
       "use strict";
       var old;
       old = window && window.Bloodhound;
       function Bloodhound(o) {
           o = oParser(o);
           this.sorter = o.sorter;
           this.identify = o.identify;
           this.sufficient = o.sufficient;
           this.local = o.local;
           this.remote = o.remote ? new Remote(o.remote) : null;
           this.prefetch = o.prefetch ? new Prefetch(o.prefetch) : null;
           this.index = new SearchIndex({
               identify: this.identify,
               datumTokenizer: o.datumTokenizer,
               queryTokenizer: o.queryTokenizer
           });
           o.initialize !== false && this.initialize();
       }
       Bloodhound.noConflict = function noConflict() {
           window && (window.Bloodhound = old);
           return Bloodhound;
       };
       Bloodhound.tokenizers = tokenizers;
       _.mixin(Bloodhound.prototype, {
           __ttAdapter: function ttAdapter() {
               var that = this;
               return this.remote ? withAsync : withoutAsync;
               function withAsync(query, sync, async) {
                   return that.search(query, sync, async);
               }
               function withoutAsync(query, sync) {
                   return that.search(query, sync);
               }
           },
           _loadPrefetch: function loadPrefetch() {
               var that = this, deferred, serialized;
               deferred = $.Deferred();
               if (!this.prefetch) {
                   deferred.resolve();
               } else if (serialized = this.prefetch.fromCache()) {
                   this.index.bootstrap(serialized);
                   deferred.resolve();
               } else {
                   this.prefetch.fromNetwork(done);
               }
               return deferred.promise();
               function done(err, data) {
                   if (err) {
                       return deferred.reject();
                   }
                   that.add(data);
                   that.prefetch.store(that.index.serialize());
                   deferred.resolve();
               }
           },
           _initialize: function initialize() {
               var that = this, deferred;
               this.clear();
               (this.initPromise = this._loadPrefetch()).done(addLocalToIndex);
               return this.initPromise;
               function addLocalToIndex() {
                   that.add(that.local);
               }
           },
           initialize: function initialize(force) {
               return !this.initPromise || force ? this._initialize() : this.initPromise;
           },
           add: function add(data) {
               this.index.add(data);
               return this;
           },
           get: function get(ids) {
               ids = _.isArray(ids) ? ids : [].slice.call(arguments);
               return this.index.get(ids);
           },
           search: function search(query, sync, async) {
               var that = this, local;
               local = this.sorter(this.index.search(query));
               sync(this.remote ? local.slice() : local);
               if (this.remote && local.length < this.sufficient) {
                   this.remote.get(query, processRemote);
               } else if (this.remote) {
                   this.remote.cancelLastRequest();
               }
               return this;
               function processRemote(remote) {
                   var nonDuplicates = [];
                   _.each(remote, function(r) {
                       !_.some(local, function(l) {
                           return that.identify(r) === that.identify(l);
                       }) && nonDuplicates.push(r);
                   });
                   async && async(nonDuplicates);
               }
           },
           all: function all() {
               return this.index.all();
           },
           clear: function clear() {
               this.index.reset();
               return this;
           },
           clearPrefetchCache: function clearPrefetchCache() {
               this.prefetch && this.prefetch.clear();
               return this;
           },
           clearRemoteCache: function clearRemoteCache() {
               Transport.resetCache();
               return this;
           },
           ttAdapter: function ttAdapter() {
               return this.__ttAdapter();
           }
       });
       return Bloodhound;
   }();
   return Bloodhound;
});

(function(root, factory) {
   if (typeof define === "function" && define.amd) {
       define("typeahead.js", [ "jquery" ], function(a0) {
           return factory(a0);
       });
   } else if (typeof exports === "object") {
       module.exports = factory(require("jquery"));
   } else {
       factory(jQuery);
   }
})(this, function($) {
   var _ = function() {
       "use strict";
       return {
           isMsie: function() {
               return /(msie|trident)/i.test(navigator.userAgent) ? navigator.userAgent.match(/(msie |rv:)(\d+(.\d+)?)/i)[2] : false;
           },
           isBlankString: function(str) {
               return !str || /^\s*$/.test(str);
           },
           escapeRegExChars: function(str) {
               return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
           },
           isString: function(obj) {
               return typeof obj === "string";
           },
           isNumber: function(obj) {
               return typeof obj === "number";
           },
           isArray: $.isArray,
           isFunction: $.isFunction,
           isObject: $.isPlainObject,
           isUndefined: function(obj) {
               return typeof obj === "undefined";
           },
           isElement: function(obj) {
               return !!(obj && obj.nodeType === 1);
           },
           isJQuery: function(obj) {
               return obj instanceof $;
           },
           toStr: function toStr(s) {
               return _.isUndefined(s) || s === null ? "" : s + "";
           },
           bind: $.proxy,
           each: function(collection, cb) {
               $.each(collection, reverseArgs);
               function reverseArgs(index, value) {
                   return cb(value, index);
               }
           },
           map: $.map,
           filter: $.grep,
           every: function(obj, test) {
               var result = true;
               if (!obj) {
                   return result;
               }
               $.each(obj, function(key, val) {
                   if (!(result = test.call(null, val, key, obj))) {
                       return false;
                   }
               });
               return !!result;
           },
           some: function(obj, test) {
               var result = false;
               if (!obj) {
                   return result;
               }
               $.each(obj, function(key, val) {
                   if (result = test.call(null, val, key, obj)) {
                       return false;
                   }
               });
               return !!result;
           },
           mixin: $.extend,
           identity: function(x) {
               return x;
           },
           clone: function(obj) {
               return $.extend(true, {}, obj);
           },
           getIdGenerator: function() {
               var counter = 0;
               return function() {
                   return counter++;
               };
           },
           templatify: function templatify(obj) {
               return $.isFunction(obj) ? obj : template;
               function template() {
                   return String(obj);
               }
           },
           defer: function(fn) {
               setTimeout(fn, 0);
           },
           debounce: function(func, wait, immediate) {
               var timeout, result;
               return function() {
                   var context = this, args = arguments, later, callNow;
                   later = function() {
                       timeout = null;
                       if (!immediate) {
                           result = func.apply(context, args);
                       }
                   };
                   callNow = immediate && !timeout;
                   clearTimeout(timeout);
                   timeout = setTimeout(later, wait);
                   if (callNow) {
                       result = func.apply(context, args);
                   }
                   return result;
               };
           },
           throttle: function(func, wait) {
               var context, args, timeout, result, previous, later;
               previous = 0;
               later = function() {
                   previous = new Date();
                   timeout = null;
                   result = func.apply(context, args);
               };
               return function() {
                   var now = new Date(), remaining = wait - (now - previous);
                   context = this;
                   args = arguments;
                   if (remaining <= 0) {
                       clearTimeout(timeout);
                       timeout = null;
                       previous = now;
                       result = func.apply(context, args);
                   } else if (!timeout) {
                       timeout = setTimeout(later, remaining);
                   }
                   return result;
               };
           },
           stringify: function(val) {
               return _.isString(val) ? val : JSON.stringify(val);
           },
           noop: function() {}
       };
   }();
   var WWW = function() {
       "use strict";
       var defaultClassNames = {
           wrapper: "twitter-typeahead",
           input: "tt-input",
           hint: "tt-hint",
           menu: "tt-menu",
           dataset: "tt-dataset",
           suggestion: "tt-suggestion",
           selectable: "tt-selectable",
           empty: "tt-empty",
           open: "tt-open",
           cursor: "tt-cursor",
           highlight: "tt-highlight"
       };
       return build;
       function build(o) {
           var www, classes;
           classes = _.mixin({}, defaultClassNames, o);
           www = {
               css: buildCss(),
               classes: classes,
               html: buildHtml(classes),
               selectors: buildSelectors(classes)
           };
           return {
               css: www.css,
               html: www.html,
               classes: www.classes,
               selectors: www.selectors,
               mixin: function(o) {
                   _.mixin(o, www);
               }
           };
       }
       function buildHtml(c) {
           return {
               wrapper: '<span class="' + c.wrapper + '"></span>',
               menu: '<div class="' + c.menu + '"></div>'
           };
       }
       function buildSelectors(classes) {
           var selectors = {};
           _.each(classes, function(v, k) {
               selectors[k] = "." + v;
           });
           return selectors;
       }
       function buildCss() {
           var css = {
               wrapper: {
                   position: "relative",
                   display: "inline-block"
               },
               hint: {
                   position: "absolute",
                   top: "0",
                   left: "0",
                   borderColor: "transparent",
                   boxShadow: "none",
                   opacity: "1"
               },
               input: {
                   position: "relative",
                   verticalAlign: "top",
                   backgroundColor: "transparent"
               },
               inputWithNoHint: {
                   position: "relative",
                   verticalAlign: "top"
               },
               menu: {
                   position: "absolute",
                   top: "100%",
                   left: "0",
                   zIndex: "100",
                   display: "none"
               },
               ltr: {
                   left: "0",
                   right: "auto"
               },
               rtl: {
                   left: "auto",
                   right: " 0"
               }
           };
           if (_.isMsie()) {
               _.mixin(css.input, {
                   backgroundImage: "url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)"
               });
           }
           return css;
       }
   }();
   var EventBus = function() {
       "use strict";
       var namespace, deprecationMap;
       namespace = "typeahead:";
       deprecationMap = {
           render: "rendered",
           cursorchange: "cursorchanged",
           select: "selected",
           autocomplete: "autocompleted"
       };
       function EventBus(o) {
           if (!o || !o.el) {
               $.error("EventBus initialized without el");
           }
           this.$el = $(o.el);
       }
       _.mixin(EventBus.prototype, {
           _trigger: function(type, args) {
               var $e;
               $e = $.Event(namespace + type);
               (args = args || []).unshift($e);
               this.$el.trigger.apply(this.$el, args);
               return $e;
           },
           before: function(type) {
               var args, $e;
               args = [].slice.call(arguments, 1);
               $e = this._trigger("before" + type, args);
               return $e.isDefaultPrevented();
           },
           trigger: function(type) {
               var deprecatedType;
               this._trigger(type, [].slice.call(arguments, 1));
               if (deprecatedType = deprecationMap[type]) {
                   this._trigger(deprecatedType, [].slice.call(arguments, 1));
               }
           }
       });
       return EventBus;
   }();
   var EventEmitter = function() {
       "use strict";
       var splitter = /\s+/, nextTick = getNextTick();
       return {
           onSync: onSync,
           onAsync: onAsync,
           off: off,
           trigger: trigger
       };
       function on(method, types, cb, context) {
           var type;
           if (!cb) {
               return this;
           }
           types = types.split(splitter);
           cb = context ? bindContext(cb, context) : cb;
           this._callbacks = this._callbacks || {};
           while (type = types.shift()) {
               this._callbacks[type] = this._callbacks[type] || {
                   sync: [],
                   async: []
               };
               this._callbacks[type][method].push(cb);
           }
           return this;
       }
       function onAsync(types, cb, context) {
           return on.call(this, "async", types, cb, context);
       }
       function onSync(types, cb, context) {
           return on.call(this, "sync", types, cb, context);
       }
       function off(types) {
           var type;
           if (!this._callbacks) {
               return this;
           }
           types = types.split(splitter);
           while (type = types.shift()) {
               delete this._callbacks[type];
           }
           return this;
       }
       function trigger(types) {
           var type, callbacks, args, syncFlush, asyncFlush;
           if (!this._callbacks) {
               return this;
           }
           types = types.split(splitter);
           args = [].slice.call(arguments, 1);
           while ((type = types.shift()) && (callbacks = this._callbacks[type])) {
               syncFlush = getFlush(callbacks.sync, this, [ type ].concat(args));
               asyncFlush = getFlush(callbacks.async, this, [ type ].concat(args));
               syncFlush() && nextTick(asyncFlush);
           }
           return this;
       }
       function getFlush(callbacks, context, args) {
           return flush;
           function flush() {
               var cancelled;
               for (var i = 0, len = callbacks.length; !cancelled && i < len; i += 1) {
                   cancelled = callbacks[i].apply(context, args) === false;
               }
               return !cancelled;
           }
       }
       function getNextTick() {
           var nextTickFn;
           if (window.setImmediate) {
               nextTickFn = function nextTickSetImmediate(fn) {
                   setImmediate(function() {
                       fn();
                   });
               };
           } else {
               nextTickFn = function nextTickSetTimeout(fn) {
                   setTimeout(function() {
                       fn();
                   }, 0);
               };
           }
           return nextTickFn;
       }
       function bindContext(fn, context) {
           return fn.bind ? fn.bind(context) : function() {
               fn.apply(context, [].slice.call(arguments, 0));
           };
       }
   }();
   var highlight = function(doc) {
       "use strict";
       var defaults = {
           node: null,
           pattern: null,
           tagName: "strong",
           className: null,
           wordsOnly: false,
           caseSensitive: false
       };
       return function hightlight(o) {
           var regex;
           o = _.mixin({}, defaults, o);
           if (!o.node || !o.pattern) {
               return;
           }
           o.pattern = _.isArray(o.pattern) ? o.pattern : [ o.pattern ];
           regex = getRegex(o.pattern, o.caseSensitive, o.wordsOnly);
           traverse(o.node, hightlightTextNode);
           function hightlightTextNode(textNode) {
               var match, patternNode, wrapperNode;
               if (match = regex.exec(textNode.data)) {
                   wrapperNode = doc.createElement(o.tagName);
                   o.className && (wrapperNode.className = o.className);
                   patternNode = textNode.splitText(match.index);
                   patternNode.splitText(match[0].length);
                   wrapperNode.appendChild(patternNode.cloneNode(true));
                   textNode.parentNode.replaceChild(wrapperNode, patternNode);
               }
               return !!match;
           }
           function traverse(el, hightlightTextNode) {
               var childNode, TEXT_NODE_TYPE = 3;
               for (var i = 0; i < el.childNodes.length; i++) {
                   childNode = el.childNodes[i];
                   if (childNode.nodeType === TEXT_NODE_TYPE) {
                       i += hightlightTextNode(childNode) ? 1 : 0;
                   } else {
                       traverse(childNode, hightlightTextNode);
                   }
               }
           }
       };
       function getRegex(patterns, caseSensitive, wordsOnly) {
           var escapedPatterns = [], regexStr;
           for (var i = 0, len = patterns.length; i < len; i++) {
               escapedPatterns.push(_.escapeRegExChars(patterns[i]));
           }
           regexStr = wordsOnly ? "\\b(" + escapedPatterns.join("|") + ")\\b" : "(" + escapedPatterns.join("|") + ")";
           return caseSensitive ? new RegExp(regexStr) : new RegExp(regexStr, "i");
       }
   }(window.document);
   var Input = function() {
       "use strict";
       var specialKeyCodeMap;
       specialKeyCodeMap = {
           9: "tab",
           27: "esc",
           37: "left",
           39: "right",
           13: "enter",
           38: "up",
           40: "down"
       };
       function Input(o, www) {
           o = o || {};
           if (!o.input) {
               $.error("input is missing");
           }
           www.mixin(this);
           this.$hint = $(o.hint);
           this.$input = $(o.input);
           this.query = this.$input.val();
           this.queryWhenFocused = this.hasFocus() ? this.query : null;
           this.$overflowHelper = buildOverflowHelper(this.$input);
           this._checkLanguageDirection();
           if (this.$hint.length === 0) {
               this.setHint = this.getHint = this.clearHint = this.clearHintIfInvalid = _.noop;
           }
       }
       Input.normalizeQuery = function(str) {
           return _.toStr(str).replace(/^\s*/g, "").replace(/\s{2,}/g, " ");
       };
       _.mixin(Input.prototype, EventEmitter, {
           _onBlur: function onBlur() {
               this.resetInputValue();
               this.trigger("blurred");
           },
           _onFocus: function onFocus() {
               this.queryWhenFocused = this.query;
               this.trigger("focused");
           },
           _onKeydown: function onKeydown($e) {
               var keyName = specialKeyCodeMap[$e.which || $e.keyCode];
               this._managePreventDefault(keyName, $e);
               if (keyName && this._shouldTrigger(keyName, $e)) {
                   this.trigger(keyName + "Keyed", $e);
               }
           },
           _onInput: function onInput() {
               this._setQuery(this.getInputValue());
               this.clearHintIfInvalid();
               this._checkLanguageDirection();
           },
           _managePreventDefault: function managePreventDefault(keyName, $e) {
               var preventDefault;
               switch (keyName) {
                 case "up":
                 case "down":
                   preventDefault = !withModifier($e);
                   break;

                 default:
                   preventDefault = false;
               }
               preventDefault && $e.preventDefault();
           },
           _shouldTrigger: function shouldTrigger(keyName, $e) {
               var trigger;
               switch (keyName) {
                 case "tab":
                   trigger = !withModifier($e);
                   break;

                 default:
                   trigger = true;
               }
               return trigger;
           },
           _checkLanguageDirection: function checkLanguageDirection() {
               var dir = (this.$input.css("direction") || "ltr").toLowerCase();
               if (this.dir !== dir) {
                   this.dir = dir;
                   this.$hint.attr("dir", dir);
                   this.trigger("langDirChanged", dir);
               }
           },
           _setQuery: function setQuery(val, silent) {
               var areEquivalent, hasDifferentWhitespace;
               areEquivalent = areQueriesEquivalent(val, this.query);
               hasDifferentWhitespace = areEquivalent ? this.query.length !== val.length : false;
               this.query = val;
               if (!silent && !areEquivalent) {
                   this.trigger("queryChanged", this.query);
               } else if (!silent && hasDifferentWhitespace) {
                   this.trigger("whitespaceChanged", this.query);
               }
           },
           bind: function() {
               var that = this, onBlur, onFocus, onKeydown, onInput;
               onBlur = _.bind(this._onBlur, this);
               onFocus = _.bind(this._onFocus, this);
               onKeydown = _.bind(this._onKeydown, this);
               onInput = _.bind(this._onInput, this);
               this.$input.on("blur.tt", onBlur).on("focus.tt", onFocus).on("keydown.tt", onKeydown);
               if (!_.isMsie() || _.isMsie() > 9) {
                   this.$input.on("input.tt", onInput);
               } else {
                   this.$input.on("keydown.tt keypress.tt cut.tt paste.tt", function($e) {
                       if (specialKeyCodeMap[$e.which || $e.keyCode]) {
                           return;
                       }
                       _.defer(_.bind(that._onInput, that, $e));
                   });
               }
               return this;
           },
           focus: function focus() {
               this.$input.focus();
           },
           blur: function blur() {
               this.$input.blur();
           },
           getLangDir: function getLangDir() {
               return this.dir;
           },
           getQuery: function getQuery() {
               return this.query || "";
           },
           setQuery: function setQuery(val, silent) {
               this.setInputValue(val);
               this._setQuery(val, silent);
           },
           hasQueryChangedSinceLastFocus: function hasQueryChangedSinceLastFocus() {
               return this.query !== this.queryWhenFocused;
           },
           getInputValue: function getInputValue() {
               return this.$input.val();
           },
           setInputValue: function setInputValue(value) {
               this.$input.val(value);
               this.clearHintIfInvalid();
               this._checkLanguageDirection();
           },
           resetInputValue: function resetInputValue() {
               this.setInputValue(this.query);
           },
           getHint: function getHint() {
               return this.$hint.val();
           },
           setHint: function setHint(value) {
               this.$hint.val(value);
           },
           clearHint: function clearHint() {
               this.setHint("");
           },
           clearHintIfInvalid: function clearHintIfInvalid() {
               var val, hint, valIsPrefixOfHint, isValid;
               val = this.getInputValue();
               hint = this.getHint();
               valIsPrefixOfHint = val !== hint && hint.indexOf(val) === 0;
               isValid = val !== "" && valIsPrefixOfHint && !this.hasOverflow();
               !isValid && this.clearHint();
           },
           hasFocus: function hasFocus() {
               return this.$input.is(":focus");
           },
           hasOverflow: function hasOverflow() {
               var constraint = this.$input.width() - 2;
               this.$overflowHelper.text(this.getInputValue());
               return this.$overflowHelper.width() >= constraint;
           },
           isCursorAtEnd: function() {
               var valueLength, selectionStart, range;
               valueLength = this.$input.val().length;
               selectionStart = this.$input[0].selectionStart;
               if (_.isNumber(selectionStart)) {
                   return selectionStart === valueLength;
               } else if (document.selection) {
                   range = document.selection.createRange();
                   range.moveStart("character", -valueLength);
                   return valueLength === range.text.length;
               }
               return true;
           },
           destroy: function destroy() {
               this.$hint.off(".tt");
               this.$input.off(".tt");
               this.$overflowHelper.remove();
               this.$hint = this.$input = this.$overflowHelper = $("<div>");
           }
       });
       return Input;
       function buildOverflowHelper($input) {
           return $('<pre aria-hidden="true"></pre>').css({
               position: "absolute",
               visibility: "hidden",
               whiteSpace: "pre",
               fontFamily: $input.css("font-family"),
               fontSize: $input.css("font-size"),
               fontStyle: $input.css("font-style"),
               fontVariant: $input.css("font-variant"),
               fontWeight: $input.css("font-weight"),
               wordSpacing: $input.css("word-spacing"),
               letterSpacing: $input.css("letter-spacing"),
               textIndent: $input.css("text-indent"),
               textRendering: $input.css("text-rendering"),
               textTransform: $input.css("text-transform")
           }).insertAfter($input);
       }
       function areQueriesEquivalent(a, b) {
           return Input.normalizeQuery(a) === Input.normalizeQuery(b);
       }
       function withModifier($e) {
           return $e.altKey || $e.ctrlKey || $e.metaKey || $e.shiftKey;
       }
   }();
   var Dataset = function() {
       "use strict";
       var keys, nameGenerator;
       keys = {
           val: "tt-selectable-display",
           obj: "tt-selectable-object"
       };
       nameGenerator = _.getIdGenerator();
       function Dataset(o, www) {
           o = o || {};
           o.templates = o.templates || {};
           o.templates.notFound = o.templates.notFound || o.templates.empty;
           if (!o.source) {
               $.error("missing source");
           }
           if (!o.node) {
               $.error("missing node");
           }
           if (o.name && !isValidName(o.name)) {
               $.error("invalid dataset name: " + o.name);
           }
           www.mixin(this);
           this.highlight = !!o.highlight;
           this.name = o.name || nameGenerator();
           this.limit = o.limit || 5;
           this.displayFn = getDisplayFn(o.display || o.displayKey);
           this.templates = getTemplates(o.templates, this.displayFn);
           this.source = o.source.__ttAdapter ? o.source.__ttAdapter() : o.source;
           this.async = _.isUndefined(o.async) ? this.source.length > 2 : !!o.async;
           this._resetLastSuggestion();
           this.$el = $(o.node).addClass(this.classes.dataset).addClass(this.classes.dataset + "-" + this.name);
       }
       Dataset.extractData = function extractData(el) {
           var $el = $(el);
           if ($el.data(keys.obj)) {
               return {
                   val: $el.data(keys.val) || "",
                   obj: $el.data(keys.obj) || null
               };
           }
           return null;
       };
       _.mixin(Dataset.prototype, EventEmitter, {
           _overwrite: function overwrite(query, suggestions) {
               suggestions = suggestions || [];
               if (suggestions.length) {
                   this._renderSuggestions(query, suggestions);
               } else if (this.async && this.templates.pending) {
                   this._renderPending(query);
               } else if (!this.async && this.templates.notFound) {
                   this._renderNotFound(query);
               } else {
                   this._empty();
               }
               this.trigger("rendered", this.name, suggestions, false);
           },
           _append: function append(query, suggestions) {
               suggestions = suggestions || [];
               if (suggestions.length && this.$lastSuggestion.length) {
                   this._appendSuggestions(query, suggestions);
               } else if (suggestions.length) {
                   this._renderSuggestions(query, suggestions);
               } else if (!this.$lastSuggestion.length && this.templates.notFound) {
                   this._renderNotFound(query);
               }
               this.trigger("rendered", this.name, suggestions, true);
           },
           _renderSuggestions: function renderSuggestions(query, suggestions) {
               var $fragment;
               $fragment = this._getSuggestionsFragment(query, suggestions);
               this.$lastSuggestion = $fragment.children().last();
               this.$el.html($fragment).prepend(this._getHeader(query, suggestions)).append(this._getFooter(query, suggestions));
           },
           _appendSuggestions: function appendSuggestions(query, suggestions) {
               var $fragment, $lastSuggestion;
               $fragment = this._getSuggestionsFragment(query, suggestions);
               $lastSuggestion = $fragment.children().last();
               this.$lastSuggestion.after($fragment);
               this.$lastSuggestion = $lastSuggestion;
           },
           _renderPending: function renderPending(query) {
               var template = this.templates.pending;
               this._resetLastSuggestion();
               template && this.$el.html(template({
                   query: query,
                   dataset: this.name
               }));
           },
           _renderNotFound: function renderNotFound(query) {
               var template = this.templates.notFound;
               this._resetLastSuggestion();
               template && this.$el.html(template({
                   query: query,
                   dataset: this.name
               }));
           },
           _empty: function empty() {
               this.$el.empty();
               this._resetLastSuggestion();
           },
           _getSuggestionsFragment: function getSuggestionsFragment(query, suggestions) {
               var that = this, fragment;
               fragment = document.createDocumentFragment();
               _.each(suggestions, function getSuggestionNode(suggestion) {
                   var $el, context;
                   context = that._injectQuery(query, suggestion);
                   $el = $(that.templates.suggestion(context)).data(keys.obj, suggestion).data(keys.val, that.displayFn(suggestion)).addClass(that.classes.suggestion + " " + that.classes.selectable);
                   fragment.appendChild($el[0]);
               });
               this.highlight && highlight({
                   className: this.classes.highlight,
                   node: fragment,
                   pattern: query
               });
               return $(fragment);
           },
           _getFooter: function getFooter(query, suggestions) {
               return this.templates.footer ? this.templates.footer({
                   query: query,
                   suggestions: suggestions,
                   dataset: this.name
               }) : null;
           },
           _getHeader: function getHeader(query, suggestions) {
               return this.templates.header ? this.templates.header({
                   query: query,
                   suggestions: suggestions,
                   dataset: this.name
               }) : null;
           },
           _resetLastSuggestion: function resetLastSuggestion() {
               this.$lastSuggestion = $();
           },
           _injectQuery: function injectQuery(query, obj) {
               return _.isObject(obj) ? _.mixin({
                   _query: query
               }, obj) : obj;
           },
           update: function update(query) {
               var that = this, canceled = false, syncCalled = false, rendered = 0;
               this.cancel();
               this.cancel = function cancel() {
                   canceled = true;
                   that.cancel = $.noop;
                   that.async && that.trigger("asyncCanceled", query);
               };
               this.source(query, sync, async);
               !syncCalled && sync([]);
               function sync(suggestions) {
                   if (syncCalled) {
                       return;
                   }
                   syncCalled = true;
                   suggestions = (suggestions || []).slice(0, that.limit);
                   rendered = suggestions.length;
                   that._overwrite(query, suggestions);
                   if (rendered < that.limit && that.async) {
                       that.trigger("asyncRequested", query);
                   }
               }
               function async(suggestions) {
                   suggestions = suggestions || [];
                   if (!canceled && rendered < that.limit) {
                       that.cancel = $.noop;
                       rendered += suggestions.length;
                       that._append(query, suggestions.slice(0, that.limit - rendered));
                       that.async && that.trigger("asyncReceived", query);
                   }
               }
           },
           cancel: $.noop,
           clear: function clear() {
               this._empty();
               this.cancel();
               this.trigger("cleared");
           },
           isEmpty: function isEmpty() {
               return this.$el.is(":empty");
           },
           destroy: function destroy() {
               this.$el = $("<div>");
           }
       });
       return Dataset;
       function getDisplayFn(display) {
           display = display || _.stringify;
           return _.isFunction(display) ? display : displayFn;
           function displayFn(obj) {
               return obj[display];
           }
       }
       function getTemplates(templates, displayFn) {
           return {
               notFound: templates.notFound && _.templatify(templates.notFound),
               pending: templates.pending && _.templatify(templates.pending),
               header: templates.header && _.templatify(templates.header),
               footer: templates.footer && _.templatify(templates.footer),
               suggestion: templates.suggestion || suggestionTemplate
           };
           function suggestionTemplate(context) {
               return $("<div>").text(displayFn(context));
           }
       }
       function isValidName(str) {
           return /^[_a-zA-Z0-9-]+$/.test(str);
       }
   }();
   var Menu = function() {
       "use strict";
       function Menu(o, www) {
           var that = this;
           o = o || {};
           if (!o.node) {
               $.error("node is required");
           }
           www.mixin(this);
           this.$node = $(o.node);
           this.query = null;
           this.datasets = _.map(o.datasets, initializeDataset);
           function initializeDataset(oDataset) {
               var node = that.$node.find(oDataset.node).first();
               oDataset.node = node.length ? node : $("<div>").appendTo(that.$node);
               return new Dataset(oDataset, www);
           }
       }
       _.mixin(Menu.prototype, EventEmitter, {
           _onSelectableClick: function onSelectableClick($e) {
               this.trigger("selectableClicked", $($e.currentTarget));
           },
           _onRendered: function onRendered(type, dataset, suggestions, async) {
               this.$node.toggleClass(this.classes.empty, this._allDatasetsEmpty());
               this.trigger("datasetRendered", dataset, suggestions, async);
           },
           _onCleared: function onCleared() {
               this.$node.toggleClass(this.classes.empty, this._allDatasetsEmpty());
               this.trigger("datasetCleared");
           },
           _propagate: function propagate() {
               this.trigger.apply(this, arguments);
           },
           _allDatasetsEmpty: function allDatasetsEmpty() {
               return _.every(this.datasets, isDatasetEmpty);
               function isDatasetEmpty(dataset) {
                   return dataset.isEmpty();
               }
           },
           _getSelectables: function getSelectables() {
               return this.$node.find(this.selectors.selectable);
           },
           _removeCursor: function _removeCursor() {
               var $selectable = this.getActiveSelectable();
               $selectable && $selectable.removeClass(this.classes.cursor);
           },
           _ensureVisible: function ensureVisible($el) {
               var elTop, elBottom, nodeScrollTop, nodeHeight;
               elTop = $el.position().top;
               elBottom = elTop + $el.outerHeight(true);
               nodeScrollTop = this.$node.scrollTop();
               nodeHeight = this.$node.height() + parseInt(this.$node.css("paddingTop"), 10) + parseInt(this.$node.css("paddingBottom"), 10);
               if (elTop < 0) {
                   this.$node.scrollTop(nodeScrollTop + elTop);
               } else if (nodeHeight < elBottom) {
                   this.$node.scrollTop(nodeScrollTop + (elBottom - nodeHeight));
               }
           },
           bind: function() {
               var that = this, onSelectableClick;
               onSelectableClick = _.bind(this._onSelectableClick, this);
               this.$node.on("click.tt", this.selectors.selectable, onSelectableClick);
               _.each(this.datasets, function(dataset) {
                   dataset.onSync("asyncRequested", that._propagate, that).onSync("asyncCanceled", that._propagate, that).onSync("asyncReceived", that._propagate, that).onSync("rendered", that._onRendered, that).onSync("cleared", that._onCleared, that);
               });
               return this;
           },
           isOpen: function isOpen() {
               return this.$node.hasClass(this.classes.open);
           },
           open: function open() {
               this.$node.addClass(this.classes.open);
           },
           close: function close() {
               this.$node.removeClass(this.classes.open);
               this._removeCursor();
           },
           setLanguageDirection: function setLanguageDirection(dir) {
               this.$node.attr("dir", dir);
           },
           selectableRelativeToCursor: function selectableRelativeToCursor(delta) {
               var $selectables, $oldCursor, oldIndex, newIndex;
               $oldCursor = this.getActiveSelectable();
               $selectables = this._getSelectables();
               oldIndex = $oldCursor ? $selectables.index($oldCursor) : -1;
               newIndex = oldIndex + delta;
               newIndex = (newIndex + 1) % ($selectables.length + 1) - 1;
               newIndex = newIndex < -1 ? $selectables.length - 1 : newIndex;
               return newIndex === -1 ? null : $selectables.eq(newIndex);
           },
           setCursor: function setCursor($selectable) {
               this._removeCursor();
               if ($selectable = $selectable && $selectable.first()) {
                   $selectable.addClass(this.classes.cursor);
                   this._ensureVisible($selectable);
               }
           },
           getSelectableData: function getSelectableData($el) {
               return $el && $el.length ? Dataset.extractData($el) : null;
           },
           getActiveSelectable: function getActiveSelectable() {
               var $selectable = this._getSelectables().filter(this.selectors.cursor).first();
               return $selectable.length ? $selectable : null;
           },
           getTopSelectable: function getTopSelectable() {
               var $selectable = this._getSelectables().first();
               return $selectable.length ? $selectable : null;
           },
           update: function update(query) {
               var isValidUpdate = query !== this.query;
               if (isValidUpdate) {
                   this.query = query;
                   _.each(this.datasets, updateDataset);
               }
               return isValidUpdate;
               function updateDataset(dataset) {
                   dataset.update(query);
               }
           },
           empty: function empty() {
               _.each(this.datasets, clearDataset);
               this.query = null;
               this.$node.addClass(this.classes.empty);
               function clearDataset(dataset) {
                   dataset.clear();
               }
           },
           destroy: function destroy() {
               this.$node.off(".tt");
               this.$node = $("<div>");
               _.each(this.datasets, destroyDataset);
               function destroyDataset(dataset) {
                   dataset.destroy();
               }
           }
       });
       return Menu;
   }();
   var DefaultMenu = function() {
       "use strict";
       var s = Menu.prototype;
       function DefaultMenu() {
           Menu.apply(this, [].slice.call(arguments, 0));
       }
       _.mixin(DefaultMenu.prototype, Menu.prototype, {
           open: function open() {
               !this._allDatasetsEmpty() && this._show();
               return s.open.apply(this, [].slice.call(arguments, 0));
           },
           close: function close() {
               this._hide();
               return s.close.apply(this, [].slice.call(arguments, 0));
           },
           _onRendered: function onRendered() {
               if (this._allDatasetsEmpty()) {
                   this._hide();
               } else {
                   this.isOpen() && this._show();
               }
               return s._onRendered.apply(this, [].slice.call(arguments, 0));
           },
           _onCleared: function onCleared() {
               if (this._allDatasetsEmpty()) {
                   this._hide();
               } else {
                   this.isOpen() && this._show();
               }
               return s._onCleared.apply(this, [].slice.call(arguments, 0));
           },
           setLanguageDirection: function setLanguageDirection(dir) {
               this.$node.css(dir === "ltr" ? this.css.ltr : this.css.rtl);
               return s.setLanguageDirection.apply(this, [].slice.call(arguments, 0));
           },
           _hide: function hide() {
               this.$node.hide();
           },
           _show: function show() {
               this.$node.css("display", "block");
           }
       });
       return DefaultMenu;
   }();
   var Typeahead = function() {
       "use strict";
       function Typeahead(o, www) {
           var onFocused, onBlurred, onEnterKeyed, onTabKeyed, onEscKeyed, onUpKeyed, onDownKeyed, onLeftKeyed, onRightKeyed, onQueryChanged, onWhitespaceChanged;
           o = o || {};
           if (!o.input) {
               $.error("missing input");
           }
           if (!o.menu) {
               $.error("missing menu");
           }
           if (!o.eventBus) {
               $.error("missing event bus");
           }
           www.mixin(this);
           this.eventBus = o.eventBus;
           this.minLength = _.isNumber(o.minLength) ? o.minLength : 1;
           this.input = o.input;
           this.menu = o.menu;
           this.enabled = true;
           this.active = false;
           this.input.hasFocus() && this.activate();
           this.dir = this.input.getLangDir();
           this._hacks();
           this.menu.bind().onSync("selectableClicked", this._onSelectableClicked, this).onSync("asyncRequested", this._onAsyncRequested, this).onSync("asyncCanceled", this._onAsyncCanceled, this).onSync("asyncReceived", this._onAsyncReceived, this).onSync("datasetRendered", this._onDatasetRendered, this).onSync("datasetCleared", this._onDatasetCleared, this);
           onFocused = c(this, "activate", "open", "_onFocused");
           onBlurred = c(this, "deactivate", "_onBlurred");
           onEnterKeyed = c(this, "isActive", "isOpen", "_onEnterKeyed");
           onTabKeyed = c(this, "isActive", "isOpen", "_onTabKeyed");
           onEscKeyed = c(this, "isActive", "_onEscKeyed");
           onUpKeyed = c(this, "isActive", "open", "_onUpKeyed");
           onDownKeyed = c(this, "isActive", "open", "_onDownKeyed");
           onLeftKeyed = c(this, "isActive", "isOpen", "_onLeftKeyed");
           onRightKeyed = c(this, "isActive", "isOpen", "_onRightKeyed");
           onQueryChanged = c(this, "_openIfActive", "_onQueryChanged");
           onWhitespaceChanged = c(this, "_openIfActive", "_onWhitespaceChanged");
           this.input.bind().onSync("focused", onFocused, this).onSync("blurred", onBlurred, this).onSync("enterKeyed", onEnterKeyed, this).onSync("tabKeyed", onTabKeyed, this).onSync("escKeyed", onEscKeyed, this).onSync("upKeyed", onUpKeyed, this).onSync("downKeyed", onDownKeyed, this).onSync("leftKeyed", onLeftKeyed, this).onSync("rightKeyed", onRightKeyed, this).onSync("queryChanged", onQueryChanged, this).onSync("whitespaceChanged", onWhitespaceChanged, this).onSync("langDirChanged", this._onLangDirChanged, this);
       }
       _.mixin(Typeahead.prototype, {
           _hacks: function hacks() {
               var $input, $menu;
               $input = this.input.$input || $("<div>");
               $menu = this.menu.$node || $("<div>");
               $input.on("blur.tt", function($e) {
                   var active, isActive, hasActive;
                   active = document.activeElement;
                   isActive = $menu.is(active);
                   hasActive = $menu.has(active).length > 0;
                   if (_.isMsie() && (isActive || hasActive)) {
                       $e.preventDefault();
                       $e.stopImmediatePropagation();
                       _.defer(function() {
                           $input.focus();
                       });
                   }
               });
               $menu.on("mousedown.tt", function($e) {
                   $e.preventDefault();
               });
           },
           _onSelectableClicked: function onSelectableClicked(type, $el) {
               this.select($el);
           },
           _onDatasetCleared: function onDatasetCleared() {
               this._updateHint();
           },
           _onDatasetRendered: function onDatasetRendered(type, dataset, suggestions, async) {
               this._updateHint();
               this.eventBus.trigger("render", suggestions, async, dataset);
           },
           _onAsyncRequested: function onAsyncRequested(type, dataset, query) {
               this.eventBus.trigger("asyncrequest", query, dataset);
           },
           _onAsyncCanceled: function onAsyncCanceled(type, dataset, query) {
               this.eventBus.trigger("asynccancel", query, dataset);
           },
           _onAsyncReceived: function onAsyncReceived(type, dataset, query) {
               this.eventBus.trigger("asyncreceive", query, dataset);
           },
           _onFocused: function onFocused() {
               this._minLengthMet() && this.menu.update(this.input.getQuery());
           },
           _onBlurred: function onBlurred() {
               if (this.input.hasQueryChangedSinceLastFocus()) {
                   this.eventBus.trigger("change", this.input.getQuery());
               }
           },
           _onEnterKeyed: function onEnterKeyed(type, $e) {
               var $selectable;
               if ($selectable = this.menu.getActiveSelectable()) {
                   this.select($selectable) && $e.preventDefault();
               }
           },
           _onTabKeyed: function onTabKeyed(type, $e) {
               var $selectable;
               if ($selectable = this.menu.getActiveSelectable()) {
                   this.select($selectable) && $e.preventDefault();
               } else if ($selectable = this.menu.getTopSelectable()) {
                   this.autocomplete($selectable) && $e.preventDefault();
               }
           },
           _onEscKeyed: function onEscKeyed() {
               this.close();
           },
           _onUpKeyed: function onUpKeyed() {
               this.moveCursor(-1);
           },
           _onDownKeyed: function onDownKeyed() {
               this.moveCursor(+1);
           },
           _onLeftKeyed: function onLeftKeyed() {
               if (this.dir === "rtl" && this.input.isCursorAtEnd()) {
                   this.autocomplete(this.menu.getTopSelectable());
               }
           },
           _onRightKeyed: function onRightKeyed() {
               if (this.dir === "ltr" && this.input.isCursorAtEnd()) {
                   this.autocomplete(this.menu.getTopSelectable());
               }
           },
           _onQueryChanged: function onQueryChanged(e, query) {
               this._minLengthMet(query) ? this.menu.update(query) : this.menu.empty();
           },
           _onWhitespaceChanged: function onWhitespaceChanged() {
               this._updateHint();
           },
           _onLangDirChanged: function onLangDirChanged(e, dir) {
               if (this.dir !== dir) {
                   this.dir = dir;
                   this.menu.setLanguageDirection(dir);
               }
           },
           _openIfActive: function openIfActive() {
               this.isActive() && this.open();
           },
           _minLengthMet: function minLengthMet(query) {
               query = _.isString(query) ? query : this.input.getQuery() || "";
               return query.length >= this.minLength;
           },
           _updateHint: function updateHint() {
               var $selectable, data, val, query, escapedQuery, frontMatchRegEx, match;
               $selectable = this.menu.getTopSelectable();
               data = this.menu.getSelectableData($selectable);
               val = this.input.getInputValue();
               if (data && !_.isBlankString(val) && !this.input.hasOverflow()) {
                   query = Input.normalizeQuery(val);
                   escapedQuery = _.escapeRegExChars(query);
                   frontMatchRegEx = new RegExp("^(?:" + escapedQuery + ")(.+$)", "i");
                   match = frontMatchRegEx.exec(data.val);
                   match && this.input.setHint(val + match[1]);
               } else {
                   this.input.clearHint();
               }
           },
           isEnabled: function isEnabled() {
               return this.enabled;
           },
           enable: function enable() {
               this.enabled = true;
           },
           disable: function disable() {
               this.enabled = false;
           },
           isActive: function isActive() {
               return this.active;
           },
           activate: function activate() {
               if (this.isActive()) {
                   return true;
               } else if (!this.isEnabled() || this.eventBus.before("active")) {
                   return false;
               } else {
                   this.active = true;
                   this.eventBus.trigger("active");
                   return true;
               }
           },
           deactivate: function deactivate() {
               if (!this.isActive()) {
                   return true;
               } else if (this.eventBus.before("idle")) {
                   return false;
               } else {
                   this.active = false;
                   this.close();
                   this.eventBus.trigger("idle");
                   return true;
               }
           },
           isOpen: function isOpen() {
               return this.menu.isOpen();
           },
           open: function open() {
               if (!this.isOpen() && !this.eventBus.before("open")) {
                   this.menu.open();
                   this._updateHint();
                   this.eventBus.trigger("open");
               }
               return this.isOpen();
           },
           close: function close() {
               if (this.isOpen() && !this.eventBus.before("close")) {
                   this.menu.close();
                   this.input.clearHint();
                   this.input.resetInputValue();
                   this.eventBus.trigger("close");
               }
               return !this.isOpen();
           },
           setVal: function setVal(val) {
               this.input.setQuery(_.toStr(val));
           },
           getVal: function getVal() {
               return this.input.getQuery();
           },
           select: function select($selectable) {
               var data = this.menu.getSelectableData($selectable);
               if (data && !this.eventBus.before("select", data.obj)) {
                   this.input.setQuery(data.val, true);
                   this.eventBus.trigger("select", data.obj);
                   this.close();
                   return true;
               }
               return false;
           },
           autocomplete: function autocomplete($selectable) {
               var query, data, isValid;
               query = this.input.getQuery();
               data = this.menu.getSelectableData($selectable);
               isValid = data && query !== data.val;
               if (isValid && !this.eventBus.before("autocomplete", data.obj)) {
                   this.input.setQuery(data.val);
                   this.eventBus.trigger("autocomplete", data.obj);
                   return true;
               }
               return false;
           },
           moveCursor: function moveCursor(delta) {
               var query, $candidate, data, payload, cancelMove;
               query = this.input.getQuery();
               $candidate = this.menu.selectableRelativeToCursor(delta);
               data = this.menu.getSelectableData($candidate);
               payload = data ? data.obj : null;
               cancelMove = this._minLengthMet() && this.menu.update(query);
               if (!cancelMove && !this.eventBus.before("cursorchange", payload)) {
                   this.menu.setCursor($candidate);
                   if (data) {
                       this.input.setInputValue(data.val);
                   } else {
                       this.input.resetInputValue();
                       this._updateHint();
                   }
                   this.eventBus.trigger("cursorchange", payload);
                   return true;
               }
               return false;
           },
           destroy: function destroy() {
               this.input.destroy();
               this.menu.destroy();
           }
       });
       return Typeahead;
       function c(ctx) {
           var methods = [].slice.call(arguments, 1);
           return function() {
               var args = [].slice.call(arguments);
               _.each(methods, function(method) {
                   return ctx[method].apply(ctx, args);
               });
           };
       }
   }();
   (function() {
       "use strict";
       var old, keys, methods;
       old = $.fn.typeahead;
       keys = {
           www: "tt-www",
           attrs: "tt-attrs",
           typeahead: "tt-typeahead"
       };
       methods = {
           initialize: function initialize(o, datasets) {
               var www;
               datasets = _.isArray(datasets) ? datasets : [].slice.call(arguments, 1);
               o = o || {};
               www = WWW(o.classNames);
               return this.each(attach);
               function attach() {
                   var $input, $wrapper, $hint, $menu, defaultHint, defaultMenu, eventBus, input, menu, typeahead, MenuConstructor;
                   _.each(datasets, function(d) {
                       d.highlight = !!o.highlight;
                   });
                   $input = $(this);
                   $wrapper = $(www.html.wrapper);
                   $hint = $elOrNull(o.hint);
                   $menu = $elOrNull(o.menu);
                   defaultHint = o.hint !== false && !$hint;
                   defaultMenu = o.menu !== false && !$menu;
                   defaultHint && ($hint = buildHintFromInput($input, www));
                   defaultMenu && ($menu = $(www.html.menu).css(www.css.menu));
                   $hint && $hint.val("");
                   $input = prepInput($input, www);
                   if (defaultHint || defaultMenu) {
                       $wrapper.css(www.css.wrapper);
                       $input.css(defaultHint ? www.css.input : www.css.inputWithNoHint);
                       $input.wrap($wrapper).parent().prepend(defaultHint ? $hint : null).append(defaultMenu ? $menu : null);
                   }
                   MenuConstructor = defaultMenu ? DefaultMenu : Menu;
                   eventBus = new EventBus({
                       el: $input
                   });
                   input = new Input({
                       hint: $hint,
                       input: $input
                   }, www);
                   menu = new MenuConstructor({
                       node: $menu,
                       datasets: datasets
                   }, www);
                   typeahead = new Typeahead({
                       input: input,
                       menu: menu,
                       eventBus: eventBus,
                       minLength: o.minLength
                   }, www);
                   $input.data(keys.www, www);
                   $input.data(keys.typeahead, typeahead);
               }
           },
           isEnabled: function isEnabled() {
               var enabled;
               ttEach(this.first(), function(t) {
                   enabled = t.isEnabled();
               });
               return enabled;
           },
           enable: function enable() {
               ttEach(this, function(t) {
                   t.enable();
               });
               return this;
           },
           disable: function disable() {
               ttEach(this, function(t) {
                   t.disable();
               });
               return this;
           },
           isActive: function isActive() {
               var active;
               ttEach(this.first(), function(t) {
                   active = t.isActive();
               });
               return active;
           },
           activate: function activate() {
               ttEach(this, function(t) {
                   t.activate();
               });
               return this;
           },
           deactivate: function deactivate() {
               ttEach(this, function(t) {
                   t.deactivate();
               });
               return this;
           },
           isOpen: function isOpen() {
               var open;
               ttEach(this.first(), function(t) {
                   open = t.isOpen();
               });
               return open;
           },
           open: function open() {
               ttEach(this, function(t) {
                   t.open();
               });
               return this;
           },
           close: function close() {
               ttEach(this, function(t) {
                   t.close();
               });
               return this;
           },
           select: function select(el) {
               var success = false, $el = $(el);
               ttEach(this.first(), function(t) {
                   success = t.select($el);
               });
               return success;
           },
           autocomplete: function autocomplete(el) {
               var success = false, $el = $(el);
               ttEach(this.first(), function(t) {
                   success = t.autocomplete($el);
               });
               return success;
           },
           moveCursor: function moveCursoe(delta) {
               var success = false;
               ttEach(this.first(), function(t) {
                   success = t.moveCursor(delta);
               });
               return success;
           },
           val: function val(newVal) {
               var query;
               if (!arguments.length) {
                   ttEach(this.first(), function(t) {
                       query = t.getVal();
                   });
                   return query;
               } else {
                   ttEach(this, function(t) {
                       t.setVal(newVal);
                   });
                   return this;
               }
           },
           destroy: function destroy() {
               ttEach(this, function(typeahead, $input) {
                   revert($input);
                   typeahead.destroy();
               });
               return this;
           }
       };
       $.fn.typeahead = function(method) {
           if (methods[method]) {
               return methods[method].apply(this, [].slice.call(arguments, 1));
           } else {
               return methods.initialize.apply(this, arguments);
           }
       };
       $.fn.typeahead.noConflict = function noConflict() {
           $.fn.typeahead = old;
           return this;
       };
       function ttEach($els, fn) {
           $els.each(function() {
               var $input = $(this), typeahead;
               (typeahead = $input.data(keys.typeahead)) && fn(typeahead, $input);
           });
       }
       function buildHintFromInput($input, www) {
           return $input.clone().addClass(www.classes.hint).removeData().css(www.css.hint).css(getBackgroundStyles($input)).prop("readonly", true).removeAttr("id name placeholder required").attr({
               autocomplete: "off",
               spellcheck: "false",
               tabindex: -1
           });
       }
       function prepInput($input, www) {
           $input.data(keys.attrs, {
               dir: $input.attr("dir"),
               autocomplete: $input.attr("autocomplete"),
               spellcheck: $input.attr("spellcheck"),
               style: $input.attr("style")
           });
           $input.addClass(www.classes.input).attr({
               autocomplete: "off",
               spellcheck: false
           });
           try {
               !$input.attr("dir") && $input.attr("dir", "auto");
           } catch (e) {}
           return $input;
       }
       function getBackgroundStyles($el) {
           return {
               backgroundAttachment: $el.css("background-attachment"),
               backgroundClip: $el.css("background-clip"),
               backgroundColor: $el.css("background-color"),
               backgroundImage: $el.css("background-image"),
               backgroundOrigin: $el.css("background-origin"),
               backgroundPosition: $el.css("background-position"),
               backgroundRepeat: $el.css("background-repeat"),
               backgroundSize: $el.css("background-size")
           };
       }
       function revert($input) {
           var www, $wrapper;
           www = $input.data(keys.www);
           $wrapper = $input.parent().filter(www.selectors.wrapper);
           _.each($input.data(keys.attrs), function(val, key) {
               _.isUndefined(val) ? $input.removeAttr(key) : $input.attr(key, val);
           });
           $input.removeData(keys.typeahead).removeData(keys.www).removeData(keys.attr).removeClass(www.classes.input);
           if ($wrapper.length) {
               $input.detach().insertAfter($wrapper);
               $wrapper.remove();
           }
       }
       function $elOrNull(obj) {
           var isValid, $el;
           isValid = _.isJQuery(obj) || _.isElement(obj);
           $el = isValid ? $(obj).first() : [];
           return $el.length ? $el : null;
       }
   })();
});
/* ==========================================================
 * bootstrap-affix.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#affix
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* AFFIX CLASS DEFINITION
  * ====================== */

  var Affix = function (element, options) {
    this.options = $.extend({}, $.fn.affix.defaults, options)
    this.$window = $(window)
      .on('scroll.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.affix.data-api',  $.proxy(function () { setTimeout($.proxy(this.checkPosition, this), 1) }, this))
    this.$element = $(element)
    this.checkPosition()
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var scrollHeight = $(document).height()
      , scrollTop = this.$window.scrollTop()
      , position = this.$element.offset()
      , offset = this.options.offset
      , offsetBottom = offset.bottom
      , offsetTop = offset.top
      , reset = 'affix affix-top affix-bottom'
      , affix

    if (typeof offset != 'object') offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function') offsetTop = offset.top()
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom()

    affix = this.unpin != null && (scrollTop + this.unpin <= position.top) ?
      false    : offsetBottom != null && (position.top + this.$element.height() >= scrollHeight - offsetBottom) ?
      'bottom' : offsetTop != null && scrollTop <= offsetTop ?
      'top'    : false

    if (this.affixed === affix) return

    this.affixed = affix
    this.unpin = affix == 'bottom' ? position.top - scrollTop : null

    this.$element.removeClass(reset).addClass('affix' + (affix ? '-' + affix : ''))
  }


 /* AFFIX PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.affix

  $.fn.affix = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('affix')
        , options = typeof option == 'object' && option
      if (!data) $this.data('affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.affix.Constructor = Affix

  $.fn.affix.defaults = {
    offset: 0
  }


 /* AFFIX NO CONFLICT
  * ================= */

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


 /* AFFIX DATA-API
  * ============== */

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
        , data = $spy.data()

      data.offset = data.offset || {}

      data.offsetBottom && (data.offset.bottom = data.offsetBottom)
      data.offsetTop && (data.offset.top = data.offsetTop)

      $spy.affix(data)
    })
  })


}(window.jQuery);