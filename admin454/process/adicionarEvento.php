<?php
@session_start();
include_once('../../conf/config.php');
if($_POST['acao'] == 'editar'){


	
	$id_agendamento  = (filter_input(INPUT_POST, 'id_agendamento', FILTER_SANITIZE_SPECIAL_CHARS));
	$bind = array(
			":id_agendamento" => $id_agendamento
	);
	$custom_agenda = $db->select("agendamento", "id_agendamento =  :id_agendamento ", $bind);

	if($custom_agenda){

		$custom_agenda = $custom_agenda[0];

		$consultor = $custom_agenda['id_consultor'];

		$dtInicio = $custom_agenda['inicio_agendamento'];
		$dtFim = $custom_agenda['fim_agendamento'];
		$inicio = (date('d/m/Y H:i:s' , strtotime($dtInicio)));
		$fim =  (date('d/m/Y H:i:s' , strtotime($dtFim)));
		$inicio_banco = (date('Y-m-d H:i:s' , strtotime($dtInicio)));
		$fim_banco =  (date('Y-m-d H:i:s' , strtotime($dtFim)));
		$valor_agendamento = str_replace('.',',',$custom_agenda['valor_agendamento']);
	}


}else{
$dtInicio = str_replace('GMT+0000','',$_POST ['dtIni']);
$dtFim = str_replace('GMT+0000','',$_POST ['dtFim']);


$inicio = (date('d/m/Y H:i:s' , strtotime($dtInicio)));
$fim =  (date('d/m/Y H:i:s' , strtotime($dtFim)));


$inicio_banco = (date('Y-m-d H:i:s' , strtotime($dtInicio)));
$fim_banco =  (date('Y-m-d H:i:s' , strtotime($dtFim)));

$consultor = base64_decode($_POST['consultor']);

$config = $db->select('configuracoes');
$config = $config[0];

$valor_agendamento = $config['valor_consulta'];

}
?>
<script>


($(function(){
$("#valor_agendamento").inputmask('R$ 999,99' , { numericInput: true });
$("#hora_agendamento").inputmask('hh:mm:ss' , { "clearIncomplete": true , clearMaskOnLostFocus: true });
$("#dlgcancel").click(function(){
$( "#editdialog" ).dialog( "close" );
})
$("#hora_agendamento").focus();
}))

function checkForm(objectform){
	var dados  = {'id_agendamento': $("#id_agendamento").val(),'inicio_agendamento': $("#inicio_agendamento").val(), 'fim_agendamento': $("#fim_agendamento").val(), 'valor_agendamento': $("#valor_agendamento").val() , 	'consultor': $("#consultor").val()}

		id = 'abcc' ;// saveMyData(dados);

		console.log(dados);

		horaI = new String((dados.inicio_agendamento)).split(' ');
		horaI = horaI[1];

		horaF = new String((dados.fim_agendamento)).split(' ');
		horaF = horaF[1];

// 		calendar.fullCalendar('renderEvent', {
// 			 title: 'Agendamento: '+ ' de  ' + horaI + ' até ' + horaF,
// 			 start: dados.inicio_agendamento,
// 			 end: dados.fim_agendamento ,

// 			 allDay: false,
// 			 id: id
// 		 }, true);


	 $.post('process/salvarEvento.php' , dados , function(data) {

		 $( "#editdialog" ).dialog( "close" );
		 calendar.fullCalendar( 'refetchEvents' );
		 //document.location='?action-grano-filter=consultor_agenda&consultor='+$("#consultor").val()

		  })
	 return false;

}
</script>
<form role='form' onsubmit="return checkForm(this)">
	<div class="box-body">
	<input type="hidden" name="id_agendamento" id="id_agendamento" value = "<?php echo $id_agendamento;?>">
		<input type="hidden" name="consultor" id="consultor" value = "<?php echo $consultor;?>">
		<input type="hidden" name="fim_agendamento" id="fim_agendamento" value = "<?php echo $fim_banco;?>">
		<input type="hidden" name="inicio_agendamento" id="inicio_agendamento" value = "<?php echo $inicio_banco;?>">
		<div class="form-group ">
			<label>Início do agendamento:</label> <input type="text" class="form-control"
				name='inicio' id='inicio' value="<?php echo $inicio ;?>" readonly="">
		</div>
		<!-- /.form group -->



		<!-- time Picker -->
		<div class="form-group ">
			<label>Término do agendamento:</label> <input type="text" class="form-control"
				name='final' id='final' value="<?php echo $fim ;?>" readonly="">
		</div>

<!-- 		<div class="bootstrap-timepicker form-group "> -->
<!-- 			<div class="form-group"> -->
<!-- 				<label>Valor:</label> -->
<!-- 				<div class="input-group col-xs-5"> -->
					<input type="hidden" name='valor_agendamento' id='valor_agendamento'
						class="form-control" required value="<?php echo $valor_agendamento ;?>">

<!-- 				</div> -->
				<!-- /.input group -->
<!-- 			</div> -->
			<!-- /.form group -->
<!-- 		</div> -->

	</div>
	<div class="box-footer">
		<button class="btn btn-primary" type="submit">Criar agendamento</button>
	</div>
	</div>
	<!-- /.box-body -->
</form>
<!-- /.box -->
<script>
      $(function () {

        $(".timepicker").timepicker({
          showInputs: false
        });
				$("#valor_agendamento").focus();
      });
    </script>