<?php

include_once('../../conf/config.php');
$consultor  = base64_decode(filter_input(INPUT_GET, 'consultor', FILTER_SANITIZE_SPECIAL_CHARS));
$bind = array(
    ":id_consultor" => $consultor
);
$custom_agenda = $db->select("agendamento", "id_consultor =  :id_consultor and date(inicio_agendamento) >= date(NOW()) ", $bind);

$events = array();

if($custom_agenda){

foreach ($custom_agenda as $key => $value) {

  $e = array();
        $e['id'] = $value['id_agendamento'];
       
        
        $agendaIni = date('H:i:s',strtotime($value['inicio_agendamento']));
        $agendaFim = date('H:i:s',strtotime($value['fim_agendamento']));
        $e['title'] = 'Agendamento: de '. $agendaIni . ' até '. $agendaFim . ' - (R$ ' . $value['valor_agendamento'].')';
        $e['start'] = $value['inicio_agendamento'];
        $e['end'] = $value['fim_agendamento'];       
        $e['allDay'] = false;

        // Merge the event array into the return array
        array_push($events, $e);


}
echo json_encode($events);
exit;

}