<?php

include_once('../../conf/config.php');
include_once('Redimensionar.php');
$tmp_name = ($_FILES['file']['tmp_name']);
$name = uniqid('GRANO').($_FILES['file']['name']);

if($_GET['foto_consultor_antiga'] != ''){
	@unlink('../dist/img/'.$_GET['foto_consultor_antiga']);
}

if(move_uploaded_file($tmp_name,'../tmpGallery/'.$name)){
  $mask = umask();
  umask(0);
  chmod('../tmpGallery/'.$name,0777);
  $red = new Redimensionar;
  $red->AdicionaTamanho(2000);  // adiciona o primeiro tamanho pra fazer a proporção

  $red->AdicionaExtensoes('jpeg'); // adiciona uma extensão pra procurar arquivos pra redimensionar
  $red->AdicionaExtensoes('jpg'); // adiciona uma extensão pra procurar arquivos pra redimensionar
  $red->AdicionaExtensoes('png'); // adiciona uma extensão pra procurar arquivos pra redimensionar
  $red->__set('ArqOrigem','../tmpGallery/'.$name); // adiciona um diretório  de onde está o arquivo pra conversão
    $red->__set('DirDestino','../dist/img/'); // adiciona um diretório de onde será colocado os arquivos
  $convertidos = $red->executar();  // retorna em um array os arquivos redimensionados , se quiser gravar no banco os  thumbs ou algo do tipo


  $explode_path       = explode('/',$convertidos[0])     ;
  $nomeArquivo = $explode_path[count($explode_path)-1];
  @unlink('../tmpGallery/'.$name);
  echo $nomeArquivo;
}
?>
