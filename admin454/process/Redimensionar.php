<?php
class Redimensionar
{
	protected $ArqOrigem;
	protected $DirDestino;
	private $Tamanhos;
	private $Extensoes;

	function __get($campo)
	{
		return $this->$campo;
	}
	function __set($campo,$valor)
	{
		$this->$campo = $valor;
		return true;
	}


	function __construct()
	{
		$this->Tamanhos = array();
		$this->Extensoes = array();
	}

	/* Adiciona os tamanhos a serem convertidos */
	function AdicionaTamanho($tam)
	{
		$chave = false;
		foreach($this->Tamanhos as $tamList)
		{
			if($tamList == $tam)
			{
				$chave = true ;
				return false;
			}
		}
		if(!$chave)
		{
			$this->Tamanhos[] = $tam;
			return true;
		}
	}

	/* Retira um tamanho da fila */
	function RetiraTamanho($tam)
	{
		$chave = false;
		foreach ($this->Tamanhos as $Ind=>$tamList)
		{
			if($tamList == $tam)
			{
				$indice = $Ind;
				unset($this->Tamanhos[$indice]);
				break;
			}
		}
	}

	/* Pega todos os tamanhos existentes na fila */
	function PegaTamanhos()
	{
		return $this->Tamanhos;
	}

	/* Adiciona as extensões nos arquivos */
	function AdicionaExtensoes($ext)
	{
		$chave = false;
		foreach($this->Extensoes as $extList)
		{
			if($extList == $ext)
			{
				$chave = true ;
				return false;
			}
		}
		if(!$chave)
		{
			$this->Extensoes[] = $ext;
			return true;
		}
	}

	/* Retira as extensões dos arquivos */
	function RetiraExtensoes($ext)
	{
		$chave = false;
		foreach ($this->Extensoes as $Ind=>$extlist)
		{
			if($extlist == $ext)
			{
				$indice = $Ind;
				unset($this->Extensoes[$indice]);
				break;
			}
		}
	}

	/* Pega as extensões dos arquivos */
	function PegaExtensoes()
	{
		return $this->Extensoes;
	}

	/* Executa a conversão das imagens */
	function executar()
	{

		@chmod($this->DirDestino,0777);
		$arquivos  = array();
		$DestFile = $this->ArqOrigem;

		$extensoes = implode("|",$this->Extensoes);

		$contador = 1 ;
		if(preg_match("($extensoes)", $DestFile,$ext))
		{

			$ext = $ext[0];

			switch(strtolower($ext))
			{
				case "jpg":
				case "jpeg":
				if(function_exists('imagecreatefromjpeg')){
					$imagem_orig = imagecreatefromjpeg(trim($DestFile));
				}else{
					$cript = $DestFile.uniqid();
					copy($DestFile , $this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".jpg" ) ;
					$meusarquivos[] = $this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".jpg";
					return $meusarquivos;
				}
				break;
				case "png":
				$imagem_orig = imagecreatefrompng($DestFile);
				break;
				case "gif":
					$imagem_orig = imagecreatefromgif($DestFile);
					break;
					default:
					if(function_exists('imagecreatefromjpeg')){
						$imagem_orig = imagecreatefromjpeg(trim($DestFile));
					}else{
						$cript = $DestFile.uniqid();
						copy($DestFile , $this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".jpg" ) ;
						$meusarquivos[] = $this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".jpg";
						return $meusarquivos;
					}
					break;
				}
				$thumb_largura    =   @imagesx($imagem_orig);
				$thumb_altura	  =   @imagesy($imagem_orig);


				foreach($this->Tamanhos as $redim)
				{
					if($thumb_largura < $redim  && $thumb_altura < $redim){
						$larguraT = $thumb_largura;
						$alturaT  = $thumb_altura;
					}else{
					if($thumb_largura > $thumb_altura){
						$alturaT = floor($redim * ($thumb_altura / $thumb_largura));
					}else{
						$alturaT = $redim;
					}
					if($thumb_largura < $thumb_altura){
						$larguraT = floor($redim * ($thumb_largura / $thumb_altura));
					}else{
						$larguraT = $redim;
					}
				}


					$imagem_fin = @imagecreatetruecolor($larguraT, $alturaT);
					$cript = $DestFile.uniqid();
					if($ext == 'png'){
						imagecolortransparent($imagem_fin, imagecolorallocatealpha($imagem_fin, 0, 0, 0, 127));
	 					imagealphablending($imagem_fin, false);
	 					imagesavealpha($imagem_fin, true);
						imagecopyresampled($imagem_fin, $imagem_orig, 0, 0, 0, 0, $larguraT, $alturaT, $thumb_largura, $thumb_altura);
						imagepng($imagem_fin,$this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".png",9);
						$meusarquivos[] = $this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".png";
					}else{
							imagecopyresampled($imagem_fin, $imagem_orig, 0, 0, 0, 0, $larguraT, $alturaT, $thumb_largura, $thumb_altura);
					imagepng($imagem_fin,$this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".png",9);
				}
					$meusarquivos[] = $this->DirDestino.'/'.$_SERVER['HOSTNAME']."_".md5($cript).".png";
					$contador++;
				}
			}

			/* Volta para a permissao normal */
			//@chmod($this->DirDestino,0444);
			return $meusarquivos;
		}
	}
	?>