<?php
session_start();
include_once ('conf/config.php');
$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode($usuario);
$cliente = $db->select ( 'cliente', ' id_cliente = ' . $jsonUsuario->id_cliente);
$cliente = $cliente [0];
foreach($cliente as $idx=>$cli){
	$$idx = $cli;
}
$nascimento_cliente = date('d/m/Y',strtotime($nascimento_cliente));
if ($cliente ['facebookid'] != '') {
	if (file_exists ( 'admin454/dist/img/' . $cliente ['foto_cliente'] )) {
		$imagemCli = 'admin454/dist/img/' . $cliente ['foto_cliente'];
	} else {
		$imagemCli = $cliente ['foto_cliente'];
	}
} // Imagem do face
else {
	if($cliente ['foto_cliente'] == ''){
			$imagemCli = 'admin454/dist/img/nophoto.jpg';
	}else{
	if (file_exists ( 'admin454/dist/img/' . $cliente ['foto_cliente'] )) {
		$imagemCli = 'admin454/dist/img/' . $cliente ['foto_cliente'];
	} else {
		$imagemCli = 'admin454/dist/img/nophoto.jpg';
	}
}
}
$agentamento_last = $db->select('agendamento_cliente', 'id_cliente = '.$id_cliente . ' and finalizado_agendamento = 0 order by data_agendamento_cliente DESC limit 1 ');
$agentamento_last = $agentamento_last[0];

$agenda = $db->select('agendamento', 'id_agendamento = '. $agentamento_last['id_agendamento']);
$agenda = $agenda[0];
$consultor = $db->select('consultor', 'id_consultor = '. $agenda['id_consultor']);
$consultor= $consultor[0];


$agentamento_todas = $db->select('agendamento_cliente', 'id_cliente = '.$id_cliente . '  order by data_agendamento_cliente DESC limit 10 ');

$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode ( $usuario );
$cliente = $db->select ( 'cliente', 'id_cliente = ' . $jsonUsuario->id_cliente );

$cliente = $cliente [0];

$bind = array (
	':id_cliente' => $cliente ['id_cliente']
	);
$pagamentos = $db->select ( 'pagamento', 'id_cliente = :id_cliente and status_pagamento is not null order by data_pagamento DESC limit 30', $bind );

if ($pagamentos [0]) {




	foreach ( $pagamentos as $pagamento ) {

		$status_pagamento = $db->select('status_pagamento' , 'id_status = '.$pagamento['status_pagamento']);
		$status_pagamento = $status_pagamento[0];

		if($status_pagamento['nome_status']){
			$strStatus = $status_pagamento['nome_status'];
		}else{
			$strStatus = $pagamento['status_pagamento'];
		}

		$infos [strtotime ( $pagamento ['data_pagamento'] )] [] = array (
			'id_info' => $pagamento ['id_pagamento'],
			'data' => date ( 'd/m/Y H:i:s', strtotime ( $pagamento ['data_pagamento'] ) ),
			'info' => $pagamento ['item_pagamento'],
			'status' => ($pagamento ['status_pagamento']) ?  $pagamento ['status_pagamento'] : 'x',
			'str_status' => ($pagamento ['status_pagamento']) ?  $strStatus : '',
			'forma_pagamento' => $pagamento ['forma_pagamento'],
			'transaction_id' => $pagamento ['numero_pagamento'] . ' ('.$pagamento['transacao_pagamento'].')',
			'tipo' => 'C'
			);
	}
}


$agendamento_cliente = $db->select ( 'agendamento_cliente', 'id_cliente = :id_cliente order by data_agendamento_cliente', $bind );
if($agendamento_cliente[0]){

	foreach($agendamento_cliente as $ag_cli){

		$agenda = $db->select('agendamento', 'id_agendamento = '.$ag_cli['id_agendamento']);
		$agenda = $agenda[0];
		$consultor = $db->select('consultor' , 'id_consultor = '. $agenda['id_consultor']);
		$consultor = $consultor[0];

		$infos [strtotime ( $ag_cli ['data_agendamento_cliente'] )] [] = array (
			'id_info' =>  $ag_cli ['id_agendamento_cliente'],
			'data' => date ( 'd/m/Y H:i:s', strtotime ( $ag_cli ['data_agendamento_cliente'] ) ),
			'info' => "Consulta com <strong>{$consultor['nome_consultor']}</strong><br>  ".date('d/m/Y',strtotime($agenda['inicio_agendamento']))." (".date('H:i',strtotime($agenda['inicio_agendamento']))."-".date('H:i',strtotime($agenda['fim_agendamento'])).")",
			'status' => $ag_cli ['finalizado_agendamento'],
			'nota_agendamento' => $ag_cli ['nota_agendamento'],
			'tipo' => 'D'
			);
	}

}
if($infos){
	ksort($infos);

	$infos = array_reverse($infos);
}
?>
<section id="areaCliente">
	<div class="container">
		<div class="row ">
			<div class="col-md-3 area-cliente-perfil">
				<img class="img-circle img-responsive center-block" src="<?php echo $imagemCli;?>" alt="">
				<h3><?php echo $nome_cliente;?></h3>
				<p>Saldo atual: R$ <?php  echo number_format($saldo_cliente,2,',','.') ;?></p>
				<a href="carregarCredito.php" onclick="return false" role="button"  class='btn-acao-dados-cliente btn btn-default' >Adicionar saldo <!--img src='img/add-credito.png'  style='border:0px;width:24px;height:24px' width="24"--></a>
				<a href="register.php?id_cliente=<?php echo $cliente['id_cliente'];?>" role="button" class="btn-acao-dados-cliente btn btn-default" onclick='return false'>Configurações da conta</a>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12">
						<h4>Próxima consulta</h4>
					</div>
				</div>
				<?php if($agentamento_last) { ?>
					<div class="row">
						<div class="col-md-2">
							<div class="dataConsulta">
								<div class="mes"><?php echo strftime ( '%b', strtotime($agentamento_last['data_agendamento_cliente'])) ;?></div>
								<div class="dia"><?php echo date('d' , strtotime($agentamento_last['data_agendamento_cliente'])) ;?></div>
							</div>
						</div>
						<div class="col-md-8">
							<p>
								<span>Consultor:</span> <?php echo $consultor['nome_consultor']. ' '. $consultor['sobrenome_consultor'] ;?><br>
								<span>Skype:</span> <?php echo $consultor['skype_consultor'] ;?><br>
								<span>Horário:</span> <?php echo utf8_encode(ucfirst(strftime ( '%A %H:%M', strtotime($agentamento_last['data_agendamento_cliente'])))) ;?>
							</p>
						</div>
						<div class="col-md-12" style="display:none">
							<button type="button" class="btn btn-default btn-md btn-block" >Reagendar</button>
						</div>
					</div>


					<?php } ?>

					<ul class="nav nav-tabs">

						<li class="active"><a data-toggle="tab" href="#menu1">Últimas consultas</a></li>
						<li><a data-toggle="tab" href="#menu2">Extrato das últimas transações</a></li>
					</ul>

					<div class="tab-content">

						<div id="menu1" class="tab-pane fade in active">
							<div class="row">
								<div class="col-md-12 ultimasConsultas">
									<h4>Últimas consultas</h4>
									<table class="table table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>Data da consulta</th>
												<th>Consulta</th>
												<th>Finalizada</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($agentamento_todas as $idx=>$agendamento_lista)  {

												$agenda = $db->select('agendamento', 'id_agendamento = '. $agendamento_lista['id_agendamento']);
												$agenda = $agenda[0];
												$strAgenda = date('d/m/Y', strtotime($agenda['inicio_agendamento'])). ' ' . date('H:i:s', strtotime($agenda['inicio_agendamento'])) . ' às '.date('H:i:s', strtotime($agenda['fim_agendamento']));

												if($agendamento_lista['finalizado_agendamento'] == 1){
													$classAguardando = 'class="alert alert-success"';
												}else{
													$classAguardando = '';
												}


												?>
												<tr <?php echo $classAguardando ;?> onclick="avaliarAgendamento('<?php echo base64_encode($agendamento_lista['id_agendamento']);?>' , '<?php echo base64_encode($agendamento_lista['id_agendamento_cliente']);?>' ,'<?php echo $agendamento_lista['finalizado_agendamento'] ;?>' , '<?php echo $agendamento_lista['nota_agendamento']; ?>')">
													<th scope="row"><?php echo ($idx + 1 );?></th>
													<td><?php echo $strAgenda ;?></td>
													<td><?php echo ($agendamento_lista['agendamento_sala_criada'] == 1 ? '<a href="javascript:void(0)" class="iniciarAgendamento" agendamento='.$agendamento_lista['id_agendamento'].'><strong>Iniciar</strong></span>': 'Não') ; ?></td>

													<td><?php echo ($agendamento_lista['finalizado_agendamento'] == 1 ?  ($agendamento_lista['nota_agendamento'] ? 'Nota: '.$agendamento_lista['nota_agendamento']: 'Avaliar' ) : 'Não') ; ?></td>

												</tr>
												<? } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div id="menu2" class="tab-pane fade">


								<div class="col-md-12 ultimasConsultas">
									<table class="table table-hover">
										<tbody>
											<tr>

												<th>Data</th>
												<th>Descrição</th>
												<th>Status</th>

											</tr>
											<?php 

											if($infos){
												foreach ($infos as $info ){
													foreach($info as $extrato){
														$classTR = '';
														$titleTR = '';
														switch($extrato['status']){

															case 0:
															if($extrato['tipo'] == 'C'){
																$classTR = 'class="alert alert-danger"';
																$titleTR = 'title="Transação abandonada"';
																$status_str = '<span class="label label-warning">'.$extrato['str_status'].'</span>';

															}else{
																$status_str = '<span class="label alert-info">Aguardando a consulta</span>';
															}
															break;

															case 'x':
															$classTR = 'class="alert alert-danger"';
															$titleTR = 'title="Transação abandonada"';
															$status_str = '<span class="label label-success">'.$extrato['status'].'</span>';
															break;
															case 1:
															if($extrato['tipo'] == 'C'){
																$status_str = '<span class="label label-warning">'.$extrato['str_status'].'</span>';
																$titleTR = 'title="'.$extrato['transaction_id'].'"';

															}else{
																if($extrato['nota_agendamento'] >= 0 && $extrato['nota_agendamento'] <= 3 ){
																	$img_estrela = '0.png';
																}elseif($extrato['nota_agendamento'] > 3 && $extrato['nota_agendamento'] <= 6){

																	$img_estrela = '1.png';
																}

																elseif($extrato['nota_agendamento'] > 6 && $extrato['nota_agendamento'] <= 8){

																	$img_estrela = '2.png';
																}else{
																	$img_estrela = '3.png';
																}

																$status_str  = '<span class="label label-success"> Finalizada</span>';

															}
															break;

															case 3:
															if($extrato['tipo'] == 'C'){
																$titleTR = 'title="'.$extrato['transaction_id'].'"';
																$status_str = '<span class="label label-success">'.$extrato['str_status'].'</span>';

															}
															break;

															default:
															if($extrato['tipo'] == 'C'){
																$titleTR = 'title="'.$extrato['transaction_id'].'"';
																$status_str = '<span class="label label-danger">'.$extrato['str_status'].'</span>';

															}
															break;

														}
														?>
														<tr <?php echo $classTR; ?> <?php echo $titleTR;?>>

															<td nowrap class='text-left'><?php echo $extrato['data'];?></td>
															<td nowrap class='text-left'><?php echo $extrato['info'];?></td>
															<td class='text-left'><?php echo $status_str; ?></td>

														</tr>
														<?php } } }?>
													</tbody>
												</table>
											</div>

										</div>
									</div>




									<div class="row">
										<div class="col-md-12">
											<!--button type="button" class="btn btn-primary btn-lg btn-block">Agendar nova consulta</button-->
											<button type="button" class="btn-acao-dados-cliente btn btn-default btn-md btn-block" href="header_register.php" onclick="return false">Agendar nova consulta</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</section>
					<!--Script para background nav-->
					<script type="text/javascript">
						$(window).on("scroll", function() {
							if($(this).scrollTop() > 80) {
								$("header").addClass("cor-header");
							} else {
								$("header").removeClass("cor-header");
							}
						});
  //Script anchor
  (function($){

  	$('#menu a').on('click tap',function(){
					//Toggle Class

					if($(this).attr('role') != null){
						return true;
					}
					//Animate
					$('html, body').stop().animate({
						scrollTop: $( $(this).attr('href') ).offset().top
					}, 400);
					return false;
				});
  	$('.scrollTop a').scrollTop();


  	$(".btn-acao-dados-cliente").on('click',function() {

  		href_link = ($(this).attr('href'))

  		if(href_link != '#'){

  			$(".modal-body").load('loading.php',function(){
  				$('#modalSystem').modal('show');
  				$('.modal-footer').show();
  				$(".modal-body").load(href_link,function(){
  					$('#modalSystem').modal('show');
  				})
  			})
  		}
					//$(this).find(".modal-body").load(link.attr("href"));
				});

  	$(".iniciarAgendamento").click(function(){

  		var telaPopup = null;	
       	var agendamento = $(this).attr('agendamento');
        telaPopup = window.open ('https://apprtc.appspot.com/r/Consulta_'+agendamento+'?video=true&stereo=true','consulta','width=600,height=500,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no');

        if(!telaPopup){
            alert('É necessário que habilite o pop-up em seu navegador');
        }else{
          
        }



  	})
  })(jQuery);

($(function(){

			$('#modalSystem').on('hidden.bs.modal', function () {
			  //  document.location='index.php';
			})

			}))
  function efetivarPagamento(codTpag){
  	$('.modal-footer').hide();
  	$(".modal-body").load('loading.php',function(){
  		$('#loginModal').modal('show');
  	})





  	$.post('process/preparaPagseguro.php' , {cod_pagamento : codTpag} , function(data){

  		$('#loginModal').modal('hide');
  		if(data == 'Unauthorized'){
  			alert('Pagamento não autorizado');
  		}else{

  			checkoutCode = data;
  			PagSeguroLightbox({
  				code: checkoutCode
  			}, {
  				success : function(transactionCode) {
  					$(".modal-body").load('loading.php',function(){
  						$('#loginModal').modal('show');
  					})
  					$.post('paymentCompleted.php', {transaction_id:transactionCode},function(data){
  						$('.modal-footer').show();

  						$(".modal-body").load('invoice.php?transaction_id='+transactionCode,function(){
  							$('#loginModal').modal('show');
  						})

  					});
  				},
  				abort : function() {

  				}
  			});

  		}

  	})

  }

  function avaliarAgendamento(id_agendamento,id_agendamento_cliente,avaliar,nota){



  		if(avaliar == 1){
  			if(nota == ''){
  			$(".modal-body").load('loading.php', function() {
						$('#modalSystem').modal('show');
						$('.modal-footer').show();
						 $(".modal-body").load('avaliarAgendamento.php?agendamento='+id_agendamento+'&agendamento_cliente='+id_agendamento_cliente, function() {
							$('#modalSystem').modal('show');
						})
					})
  			}else{
  				alert('A consulta avaliada anteriormente.')
  			}
  	}
  }


</script>
</body>
</html>
