<?php

/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */



require 'facebook/facebook-php-sdk/src/facebook.php';
include_once ('conf/config.php');



$facebook = new Facebook(array(
  'appId'  => '465523473647316',
  'secret' => '09cf0eefca221077a674e1b21514b4a1',
  ));

if(isset($_GET['logoutFacebook'])){
  session_destroy();

  $facebook->destroySession();
  header('location:register.php');
  exit;
}


// Create our Application instance (replace this with your appId and secret).

if(isset($_GET['id_cliente'])){


  $cliente = $db->select ( 'cliente', 'id_cliente = ' . $_GET['id_cliente'] );

  $cliente = $cliente [0];

  foreach($cliente as $idx=>$cli){
    $$idx = $cli;
  }
  $facebook_id = $facebookid;

  $imagem_cliente = $foto_cliente;
}else{

// Get User ID
$user = $facebook->getUser();

// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
  $access_token =  $facebook->getAccessToken();
  $facebook->setAccessToken($access_token);
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me?fields=id,name,email,gender,first_name,last_name,cover' );
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}


if($user_profile){

  $facebook_id = $user_profile['id'];
  $nome_cliente = $user_profile['name'];
  $email_cliente = $user_profile['email'];
  $sexo_cliente = ($user_profile['gender'] == 'male') ? 'M' : 'F';
  $imagem_cliente = "https://graph.facebook.com/{$user}/picture?type=large";

  $bind = array('email_cliente' => $email_cliente);
  $cliente = $db->select('cliente', 'email_cliente = :email_cliente' , $bind);
  if($cliente){
      $meusDados = $cliente[0];
      $arrayCliente = array(
          'facebookid' => $facebook_id,
          'foto_cliente'=>$imagem_cliente,
          'nome_cliente' => $nome_cliente
      );
      $db->update('cliente',$arrayCliente,'id_cliente = '.$meusDados['id_cliente']);

      $cliente = $db->selecT('cliente', 'email_cliente = :email_cliente' , $bind);
      $clienteEncontrado = $cliente[0];
      $_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $clienteEncontrado );

      header('location:index.php');
      exit;
  }else{
    $senha_cliente = uniqid('TAROT');
    $conteudoContato = file_get_contents('process/modeloContatoFacebook.html');
    $nameInput = $nome_cliente;
    $emailInput =  $email_cliente;

    $conteudoContato = str_replace('#NOMECLIENTE#', $nameInput , $conteudoContato);
    $conteudoContato = str_replace('#EMAILCLIENTE#', $emailInput , $conteudoContato);
    $conteudoContato = str_replace('#SENHACLIENTE#', $senha_cliente , $conteudoContato);
    $conteudoContato = str_replace('#IMAGEMCLIENTE#', $imagem_cliente , $conteudoContato);

    $headers .= "Return-Path: " . $emailsender . PHP_EOL; // Se "não for Postfix"
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $envio = mail($emailInput, 'Cadastro de cliente', $conteudoContato, $headers ,"-r {$emailsender}");



    $arrayCliente = array(
        'facebookid' => $facebook_id,
        'foto_cliente'=>$imagem_cliente,
        'nome_cliente' => $nome_cliente,
        'senha_cliente' => md5($senha_cliente),
        'email_cliente'=>$email_cliente,
        'sexo_cliente' => $sexo_cliente
    );

    $cliente = $db->insert('cliente', $arrayCliente);

    $cliente = $db->selecT('cliente', 'email_cliente = :email_cliente' , $bind);
    $clienteEncontrado = $cliente[0];
    $_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $clienteEncontrado );


    header('location:index.php');
    exit;


  }

}
// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
} else {

  $scopes = array('scope'=>'email,public_profile, user_friends');
  $loginUrl = $facebook->getLoginUrl($scopes);
}

// This call will always work since we are fetching public data.

}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Faça seu cadastro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">



  <style>

.register-box {
  width: 360px;
  margin: 1% auto;
}
    #holder {
      border: 10px dashed #292759;
      width: 190px;
      min-height: 160px;
      margin: 20px auto;
    }



    #holder.hover {
      border: 10px dashed #0c0;
    }



    #holder img {
      display: block;
      margin: 10px auto;
    }



    #holder p {
      margin: 10px;
      font-size: 14px;
    }

.box {
  position: relative;
  border-radius: 2px;
  background: #fafafa;
  border: 1px solid #e3e3e3;
  margin-bottom: 20px;
  width: 100%;
}
.box.box-primary {
  border-top-color: #e3e3e3;
}
.box.box-info {
  border-top-color: #e3e3e3;
}
.box.box-danger {
  border-top-color: #dd4b39;
}
.box.box-warning {
  border-top-color: #f39c12;
}
.box.box-success {
  border-top-color: #00a65a;
}
.box.box-default {
  border-top-color: #d2d6de;
}
.box.collapsed-box .box-body,
.box.collapsed-box .box-footer {
  display: none;
}
.box .nav-stacked > li {
  border-bottom: 1px solid #f4f4f4;
  margin: 0;
}
.box .nav-stacked > li:last-of-type {
  border-bottom: none;
}
.box.height-control .box-body {
  max-height: 300px;
  overflow: auto;
}
.box .border-right {
  border-right: 1px solid #f4f4f4;
}
.box .border-left {
  border-left: 1px solid #f4f4f4;
}
.box.box-solid {
  border-top: 1;
}
.box.box-solid > .box-header .btn.btn-default {
  background: transparent;
}
.box.box-solid > .box-header .btn:hover,
.box.box-solid > .box-header a:hover {
  background: rgba(0, 0, 0, 0.1);
}
.box.box-solid.box-default {
  border: 1px solid #d2d6de;
}
.box.box-solid.box-default > .box-header {
  color: #444444;
  background: #d2d6de;
  background-color: #d2d6de;
}
.box.box-solid.box-default > .box-header a,
.box.box-solid.box-default > .box-header .btn {
  color: #444444;
}
.box.box-solid.box-primary {
  border: 1px solid #3c8dbc;
}
.box.box-solid.box-primary > .box-header {
  color: #ffffff;
  background: #3c8dbc;
  background-color: #3c8dbc;
}
.box.box-solid.box-primary > .box-header a,
.box.box-solid.box-primary > .box-header .btn {
  color: #ffffff;
}
.box.box-solid.box-info {
  border: 1px solid #00c0ef;
}
.box.box-solid.box-info > .box-header {
  color: #ffffff;
  background: #00c0ef;
  background-color: #00c0ef;
}
.box.box-solid.box-info > .box-header a,
.box.box-solid.box-info > .box-header .btn {
  color: #ffffff;
}
.box.box-solid.box-danger {
  border: 1px solid #dd4b39;
}
.box.box-solid.box-danger > .box-header {
  color: #ffffff;
  background: #dd4b39;
  background-color: #dd4b39;
}
.box.box-solid.box-danger > .box-header a,
.box.box-solid.box-danger > .box-header .btn {
  color: #ffffff;
}
.box.box-solid.box-warning {
  border: 1px solid #f39c12;
}
.box.box-solid.box-warning > .box-header {
  color: #ffffff;
  background: #f39c12;
  background-color: #f39c12;
}
.box.box-solid.box-warning > .box-header a,
.box.box-solid.box-warning > .box-header .btn {
  color: #ffffff;
}
.box.box-solid.box-success {
  border: 1px solid #00a65a;
}
.box.box-solid.box-success > .box-header {
  color: #ffffff;
  background: #00a65a;
  background-color: #00a65a;
}
.box.box-solid.box-success > .box-header a,
.box.box-solid.box-success > .box-header .btn {
  color: #ffffff;
}
.box.box-solid > .box-header > .box-tools .btn {
  border: 0;
  box-shadow: none;
}
.box.box-solid[class*='bg'] > .box-header {
  color: #fff;
}
.box .box-group > .box {
  margin-bottom: 5px;
}
.box .knob-label {
  text-align: center;
  color: #333;
  font-weight: 100;
  font-size: 12px;
  margin-bottom: 0.3em;
}
.box > .overlay,
.overlay-wrapper > .overlay,
.box > .loading-img,
.overlay-wrapper > .loading-img {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
.box .overlay,
.overlay-wrapper .overlay {
  z-index: 50;
  background: rgba(255, 255, 255, 0.7);
  border-radius: 3px;
}
.box .overlay > .fa,
.overlay-wrapper .overlay > .fa {
  position: absolute;
  top: 50%;
  left: 50%;
  margin-left: -15px;
  margin-top: -15px;
  color: #000;
  font-size: 30px;
}
.box .overlay.dark,
.overlay-wrapper .overlay.dark {
  background: rgba(0, 0, 0, 0.5);
}
.box-header:before,
.box-body:before,
.box-footer:before,
.box-header:after,
.box-body:after,
.box-footer:after {
  content: " ";
  display: table;
}
.box-header:after,
.box-body:after,
.box-footer:after {
  clear: both;
}
.box-header {
  color: #444;
  display: block;
  padding: 10px;
  position: relative;
}
.box-header.with-border {
  border-bottom: 1px solid #e3e3e3;
}
.collapsed-box .box-header.with-border {
  border-bottom: none;
}
.box-header > .fa,
.box-header > .glyphicon,
.box-header > .ion,
.box-header .box-title {
  display: inline-block;
  font-size: 18px;
  margin: 0;
  line-height: 1;
}
.box-header > .fa,
.box-header > .glyphicon,
.box-header > .ion {
  margin-right: 5px;
}
.box-header > .box-tools {
  position: absolute;
  right: 10px;
  top: 5px;
}
.box-header > .box-tools [data-toggle="tooltip"] {
  position: relative;
}
.box-header > .box-tools.pull-right .dropdown-menu {
  right: 0;
  left: auto;
}
.btn-box-tool {
  padding: 5px;
  font-size: 12px;
  background: transparent;
  color: #97a0b3;
}
.open .btn-box-tool,
.btn-box-tool:hover {
  color: #606c84;
}
.btn-box-tool.btn:active {
  box-shadow: none;
}
.box-body {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  padding: 10px;
}
.no-header .box-body {
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}
.box-body > .table {
  margin-bottom: 0;
}
.box-body .fc {
  margin-top: 5px;
}
.box-body .full-width-chart {
  margin: -19px;
}
.box-body.no-padding .full-width-chart {
  margin: -9px;
}
.box-body .box-pane {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 3px;
}
.box-body .box-pane-right {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 0;
}
.box-footer {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  border-top: 1px solid #f4f4f4;
  padding: 10px;
  background-color: #ffffff;
}

.form-control {
  border-radius: 0;
  box-shadow: none;
  border-color: #d2d6de;
}
.form-control:focus {
  border-color: #3c8dbc;
  box-shadow: none;
}
.form-control::-moz-placeholder,
.form-control:-ms-input-placeholder,
.form-control::-webkit-input-placeholder {
  color: #bbb;
  opacity: 1;
}

.form-group.has-success label {
  color: #00a65a;
}
.form-group.has-success .form-control {
  border-color: #00a65a;
  box-shadow: none;
}
.form-group.has-warning label {
  color: #f39c12;
}
.form-group.has-warning .form-control {
  border-color: #f39c12;
  box-shadow: none;
}
.form-group.has-error label {
  color: #dd4b39;
}
.form-group.has-error .form-control {
  border-color: #dd4b39;
  box-shadow: none;
}

  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body class="hold-transition register-page">

            <div class="col-md-6 col-md-offset-3 box-cadastro">
        <div class="register-box">
          <div class="register-logo">
          <h2>Cadastre-se</h2>
          </div>

          <div class="register-box-body">
            <p class="login-box-msg">Complete seus dados</p>
            <form  method="post" role="form" name="form_register" id="form_register" onsubmit="return submitForm();">
                <input type="hidden" name='id_cliente' id='id_cliente' value="<?php echo $id_cliente;?>">
              <input type="hidden" name='facebookid' id='facebookid' value="<?php echo $facebook_id;?>">
              <input type="hidden" name='foto_cliente' id='foto_cliente' value="<?php echo $imagem_cliente;?>">
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id='nome_cliente' name='nome_cliente' placeholder="Nome completo" required  value="<?php echo $nome_cliente;?>" >


              </div>

              <div class="form-group has-feedback"  id='divEmail'>
                <input type="email" class="form-control" id="email_cliente" name="email_cliente" placeholder="Email"  value="<?php echo $email_cliente;?>" required>
                 <span id='spanEmail'></span>
              </div>

              <div class="form-group has-feedback">
                <input type="password" class="form-control" name='senha_cliente' id='senha_cliente' placeholder="Senha" <?if(!isset($_GET['id_cliente'])) { ?> required <?php } ?>>

              </div>

<!--               <div class="form-group has-feedback" > -->
<!--                 <input type="text" class="form-control" name='nascimento_cliente' id='nascimento_cliente' placeholder="Data de nascimento" required> -->

<!--               </div> -->

              <div class="row">
                <div class="form-group col-xs-5">
                 <div class="form-group has-feedback" >
                  <input type="radio"
                  class="form-control" name='sexo_cliente'
                  id='sexo_cliente' value ='M' required <?php echo ($sexo_cliente == 'M' ) ? 'checked': '' ;?>> <p>Masculino</p>
                </div>
              </div>
              <div class="form-group col-xs-5">
               <div class="form-group has-feedback" >
                <input type="radio" class="form-control" name='sexo_cliente' id='sexo_cliente' value ='F' required <?php echo ($sexo_cliente == 'F' ) ? 'checked': '' ;?>> <p>Feminino</p>
              </div>
            </div>
          </div>


      <div class="form-group">

            <label for="exampleInputFile">Foto de perfil - <small> Arraste sua imagem aqui</small></label>
            <article>
              <div class="form-group col-md-12 color-palette">
                <div id="holder"></div>
              </div>
              <p id="upload" class="hidden">
                <label>Drag &amp; drop not supported, but you can still upload via
                  this input field:<br> <input type="file">
                </label>
              </p>
              <p id="filereader">File API &amp; FileReader API not supported</p>
              <p id="formdata">XHR2's FormData is not supported</p>
              <p id="progress">XHR2's upload progress isn't supported</p>
            </article>




            <div class="users-list clearfix" >

                  <?php if(file_exists("admin454/dist/img/".$imagem_cliente)){ ?>

                  <img src="admin454/dist/img/<?php echo $imagem_cliente ; ?>" id='avatar_consultor' alt="" class="img-circle" width='215'>
                  <?php } else{
                    if($facebook_id){ ?>
                      <img src="<?php echo $imagem_cliente;?>" id='avatar_consultor' alt="" class="img-circle" width='215'>
                    <?php }else{
                   ?>
                  <img src="admin454/dist/img/nophoto.jpg" id='avatar_consultor' alt="" class="img-circle" width='215'>
                  <?php  } } ?>

              </div>
            </p>
          </div>
<?php if(!isset($_GET['id_cliente'])){?>
        <div >
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Atenção</h3>
            </div><!-- /.box-header -->
            <div class="box-body text-left">
              <p>As informações abaixo somente serão incluídas em seu cadastro após a primeira compra.<br>
              <ol>
                <li>CPF</li>
                <li>Endereço completo</li>
                <li>CEP</li>
                <li>Cidade</li>
                <li>Estado</li>
                <li>Pais</li>
              </ol>
              <p><span>Nenhum dado de cartão de crédito será armazenado em nossos servidores</span></p>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>


          <div class="row">
            <div class="col-md-12">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" class="checkbox" required=""> <p>Concordo com os  <a class="btn-mostra-cadastro" href="termosecondicoes/termos.pdf" target="_blank">Termos de serviço</a></p>
                </label>
              </div>
            </div><!-- /.col -->
            <? } ?>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <button type="submit" class="btn btn-default btn-block btn-flat" >Finalizar cadastro</button>
            </div><!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">

          <p>- ou -</p>
          <?php if ($user) { ?>
            <a href="?logoutFacebook=<?php echo $logoutUrl;?>"  class="btn btn-default fb-button">Sincronizar outro facebook</a>
          <?php }else{ ?>
          <a href="<?php echo $loginUrl;?>" class="btn btn-default fb-button">Sincronizar com Facebook</a>
          <? } ?>

    </div><!-- /.register-box -->

    <!-- jQuery 2.1.4 -->





    <script>
      $(function () {

        $("#email_cliente").blur(function(){

          $.post('process/procuraEmail.php' , {email: $(this).val() , atual: $("#id_cliente").val()}, function(data){

              if(data=='erro'){
                $("#divEmail").attr('class','form-group has-error');
                $("spanEmail").attr('class' , 'has-error');
                $("#spanEmail").html('E-mail  <strong>'+$("#email_cliente").val()+ '</strong> cadastrado');
                $("#email_cliente").val('');
                $("#email_cliente").focus()
              }else{
                $("#divEmail").attr('class','form-group has-feedback');
                $("#spanEmail").html('');
                $("spanEmail").attr('class' , 'has-feedback');
              }

          });

        })





        $("#email_cliente").blur();
      });
      function submitForm(){
        $.post('process/salvarCliente.php' , $("#form_register").serialize() , function(data){
           if(data == 'ok'){

            document.location='index.php';
           }

        })
        return false;

      }


      var holder = document.getElementById('holder'),
      tests = {
        filereader: typeof FileReader != 'undefined',
        dnd: 'draggable' in document.createElement('span'),
        formdata: !!window.FormData,
        progress: "upload" in new XMLHttpRequest
      },
      support = {
        filereader: document.getElementById('filereader'),
        formdata: document.getElementById('formdata'),
        progress: document.getElementById('progress')
      },
      acceptedTypes = {
        'image/png': true,
        'image/jpeg': true,
        'image/gif': true
      },
      progress = document.getElementById('uploadprogress'),
      fileupload = document.getElementById('upload');

      "filereader formdata progress".split(' ').forEach(function (api) {
        if (tests[api] === false) {
          support[api].className = 'fail';
        } else {
      // FFS. I could have done el.hidden = true, but IE doesn't support
      // hidden, so I tried to create a polyfill that would extend the
      // Element.prototype, but then IE10 doesn't even give me access
      // to the Element object. Brilliant.
      support[api].className = 'hidden';
    }
  });

      function previewfile(file) {

        if (tests.filereader === true && acceptedTypes[file.type] === true) {
          var reader = new FileReader();
          reader.onload = function (event) {
            var image = new Image();
            image.src = event.target.result;
        image.width = 250; // a fake resize
        holder.appendChild(image);
      };

      reader.readAsDataURL(file);
      return true;
    }  else {
      alert('Arquivo não suportado');
      return false;

    }
  }

  function readfiles(files) {
    debugger;
    var formData = tests.formdata ? new FormData() : null;
    for (var i = 0; i < files.length; i++) {
      if (tests.formdata) formData.append('file', files[i]);
      if(!previewfile(files[i])){
        return false;
      }
    }

      // now post a new XHR request
      if (tests.formdata) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
          if (xhr.readyState == XMLHttpRequest.DONE) {
            loadEnd((xhr.responseText))
          }
        }
        holder.innerHTML = '';
        xhr.open('POST', 'process/upload_galeria.php?foto_consultor_antiga='+$("#foto_cliente").val());
        xhr.onload = function() {
          console.log("INICIANDO O UPLOAD");
        };
        retorno =   xhr.send(formData);



      }
    }

    function loadEnd(text) {
      holder.innerHTML = '';

      $("#foto_cliente").val(text);
      $("#avatar_consultor").attr('src' , 'admin454/dist/img/'+text);

    }


    function uploadGaleria(){

   // location.reload();

 }

 if (tests.dnd) {
  holder.ondragover = function () { this.className = 'hover'; return false; };
  holder.ondragend = function () { this.className = ''; return false; };
  holder.ondrop = function (e) {
    this.className = '';
    e.preventDefault();
    readfiles(e.dataTransfer.files);
  }
} else {
  fileupload.className = 'hidden';
  fileupload.querySelector('input').onchange = function () {
    readfiles(this.files);
  };
}


</script>

  </div>
</body>
</html>
