<?php

/*
 * $email = 'mvbdesenvolvimento@gmail.com'; $token = '807FA521175B43C5B8DD7639D329F1CB'; $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/?email=' . $email . '&token=' . $token; $xml = '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?> <checkout> <currency>BRL</currency> <redirectURL>http://localhost/prototipos/tarot/paymentCompleted.php</redirectURL> <items> <item> <id>0001</id> <description>Notebook Prata</description> <amount>1.00</amount> <quantity>1</quantity> <weight>1000</weight> </item> <item> <id>0002</id> <description>Notebook Rosa</description> <amount>2.00</amount> <quantity>2</quantity> <weight>750</weight> </item> </items> <reference>'.uniqid('TAROT').'</reference> <sender> <name>José Comprador</name> <email>sounoob@comprador.com.br</email> <phone> <areaCode>11</areaCode> <number>55663377</number> </phone> <documents> <document> <type>CPF</type> <value>34891629860</value> <bornDate>19/07/1986</bornDate> </document> </documents> </sender> <shipping> <type>3</type> <address> <street>Rua sem nome</street> <number>1384</number> <complement>5o andar</complement> <district>Jardim Paulistano</district> <postalCode>01452002</postalCode> <city>Sao Paulo</city> <state>SP</state> <country>BRA</country> </address> </shipping> </checkout>'; $curl = curl_init($url); curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/xml; charset=ISO-8859-1')); curl_setopt($curl, CURLOPT_POSTFIELDS, $xml); $xml= curl_exec($curl); if($xml == 'Unauthorized'){ //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção header('Location: paginaDeErro.php'); exit;//Mantenha essa linha } curl_close($curl); $xml= simplexml_load_string($xml); var_dump($xml); //header('Location: https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml -> code);
 */
session_start();
include_once('conf/config.php');
$cod_agendamento = base64_decode($_GET['agendamento']);

$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode($usuario);

?>


<div class="container-fluid text-center">
	<div class="row">
    <div class="col-md-8 col-md-offset-2 add-Credito">


      <table class="table table-striped">
        <thead>
          <tr>
          <th colspan="2" class='text-center'> <h3>Área de aquisição de créditos</h3></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class='text-left'>Nome completo:</td>
            <td class='text-left'> <?php echo $jsonUsuario->nome_cliente;?></td>
          </tr>
          <tr>
            <td class='text-left'>E-mail:</td>
            <td class='text-left'><?php echo $jsonUsuario->email_cliente;?></td>
          </tr>
          <tr>
            <td class='text-left'>Saldo Atual:</td>
            <td class='text-left'>R$ <?php echo number_format($jsonUsuario->saldo_cliente,2,',','');?></td>
          </tr>
          <tr>
            <td class='text-left'>Inserir crédito:</td>
            <td class='text-left'>


              <select name='addCredito' id='addCredito'>
                <?php for($i=5;$i<=100;$i+=5){
                 echo '<option value='.base64_encode($i.'_'.$cod_agendamento).'>R$ '.number_format($i,2,',','').'</option>';
               }?>

             </select>
           </td>
         </tr>
         <tr>
          <td colspan="2"> <img src='admin454/data/img/pagseguro.png'></td>

        </tr>
        <tr>
          <td colspan="2"><button type="button" class="btn btn-default btn-block btn-flat" id='btnEfetivar' onclick="efetivarPagamento(document.getElementById('addCredito').value)" >Efetuar pagamento</button></td>

        </tr>
      </tbody></table>
    </div>
  </div>

</div>
<script type="text/javascript">
  function efetivarPagamento(codTpag){
    $('.modal-footer').hide();
    $(".modal-body").load('loading.php',function(){
      $('#modalSystem').modal('show');






      $.post('process/preparaPagseguro.php' , {cod_pagamento : codTpag} , function(data){

        $('#modalSystem').modal('hide');
        if(data == 'Unauthorized'){
          alert('Pagamento não autorizado');
        }else{
          mensagem = data.split('|');
          if(mensagem[0] == 'erro'){
            alert('Erro retornado do gateway de pagamento : ' + mensagem[1]);
            return false;
          }
          checkoutCode = data;
          PagSeguroLightbox({
            code: checkoutCode
          }, {
            success : function(transactionCode) {
              $(".modal-body").load('loading.php',function(){
                $('#modalSystem').modal('show');
              })
              $.post('paymentCompleted.php', {transaction_id:transactionCode},function(data){
                $('.modal-footer').show();

                if(data == 'erro'){

                  $('#modalSystem').modal('hide');
                }else{

                  $(".modal-body").load('invoice.php?transaction_id='+transactionCode,function(){
                    $('#modalSystem').modal('show');
                  })
                }

              });
            },
            abort : function() {

            }
          });

        }

      })

    })

  }
</script>
