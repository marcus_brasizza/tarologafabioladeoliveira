<?php

/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */



require 'facebook/facebook-php-sdk/src/facebook.php';
include_once ('conf/config.php');
$facebook = new Facebook(array(
  'appId'  => '465523473647316',
  'secret' => '09cf0eefca221077a674e1b21514b4a1',
  ));

if(isset($_GET['logoutFacebook'])){
  session_destroy();

  $facebook->destroySession();
  header('location:register.php');
  exit;
}


// Create our Application instance (replace this with your appId and secret).



// Get User ID
$user = $facebook->getUser();

// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
  $access_token =  $facebook->getAccessToken();
  $facebook->setAccessToken($access_token);
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me?fields=id,name,email,gender,first_name,last_name,cover' );
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}


if($user_profile){

  $facebook_id = $user_profile['id'];
  $nome_cliente = $user_profile['name'];
  $email_cliente = $user_profile['email'];
  $sexo_cliente = ($user_profile['gender'] == 'male') ? 'M' : 'F';
  $imagem_cliente = "https://graph.facebook.com/{$user}/picture?type=large";

  $bind = array('email_cliente' => $email_cliente);
  $cliente = $db->selecT('cliente', 'email_cliente = :email_cliente' , $bind);
  if($cliente){
      $meusDados = $cliente[0];
      $arrayCliente = array(
          'facebookid' => $facebook_id,
          'foto_cliente'=>$imagem_cliente,
          'nome_cliente' => $nome_cliente          
      );
      $db->update('cliente',$arrayCliente,'id_cliente = '.$meusDados['id_cliente']);
      
      $cliente = $db->selecT('cliente', 'email_cliente = :email_cliente' , $bind);
      $clienteEncontrado = $cliente[0];
      $_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $clienteEncontrado );
      
      header('location:index.php');
      exit;
  }else{

      $senha_cliente = uniqid('TAROT');
      $conteudoContato = file_get_contents('process/modeloContatoFacebook.html');
      $nameInput = $nome_cliente;
      $emailInput =  $email_cliente;
      
      $conteudoContato = str_replace('#NOMECLIENTE#', $nameInput , $conteudoContato);
      $conteudoContato = str_replace('#EMAILCLIENTE#', $emailInput , $conteudoContato);
      $conteudoContato = str_replace('#SENHACLIENTE#', $senha_cliente , $conteudoContato);
      $conteudoContato = str_replace('#IMAGEMCLIENTE#', $imagem_cliente , $conteudoContato);
      
      
      
     
      $headers .= "Return-Path: " . $emailsender . PHP_EOL; // Se "não for Postfix"
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $envio = mail($emailInput, 'Cadastro de cliente', $conteudoContato, $headers ,"-r {$emailsender}");
      
      
      
      $arrayCliente = array(
          'facebookid' => $facebook_id,
          'foto_cliente'=>$imagem_cliente,
          'nome_cliente' => $nome_cliente,
          'senha_cliente' => md5($senha_cliente),
          'email_cliente'=>$email_cliente,
          'sexo_cliente' => $sexo_cliente
      );
      
      $cliente = $db->insert('cliente', $arrayCliente);
      
      $cliente = $db->selecT('cliente', 'email_cliente = :email_cliente' , $bind);
      $clienteEncontrado = $cliente[0];
      $_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $clienteEncontrado );
       
      
      header('location:index.php');
      exit;
      
  }
  
}
// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
} else {

  $scopes = array('scope'=>'email,public_profile, user_friends');
  $loginUrl = $facebook->getLoginUrl($scopes);
  header('location:'.$loginUrl);
  exit;
}

// This call will always work since we are fetching public data.


?>