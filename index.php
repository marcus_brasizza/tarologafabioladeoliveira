<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
@session_start();
include_once ('conf/config.php');

$glob = glob('admin454/data/baralho/*.png');

$idx  = array_rand($glob,1);
$imagem_randomica = ($glob[$idx]);
// exit;
// $carta_banco = $db->select('cartas_baralho' , '1=1 order by rand() limit 1');
// $carta_banco = $carta_banco[0];
// $imagem_randomica = $carta_banco['imagem_carta'];



$configuracoes = $db->select('configuracoes' , '1=1 ');
$configuracoes = $configuracoes[0];
$telefone_contato = $configuracoes['telefone_contato'];


$carta_semana = $db->select('cartas_baralho' , 'carta_semana = 1');
if($carta_semana){
$carta_semana = $carta_semana[0];
//Gerar Opacidade da carta da semana
$texto_semana = $carta_semana['texto_carta'];

@unlink('img/carta-da-semana.png');
$imageName= 'admin454/'.$carta_semana['imagem_carta'];


copy ( $imageName , 'img/carta-da-semana.png');
@chmod('img/carta-da-semana.png',0777);


}
$bindHome = array('areaHome'=>'texto_home');
$texto  = $db->select('textos_site', 'area=:areaHome',$bindHome);
$texto = $texto[0];


$bindSobre = array('areaHome'=>'texto_sobre');
$texto_sobre  = $db->select('textos_site', 'area=:areaHome',$bindSobre);
$texto_sobre = $texto_sobre[0];

if(($_GET['area'] == 'blog')){
   $site_html = file_get_contents('blog.html');
}else{
$site_html = file_get_contents('site_index.html');
}
$site_html = str_replace('#[#IMAGEMRANDOMICA#]#',$imagem_randomica,$site_html);
$site_html = str_replace('#[#TEXTOCARTASEMANA#]#',html_entity_decode($texto_semana),$site_html);
//AREA HOME
$site_html = str_replace('#[#TITULOHOME#]#',$texto['titulo'],$site_html);
$site_html = str_replace('#[#TEXTOHOME#]#',html_entity_decode($texto['texto']),$site_html);
//AREA SOBRE

$site_html = str_replace('#[#TITULOSOBRE#]#',$texto_sobre['titulo'],$site_html);
$site_html = str_replace('#[#TEXTOSOBRE#]#',html_entity_decode($texto_sobre['texto']),$site_html);


$site_html = str_replace('#[#TELEFONECONTATO#]#',$telefone_contato,$site_html);

if(isset($_GET['DEBUG'])){

$site_html = str_replace("#[#DEBUG#]#" , "<li><a  onClick=\"window.open ('tvConsultor.php','win1','width=700,height=500,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no')\">DEBUG</a></li>",$site_html);
}else{
  $site_html = str_replace("#[#DEBUG#]#" , "",$site_html);
}

$eventos  = $db->select('eventos', 'status_evento=1 order by data_evento DESC limit 10');

if($eventos){
    
    foreach($eventos as $evento){
        
        	$strEventos.='
        	<div class="areaEventos">
            <div class="dataEvento">
            <div class="mes">'.strftime ( '%b', strtotime($evento['data_evento'])) .'</div>
            <div class="dia">'.date('d' , strtotime($evento['data_evento'])).'</div>
            </div>
            <div class="localEvento">
            <h2>Local do evento</h2>
            <p>'.$evento['titulo_evento'].'</p>
            </div>
            <div class="maisInformacoes">
            <a class="btn-acao btn btn-default" href="process/infoEvento.php?evento='.$evento['id_evento'].'" role="button" onclick="return false">Mais
            informações</a>
            </div>
            </div>';
        	
        	
      
    }
    
    $site_html = str_replace('#[#EVENTOS#]#',$strEventos,$site_html);
    
}


$blog = $db->select('blog','1=1 order by data_blog desc');
  if(count($blog) == 1){
   
     $carroussel =  '<div class="carousel-img blog-image-1">
							<div>
								<h2>'.$blog[0]['titulo_blog'].'</h2>
								<p>'.substr(html_entity_decode($blog[0]['texto_blog']),0,100).'...</p>								
								<a class="btn btn-default"  href="javascript:void(0)"   role="button">Saiba mais</a>
							</div>
						</div>';
					
  }else{
  
  
  $carroussel= '<div class="owl-carousel">';
 foreach($blog as $bbb) { 
                $imagens = rand(1,3);
                $carroussel.='	<div class="carousel-img blog-image-'.$imagens.'">
							<div>
								<h2>'. $bbb['titulo_blog'].'</h2>
								<p>'.substr(html_entity_decode($bbb['texto_blog']),0,100).'</p>
							     <a class="btn btn-default" href="?area=blog" role="button">Saiba mais</a>
							</div>
						</div>';
						
						
					  }
					  $carroussel.='</div>';
  
  }



  $depoimentos = $db->select('depoimentos_clientes' , '1=1 order by data_depoimento DESC' );


  if(count($depoimentos) == 1) {

   
          
        $str_depoimento = '  <div class="carousel-img">
            <div class="col-md-6 col-md-offset-3">
              <p>
                '.html_entity_decode(nl2br($depoimentos[0]['texto_depoimento'])).'
              </p>
            </div>
          </div>';


        

  }else{

     $str_depoimento = '<div class="owl-carousel">';
          foreach($depoimentos as $depoimento) { 
          $str_depoimento .='<div class="carousel-img">
            <div class="col-md-6 col-md-offset-3">
              <p>
                '.html_entity_decode(nl2br($depoimento['texto_depoimento'])).'
              </p>
            </div>';
     
        $str_depoimento .= '</div>';
  }
}





$site_html = str_replace('#[#CARROUSSEL#]#',$carroussel,$site_html);

$site_html = str_replace('#[#DEPOIMENTOS#]#',$str_depoimento,$site_html);


if(isset($_SESSION ['CM_GRANO_USER'] ['client'])){
  
  
  
  $cliente  = json_decode($_SESSION ['CM_GRANO_USER'] ['client']);  
  $clienteAtualizado = $db->select('cliente','id_cliente = '.$cliente->id_cliente);
  $clienteAtualizado = $clienteAtualizado[0];
  $_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $clienteAtualizado );  
  
$areaCliente = "  <a href=\"dadosCliente.php\"  class='btn-acao' onclick='return false'>".$cliente->nome_cliente."</a> |  Saldo atual: R$ ".number_format($cliente->saldo_cliente,2,',','.') ." | <a href=\"index.php?logout=true\" >Sair</a> ";
}else{
  $areaCliente = "  <a href=\"header_register.php\"  class='btn-acao' onclick='return false'>Login</a> | <a href=\"register.php\"  class='btn-acao' onclick='return false'>Cadastre-se</a>";
}
$site_html = str_replace('#[#AREACLIENTE#]#',html_entity_decode($areaCliente),$site_html);

if (isset ( $_GET ['logout'] )) {
    require 'facebook/facebook-php-sdk/src/facebook.php';
    include_once ('conf/config.php');
    $facebook = new Facebook(array(
        'appId'  => '465523473647316',
        'secret' => '09cf0eefca221077a674e1b21514b4a1',
    ));
    session_destroy ();
    $facebook->destroySession ();


    header ( 'location:index.php' );
    exit ();
}


echo $site_html;

 if(isset($_SESSION ['CM_GRANO_USER'] ['client'])){ ?>
		<script>
		localStorage.removeItem('cliente');		
		localStorage.setItem('cliente', '<?php echo  $_SESSION ['CM_GRANO_USER'] ['client'];?>');
		</script>

<? } ?>
