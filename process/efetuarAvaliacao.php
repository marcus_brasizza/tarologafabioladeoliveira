<?php
session_start();
include_once('../conf/config.php');
$cod_agendamento = base64_decode($_POST['agendamento_cliente']);
$motivo_consulta  = filter_input(INPUT_POST, 'motivo_consulta', FILTER_SANITIZE_SPECIAL_CHARS);
$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode($usuario);
$agenda = $db->select('agendamento_cliente' , 'id_agendamento_cliente = '.$cod_agendamento);
$agenda = $agenda[0];

if(!$agenda){
  echo 'erro';
  exit;
}


unset($agenda['id_agendamento_cliente']);
$agenda['nota_agendamento'] = $_POST['nota_agendamento'];

$agenda['mensagem_agendamento_cliente'] = $_POST['mensagem_agendamento_cliente'];
$db->update('agendamento_cliente' , $agenda, 'id_agendamento_cliente = '. $cod_agendamento);

  echo 'ok';
