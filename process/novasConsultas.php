<?php
session_start ();
include_once ('../conf/config.php');

$agendamento = $db->select ( 'agendamento', ' id_consultor = ' . $_POST ['id_consultor'] . ' and inicio_agendamento >= NOW() ' );

if ($agendamento) {
	foreach ( $agendamento as $agenda ) {
		
		$dia = date ( 'd/m/Y', strtotime ( $agenda ['inicio_agendamento'] ) );
		
		$dias_agendamento [$dia] [] = $agenda;
	}
}
if($dias_agendamento){
	
	$logado = (isset($_SESSION ['CM_GRANO_USER'] ['client']) ? base64_encode($_SESSION ['CM_GRANO_USER'] ['client']) : 'register'); ?>
	<div class="row">
	<div class="col-md-12 ultimasConsultas">
	<h4>Datas disponíveis</h4>
	<table class="table table-hover">
	<thead>
	<tr>									
	<th>Data da consulta</th>                  
	<th>Valor</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach($dias_agendamento as $dia=>$valor){
		
		echo '<tr class="alert alert-warning"><td colspan="2"><strong>'.$dia.'</strong></td></tr>';

		foreach($valor as $data){
			$agendaIni = date('H:i:s',strtotime($data['inicio_agendamento']));
			$agendaFim = date('H:i:s',strtotime($data['fim_agendamento']));
			$titulo =  $agendaIni . ' até '. $agendaFim ;
			$valor = number_format($data['valor_agendamento'],2,',','.');
			if($data['livre_agendamento'] == null){
				
				echo '<tr  onclick=\'confirmarAgenda("'.$data['id_agendamento'].'" ,"'.$logado.'")\' >				
				<td align="left">'.date('d/m/Y',strtotime($data['inicio_agendamento'])). ' '. $titulo .'</td>
				<td>'.$valor.'</td>
				</tr>';
				//echo '<li class="pull-left checkDisponivel  color-palette" data-agendamento = "'.$data['id_agendamento'].'" data-user = "'.$logado.'"><a href="#">'. $titulo.' <span class="pull-right badge bg-blue">R$ '.$valor.'</span></a></li>';
			}else{
				echo '<tr class="alert alert-danger" data-agendamento = "'.$data['id_agendamento'].'" data-user = "'.$logado.'">
				
				<td align="left">'.date('d/m/Y H:i:s',strtotime($data['inicio_agendamento'])). ' '. $titulo .'</td>
				<td>'.$valor.'</td>
				</tr>';
				//	echo '<li class="bg-red-active color-palette  disabled" data-agendamento = "'.$data['id_agendamento'].'" data-user = "'.$logado.'"><a href="#" class="text-yellow">'. $titulo.' <span class="pull-right badge bg-blue">R$ ---</span></a></li>';
			}
		}
		
	}
}

?>
</tbody>
</table>
</div>
</div>
