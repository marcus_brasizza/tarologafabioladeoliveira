<?php
session_start();
include_once('../conf/config.php');
$cod_agendamento = base64_decode($_POST['agendamento']);
$motivo_consulta  = filter_input(INPUT_POST, 'motivo_consulta', FILTER_SANITIZE_SPECIAL_CHARS);
$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode($usuario);
$agenda = $db->select('agendamento' , 'id_agendamento = '.$cod_agendamento. ' and ( livre_agendamento is null or livre_agendamento = 0 )');
$agenda = $agenda[0];

if(!$agenda){
  echo 'erro';
  exit;
}


$agenda['livre_agendamento'] = '1' ;
$cliente = $db->select('cliente' , 'id_cliente  = '. $jsonUsuario->id_cliente);

$cliente = $cliente[0];

$saldo_atual = $cliente['saldo_cliente'];
$valor_consulta = $agenda['valor_agendamento'];
$cliente['saldo_cliente'] = ($saldo_atual - $valor_consulta);
$array_agenda = array(
  'id_agendamento' => $agenda['id_agendamento'],
  'id_cliente' => $cliente['id_cliente'],
  'info_agendamento_cliente' => $motivo_consulta
);

$db->insert('agendamento_cliente' , $array_agenda);
$db->update('agendamento' , $agenda, 'id_agendamento = '. $agenda['id_agendamento']);

$db->update('cliente' , $cliente, 'id_cliente = '. $cliente['id_cliente']);



$cliente = $db->select('cliente','id_cliente = '.$cliente['id_cliente']);
$cliente = $cliente[0];


foreach ($cliente as $idx => $value) {
	$$idx=$value;
}

foreach ($agenda as $idx => $value) {
	$$idx=$value;
}

$consultor = $db->select('consultor' , 'id_consultor = '. $agenda['id_consultor']);
$consultor = $consultor[0];

foreach ($consultor as $idx => $value) {
	$$idx=$value;
}


$conteudoContato = file_get_contents('modeloEfetuarAgendamento.html');
   
    $emailInput =  $cliente['email_cliente'];
    $nameInput = $cliente['nome_cliente'];

    $inicio_agendamento = date('d/m/Y H:i:s' , strtotime($agenda['inicio_agendamento']));
    $fim_agendamento = date('d/m/Y H:i:s' , strtotime($agenda['fim_agendamento']));
    $consulta_valor = number_format($valor_consulta,2,',','');


    $conteudoContato = str_replace('#NOMECLIENTE#', $nameInput , $conteudoContato);
    $conteudoContato = str_replace('#EMAILCLIENTE#', $emailInput , $conteudoContato);
    $conteudoContato = str_replace('#INICIO#', $inicio_agendamento , $conteudoContato);
    $conteudoContato = str_replace('#FIM#', $fim_agendamento , $conteudoContato);
    $conteudoContato = str_replace('#CONSULTOR#', $nome_consultor. ' '. $sobrenome_consultor , $conteudoContato);
    $conteudoContato = str_replace('#VALOR#', $consulta_valor , $conteudoContato);
    $conteudoContato = str_replace('#SKYPE#', $skype_consultor , $conteudoContato);
    $conteudoContato = str_replace('#MOTIVO#', nl2br(html_entity_decode($motivo_consulta)) , $conteudoContato);
    $linkConsulta =  $linkSite ;//$linkapprtc . 'Consulta_'.($id_agendamento) '?hd=true&stereo=true';
     $conteudoContato = str_replace('#LINKCONSULTA#', $linkConsulta , $conteudoContato);   

    
    

    $headers .= "Return-Path: " . $emailsender . PHP_EOL; // Se "não for Postfix"
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $envio = mail($emailInput, 'Agendamento de consulta', $conteudoContato, $headers ,"-r {$emailsender}");

    @file_put_contents('/tmp/salvaAgendamento.html' , $conteudoContato);



$_SESSION ['CM_GRANO_USER'] ['client'] = json_encode ( $cliente );
//  mysqli_query($conn , $sqlDados) or exit('erro');
  echo 'ok';
