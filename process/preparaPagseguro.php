<?php

if($_POST){
	session_start();
	include_once('../conf/config.php');
	$configs = $db->select('configuracoes');
	$configs = $configs[0];
	
 $email = $configs['email_pagseguro']; 
 $token = $configs['token_pagseguro'];
 $urlPagseguro = $configs['ambiente']==1 ? $configs['url_pagseguro_homolog'] : $configs['url_pagseguro_prod']; 
 $usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
 $jsonUsuario = json_decode($usuario);
 $cod_pagamento = $_POST['cod_pagamento'];
 
 $pagamento = base64_decode($cod_pagamento);
 
 list($valor,$agendamento) = explode('_',$pagamento);

 $agenda = $db->select('agendamento' ,'id_agendamento = '.$agendamento);
 $agenda = $agenda[0];
 
 $consultor = $db->select('consultor'  , 'id_consultor = '.$agenda['id_consultor']);
 
 $consultor = $consultor[0];
 
 $nomeConsultor = ucfirst($consultor['nome_consultor']). ' '. ucfirst($consultor['sobrenome_consultor']);
 
 $valor_consulta = number_format($valor,2);
 

 $data_agendamento = date('d/m/Y H:i:s',strtotime($agenda['inicio_agendamento'])). ' ate '. date('d/m/Y H:i:s',strtotime($agenda['fim_agendamento']));

 
 $codigoConsulta = uniqid("TAROT",$cod_pagamento);
 $strAgendamento = 'Credito no valor de '.$valor_consulta;



 
 $url = $urlPagseguro.'/v2/checkout/?email=' . $email . '&token=' . $token; 
 $xml = '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?> 
	 <checkout> 
	 <currency>BRL</currency> 	 
	 <items> 
	 <item> 
	 <id>0001</id> 
	 <description>'.$strAgendamento.'</description> 
	 <amount>'.$valor_consulta.'</amount> 
	 <quantity>1</quantity> 
	 <weight>1</weight> 
	 </item>	 
	 </items> 
	 <reference>'.$codigoConsulta.'</reference> 
	 <sender> 
	 <name>'.$jsonUsuario->nome_cliente.'</name> 
	 <email>'.$jsonUsuario->email_cliente.'</email> 	 
	 </sender> 
	 <shipping> 
	 <type>3</type> 	 
	 </shipping> 
	 </checkout>'; 
 $curl = curl_init($url); 
 curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
 curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
 curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/xml;  charset=ISO-8859-1')); 
 curl_setopt($curl, CURLOPT_POSTFIELDS, $xml); 
 $xml= curl_exec($curl); 
 

 if($xml == 'Unauthorized') //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção header('Location: paginaDeErro.php'); 
 {
 	 echo 'erro|Unauthorized';
 	exit;
 }
 curl_close($curl); 
 $xml= simplexml_load_string($xml); 

$erroMessage = ((string)$xml->error->message);

if(!strlen(trim($erroMessage))){

 $arrayPagamento = 
 array( 		
 		'numero_pagamento' => $codigoConsulta,
 		'valor_pagamento'=>$valor_consulta,
 		'item_pagamento'=>$strAgendamento,
 		'id_cliente' => $jsonUsuario->id_cliente
 
 );
 $db->insert('pagamento', $arrayPagamento);
 //echo 'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml -> code;
 echo $xml->code;
 }else{
	 echo 'erro|'.$erroMessage;
 }
 exit;
}
 ?> 