<?php
  session_start();
  include_once('../conf/config.php');
  ?>
  
					<?php 
  if(isset($_GET['cod_editoria'])){
      $cod_editoria = $_GET['cod_editoria'];
      
      $editorial = $db->select('editorial',' id_editorial = '.$cod_editoria);
      $editorial = $editorial[0];
      $nome_editorial = $editorial['nome_editorial'];
      
      $bind = array('editoriais' => $nome_editorial);
      $blog = $db->select('blog',' editoriais like "%'.$nome_editorial.'%" order by data_blog desc');
     
  }else{
$blog = $db->select('blog','1=1 order by data_blog desc');
  }
  
  
  if(count($blog) == 1){
   ?>
     <div class="carousel-img blog-image-1">
							<div>
								<h2><?php echo $blog[0]['titulo_blog'] ?></h2>
								<p><?php echo substr(html_entity_decode($blog[0]['texto_blog']),0,100).'...';?></p>
								
								<a class="btn btn-default"  href="javascript:void(0)" onclick="lerNoticia('<?php echo $blog[0]['id_blog'];?>')"  role="button">Leia na
									integra</a>
							</div>
						</div>
						<?php 
  }else{
  ?>
  
  <div class="owl-carousel">
  <?php foreach($blog as $bbb) { 
                $imagens = rand(1,3);
      ?>
						<div class="carousel-img blog-image-<?php echo $imagens;?>">
							<div>
								<h2><?php echo $bbb['titulo_blog'] ?></h2>
								<p><?php echo substr(html_entity_decode($bbb['texto_blog']),0,100).'...';?></p>
								<a class="btn btn-default" href="javascript:void(0)" onclick="lerNoticia('<?php echo $bbb['id_blog'];?>')" role="button">Leia na
									integra</a>
							</div>
						</div>
						
						
						<?php  } ?>
					</div>
  
  
  <?php 
  }

function nomeConsultor($banco,$cod_consultor){

	$consultor =$banco->select('consultor', 'id_consultor='.$cod_consultor);
	$consultor = $consultor[0];
	$nome = $consultor['nome_consultor'].' '.$consultor['sobrenome_consultor'];
	return $nome;

}

foreach($blog as $itens){	
	if($itens['imagem_blog'] == null ){
		$itens['imagem_blog'] = 'nophoto.jpg';
	}else{

	if(!file_exists('../admin454/dist/img/'.$itens['imagem_blog'])  ){
		

		$itens['imagem_blog'] = 'nophoto.jpg';
	}
}
	?>
		
		
	
		<div class="rowStripped">
							<div class="col-md-12">
								<div class="row materia-blog">
									<a href="#">
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
											<img class="img-rounded img-responsive center-block"
												src="<?php echo 'admin454/dist/img/'.$itens['imagem_blog'];?>" alt="" width="250">
										</div>
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
											<h2 ><a href="javascript:void(0)" onclick="lerNoticia('<?php echo $itens['id_blog'];?>')"><?php echo $itens['titulo_blog'];?></a></h2>
											<!--<p><?php echo substr($itens['texto_blog'],0,100).'...';?></p> //--
>										</div>
									</a>
								</div>
							</div>
						</div>
	<?php
	$dataBlog[date('d/m/Y',strtotime($itens['data_blog']))][] = array(
			'titulo' => $itens['titulo_blog'],
			'autor' => nomeConsultor($db,$itens['id_consultor']),
			'editoriais' => $itens['editoriais'],
			'imagem' => $itens['imagem_blog'],
			'hora' => date('H:i:s',strtotime($itens['data_blog'])),
			'data' => date('d/m/Y',strtotime($itens['data_blog'])),
			'mensagem' => $itens['texto_blog']
	);
	
}

?>
<script>
$(document).ready(function() {
	$(".owl-carousel").owlCarousel({
		items : 1,
		loop : true,
		margin : 10,
		autoplay : true,
		autoplayTimeout : 3000,
		autoplayHoverPause : true
	});
});

</script>
