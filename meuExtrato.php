<?php
session_start ();
include_once ('conf/config.php');

$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode ( $usuario );
$cliente = $db->select ( 'cliente', 'id_cliente = ' . $jsonUsuario->id_cliente );

$cliente = $cliente [0];

$bind = array (
		':id_cliente' => $cliente ['id_cliente'] 
);
$pagamentos = $db->select ( 'pagamento', 'id_cliente = :id_cliente and status_pagamento is not null', $bind );

if ($pagamentos [0]) {
	
	
	
	
	foreach ( $pagamentos as $pagamento ) {
		
		$status_pagamento = $db->select('status_pagamento' , 'id_status = '.$pagamento['status_pagamento']);
		$status_pagamento = $status_pagamento[0];
		
		if($status_pagamento['nome_status']){
			$strStatus = $status_pagamento['nome_status'];
		}else{
			$strStatus = $pagamento['status_pagamento'];
		}
		
		$infos [date('dmyHis',strtotime ( $pagamento ['data_pagamento'] ))] [] = array (
				'id_info' => $pagamento ['id_pagamento'],
				'data' => date ( 'd/m/Y H:i:s', strtotime ( $pagamento ['data_pagamento'] ) ),
				'info' => $pagamento ['item_pagamento'],
				'status' => ($pagamento ['status_pagamento']) ?  $pagamento ['status_pagamento'] : 'x',
				'str_status' => ($pagamento ['status_pagamento']) ?  $strStatus : '',
				'forma_pagamento' => $pagamento ['forma_pagamento'],
				'transaction_id' => $pagamento ['numero_pagamento'] . ' ('.$pagamento['transacao_pagamento'].')',
				'tipo' => 'C' 
		);
	}
}


$agendamento_cliente = $db->select ( 'agendamento_cliente', 'id_cliente = :id_cliente', $bind );
if($agendamento_cliente[0]){
	
	foreach($agendamento_cliente as $ag_cli){
		
		$agenda = $db->select('agendamento', 'id_agendamento = '.$ag_cli['id_agendamento']);
		$agenda = $agenda[0];
		$consultor = $db->select('consultor' , 'id_consultor = '. $agenda['id_consultor']);
		$consultor = $consultor[0];
	
		$infos [date('dmyHis',strtotime ( $ag_cli ['data_agendamento_cliente'] ))] [] = array (
				'id_info' =>  $ag_cli ['id_agendamento_cliente'],
				'data' => date ( 'd/m/Y H:i:s', strtotime ( $ag_cli ['data_agendamento_cliente'] ) ),
				'info' => "Consulta com <strong>{$consultor['nome_consultor']}</strong>, ".date('d/m/Y',strtotime($agenda['inicio_agendamento']))." das ".date('H:i:s',strtotime($agenda['inicio_agendamento']))." às ".date('H:i:s',strtotime($agenda['fim_agendamento']))."",
				'status' => $ag_cli ['finalizado_agendamento'],
				'nota_agendamento' => $ag_cli ['nota_agendamento'],
				'tipo' => 'D' 
		);
	}
	
}


ksort($infos);
$infos = array_reverse($infos);



?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Confirmação de pagamento | Invoice</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="admin454/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="admin454/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="admin454/dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>




<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Extrato do cliente</h3>

			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
							
							<th>Data</th>
							<th>Descrição</th>
							<th>Status</th>
						
						</tr>
						<?php foreach ($infos as $info ){ 
								foreach($info as $extrato){
								$classTR = '';
								$titleTR = '';
								switch($extrato['status']){

									case 0:
										if($extrato['tipo'] == 'C'){
											$classTR = 'class="alert alert-danger"';
											$titleTR = 'title="Transação abandonada"';
											$status_str = '<span class="label label-warning">'.$extrato['str_status'].'</span>';
										
										}else{
											$status_str = '<span class="label alert-info">Aguardando a consulta</span>';
										}
										break;
									
									case 'x':
										$classTR = 'class="alert alert-danger"';
										$titleTR = 'title="Transação abandonada"';
										$status_str = '<span class="label label-success">'.$extrato['status'].'</span>';
										break;
									case 1:
											if($extrato['tipo'] == 'C'){
												$status_str = '<span class="label label-warning">'.$extrato['str_status'].'</span>';
												$titleTR = 'title="'.$extrato['transaction_id'].'"';
												
											}else{
												if($extrato['nota_agendamento'] >= 0 && $extrato['nota_agendamento'] <= 3 ){
													$img_estrela = '0.png';	
												}elseif($extrato['nota_agendamento'] > 3 && $extrato['nota_agendamento'] <= 6){
	
													$img_estrela = '1.png';
												}
												
												elseif($extrato['nota_agendamento'] > 6 && $extrato['nota_agendamento'] <= 8){
												
													$img_estrela = '2.png';
												}else{
													$img_estrela = '3.png';
												}
												
												$status_str  = '<span class="label label-success"> Finalizada: <img src="admin454/data/img/'.$img_estrela.'" width="16" title = "'.$extrato['nota_agendamento'].'/10"> </span>';
												
											}
											break;
											
											case 3:
												if($extrato['tipo'] == 'C'){
													$titleTR = 'title="'.$extrato['transaction_id'].'"';
													$status_str = '<span class="label label-success">'.$extrato['str_status'].'</span>';
											
												}
										break;
										
											default:
												if($extrato['tipo'] == 'C'){
													$titleTR = 'title="'.$extrato['transaction_id'].'"';
													$status_str = '<span class="label label-danger">'.$extrato['str_status'].'</span>';
														
												}
												break;
							
								}
								?>
						<tr <?php echo $classTR; ?> <?php echo $titleTR;?>>
							
							<td><?php echo $extrato['data'];?></td>
							<td><?php echo $extrato['info'];?></td>
							<td><?php echo $status_str; ?></td>
							
						</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>


<!-- jQuery 2.1.4 -->
<script src="admin454/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="admin454/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="admin454/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="admin454/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="admin454/dist/js/demo.js"></script>
</body>
</html>
