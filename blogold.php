<?php
session_start ();
include_once ('conf/config.php');

$blog = $db->select('blog','1=1 order by data_blog desc');


function nomeConsultor($banco,$cod_consultor){

	$consultor =$banco->select('consultor', 'id_consultor='.$cod_consultor);
	$consultor = $consultor[0];
	$nome = $consultor['nome_consultor'].' '.$consultor['sobrenome_consultor'];
	return $nome;

}

foreach($blog as $itens){
	if(!file_exists('admin454/dist/img/'.$itens['imagem_blog']) ||$itens['imagem_blog'] == '' ){
		
		$itens['imagem_blog'] = 'nophoto.jpg';
	}
	$dataBlog[date('d/m/Y',strtotime($itens['data_blog']))][] = array(
			'titulo' => $itens['titulo_blog'],
			'autor' => nomeConsultor($db,$itens['id_consultor']),
			'editoriais' => $itens['editoriais'],
			'imagem' => $itens['imagem_blog'],
			'hora' => date('H:i:s',strtotime($itens['data_blog'])),
			'data' => date('d/m/Y',strtotime($itens['data_blog'])),
			'mensagem' => $itens['texto_blog']
	);
	
}



?>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>BLOG</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="admin454/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="admin454/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="admin454/dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body class="hold-transition skin-blue sidebar-lg">



	<section class="content">

          <!-- row -->
          <div class="row">
            <div class="col-md-12">
              <!-- The time line -->
              <ul class="timeline">
              <?php foreach($dataBlog as $idx=>$gradePost){ 
              
              
              	?>
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-red">
                   <?php  echo $idx;?>
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <?php foreach($gradePost as $post){ ?>
                <li>
                  <img src="admin454/dist/img/<?php echo $post['imagem'];?>" class='img-rounded' width=100>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo $post['hora'];?> EM: <?php echo $post['editoriais'];?></span>
                    <h3 class="timeline-header">  <a href="#"><?php echo $post['titulo'];?></a></h3>
                    <div class="timeline-body">
                     <?php echo $post['mensagem'];?>
                    </div>
                   
                  </div>
                </li>
               <?php }  }?>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section>

	<!-- jQuery 2.1.4 -->
	<script src="admin454/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="admin454/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="admin454/plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="admin454/dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="admin454/dist/js/demo.js"></script>
</body>
</html>
