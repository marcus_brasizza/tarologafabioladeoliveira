<?php
session_start();
include_once ('conf/config.php');

$consultores = $db->select('consultor' , ' status_consultor = 1');


?>

<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistema Administrativo GRANO</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
	<!-- Bootstrap 3.3.5 -->

	<!-- Font Awesome -->
	<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="admin454/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

	<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- jvectormap -->
	<link rel="stylesheet"
	href="admin454/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="admin454/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="admin454/dist/css/skins/_all-skins.min.css">
	<!-- Theme style -->
	<style media="screen">
  
  @import url(http://fonts.googleapis.com/css?family=Rochester);
@import url(http://fonts.googleapis.com/css?family=Lato:400,300,700);
body {
  background: url('http://kieranoconnorphotography.com/wp-content/uploads/2012/06/board-black-series-texture-background-wood-wallpaper-wood-texture-wallpapers-476..jpg') no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
.singlecard {
  width: 200px;
  height: 335px;
  position: relative;
  margin: 75px auto 40px;
  -webkit-perspective: 800px;
  -moz-perspective: 800px;
  -o-perspective: 800px;
  perspective: 800px;
}
.singlecard .card {
  width: 100%;
  height: 100%;
  position: absolute;
  -webkit-transition: -webkit-transform 1s;
  -moz-transition: -moz-transform 1s;
  -o-transition: -o-transform 1s;
  transition: transform 1s;
  -webkit-transform-style: preserve-3d;
  -moz-transform-style: preserve-3d;
  -o-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-transform-origin: right center;
  -moz-transform-origin: right center;
  -o-transform-origin: right center;
  transform-origin: right center;
  -webkit-box-shadow: 0 0 12px 10px rgba(0, 0, 0, 0.5);
  box-shadow: 0 0 12px 10px rgba(0, 0, 0, 0.5);
}
.singlecard .card figure {
  cursor: pointer;
  display: block;
  height: 100%;
  width: 100%;
  line-height: 260px;
  color: #fff;
  text-align: center;
  font-weight: bold;
  font-size: 140px;
  position: absolute;
  margin: 0;
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility: hidden;
  -o-backface-visibility: hidden;
  backface-visibility: hidden;
}
.singlecard .card .front {
  height: 335px;
  background: #3D3D3D;
}
.singlecard .card .front img {
  width: 100%;
}
.singlecard .card .back {
  background: #3D3D3D;
  height: 392px;
  font-size: 14px;
  width: 100%;
  padding: 5px 20px;
  -webkit-transform: rotateY(180deg);
  -moz-transform: rotateY(180deg);
  -o-transform: rotateY(180deg);
  transform: rotateY(180deg);
}
.singlecard .card .back h3 {
  font: normal 24px/24px 'Rochester', cursive !important;
}
.singlecard .card .back .description {
  text-align: left;
  color: #B4B4B4;
  font: 12px/16px Arial, "Helvetica Neue", Helvetica, sans-serif;
}
.singlecard .card .back .description em {
  font-style: normal;
  color: #fff;
}
.singlecard .card .back .description .titlecr {
  font-weight: bold;
  color: #2D9BD8;
}
.singlecard .card.flipped {
  -webkit-transform: translateX(-100%) rotateY(-180deg);
  -moz-transform: translateX(-100%) rotateY(-180deg);
  -o-transform: translateX(-100%) rotateY(-180deg);
  transform: translateX(-100%) rotateY(-180deg);
}

	</style>
	<script src="admin454/plugins/jQuery/jQuery-2.1.4.min.js"></script>

	<script src="admin454/plugins/jQueryUI/jquery-ui.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="admin454/bootstrap/js/bootstrap.min.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="admin454/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="admin454/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script >

  $( function() {

    $('.card').click( function( event ) {
      $( event.currentTarget ).toggleClass('flipped');
    });
    
  });


	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini" >

	<div class="container-fluid text-center">
		<div class="row">

      		
      			<?php $glob = glob('admin454/data/baralho/*.jpg');
      				shuffle($glob);

      				foreach($glob as $imagem){ ?>
      			<div class="form-group col-xs-3">
      					<section class="singlecard">
					<div id="card1" class="card">
					<figure class="front"><img src="<?php echo $imagem ;?>" /></figure>
					<figure class="back">
					<h3> Sobre essa carta</h3>
					<div class="description">
					 <p><em class="titlecr">LOREM IPSUM</em> Ipsum <em>wisdom</em>,  <em>ipson</em>,  <em>ipson</em> and  <em>ipson asd</em>. lorem lore lrem. lorem lore lremlorem lore lremlorem lore lremlorem lore lremlorem lore lremlorem lore lremlorem lore lremlorem lore lrem </p>
					</div>
					</figure>
					</div>
					</section>
      		</div>
      		<?php }	?>
      		

						</div>
					</div>
				</div>
				<script src="admin454/plugins/fastclick/fastclick.min.js"></script>
				<!-- AdminLTE App -->
				<script src="admin454/dist/js/app.min.js"></script>
				<!-- Sparkline -->
				<script src="admin454/plugins/sparkline/jquery.sparkline.min.js"></script>

				<!-- SlimScroll 1.3.0 -->
				<script src="admin454/plugins/slimScroll/jquery.slimscroll.min.js"></script>
				<script src="admin454/dist/js/demo.js"></script>

				<script src="admin454/plugins/input-mask/jquery.inputmask.js"></script>
				<script
				src="admin454/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
				<script
				src="admin454/plugins/input-mask/jquery.inputmask.extensions.js"></script>

			</body>
			</html>
