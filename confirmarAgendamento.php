<?php

/*
 * $email = 'mvbdesenvolvimento@gmail.com'; $token = '807FA521175B43C5B8DD7639D329F1CB'; $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/?email=' . $email . '&token=' . $token; $xml = '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?> <checkout> <currency>BRL</currency> <redirectURL>http://localhost/prototipos/tarot/paymentCompleted.php</redirectURL> <items> <item> <id>0001</id> <description>Notebook Prata</description> <amount>1.00</amount> <quantity>1</quantity> <weight>1000</weight> </item> <item> <id>0002</id> <description>Notebook Rosa</description> <amount>2.00</amount> <quantity>2</quantity> <weight>750</weight> </item> </items> <reference>'.uniqid('TAROT').'</reference> <sender> <name>José Comprador</name> <email>sounoob@comprador.com.br</email> <phone> <areaCode>11</areaCode> <number>55663377</number> </phone> <documents> <document> <type>CPF</type> <value>34891629860</value> <bornDate>19/07/1986</bornDate> </document> </documents> </sender> <shipping> <type>3</type> <address> <street>Rua sem nome</street> <number>1384</number> <complement>5o andar</complement> <district>Jardim Paulistano</district> <postalCode>01452002</postalCode> <city>Sao Paulo</city> <state>SP</state> <country>BRA</country> </address> </shipping> </checkout>'; $curl = curl_init($url); curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/xml; charset=ISO-8859-1')); curl_setopt($curl, CURLOPT_POSTFIELDS, $xml); $xml= curl_exec($curl); if($xml == 'Unauthorized'){ //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção header('Location: paginaDeErro.php'); exit;//Mantenha essa linha } curl_close($curl); $xml= simplexml_load_string($xml); var_dump($xml); //header('Location: https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml -> code);
 */
 session_start();
 include_once('conf/config.php');
$cod_agendamento = base64_decode($_GET['agendamento']);

$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode($usuario);
$agenda = $db->select('agendamento' , 'id_agendamento = '.$cod_agendamento);
$agenda = $agenda[0];



?>
<form id='frmAgendamento' name='fmrAgendamento' onsubmit=return false>


<input type='hidden' id='agendamento' name='agendamento' value = "<?php echo $_GET['agendamento'] ;?>">
<div class="container-fluid text-center">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 agendamentoConsulta">

		<div class="form-group">

			 <div class="form-group has-feedback">
               <h3>Agendamento de consulta</h3>
              </div>
		</div>

		<table class="table table-striped">
         		<tr>
         		<td class='text-left'>Nome completo:</td>
         		<td class='text-left'>   <?php echo $jsonUsuario->nome_cliente;?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>E-mail:</td>
         		<td class='text-left'>      <?php echo $jsonUsuario->email_cliente;?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>Nascimento:</td>
         		<td class='text-left'>     <?php echo date('d/m/Y',strtotime($jsonUsuario->nascimento_cliente));?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>Saldo Atual:</td>
         		<td class='text-left'>        R$ <?php echo number_format($jsonUsuario->saldo_cliente,2,',','');?></td>
         		</tr>

         	
         		<tr>
         		<td class='text-left'>Início da consulta:</td>
         		<td class='text-left'>     <?php echo date('d/m/Y H:i:s',strtotime($agenda['inicio_agendamento']));?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>Fim:</td>
         		<td class='text-left'>  <?php echo date('d/m/Y H:i:s',strtotime($agenda['fim_agendamento']));?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>Valor da consulta:</td>
         		<td class='text-left'>     R$ <?php echo number_format($agenda['valor_agendamento'],2,',','');?></td>
         		</tr>


         		<tr>
         		<td class='text-left'>Saldo após consulta:</td>
         		<td class='text-left'>   R$ <?php echo number_format($jsonUsuario->saldo_cliente - $agenda['valor_agendamento'],2,',','');?></td>
         		</tr>

         		<tr>

         		<td colspan='2'>
         		<textarea class="textarea" name="motivo_consulta"
                  placeholder="Faça uma breve descrição do motivo da sua consulta para o nosso consultor se possível"
                  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                  cols="" rows=""></textarea>
         		</td>
         		</tr>

         		<td colspan='2'>
         	 <button type="button" class="btn btn-default btn-block btn-flat" id='btnEfetivar' onclick="efetivarAgendamento()" >Agendar</button>
         		</td>
         		</tr>
		</table>
      </div>
    </div>
</div>
</form>

<script>
function efetivarAgendamento(){
$.post('process/efetuarAgendamento.php' , $("#frmAgendamento").serialize(),function(data){

  if(data == 'ok'){
    	$('#loginModal').modal('hide');

    	//document.location='index.php';

        $(".modal-body").load('dadosCliente.php',function(){
                            $('#loginModal').modal('show');
        })


  }else{
    alert('O horário escolhido já foi agendado');
  }
})

}



</script>
