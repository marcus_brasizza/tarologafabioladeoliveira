<?php


 session_start();
 include_once('conf/config.php');
$cod_agendamento = base64_decode($_GET['agendamento']);


$usuario = ($_SESSION ['CM_GRANO_USER'] ['client']);
$jsonUsuario = json_decode($usuario);
$agenda = $db->select('agendamento' , 'id_agendamento = '.$cod_agendamento);
$agenda = $agenda[0];




?>
<form id='frmAgendamento' name='fmrAgendamento' onsubmit=return false>


<input type='hidden' id='agendamento' name='agendamento' value = "<?php echo $_GET['agendamento'] ;?>">
<input type='hidden' id='agendamento_cliente' name='agendamento_cliente' value = "<?php echo $_GET['agendamento_cliente'] ;?>">
<div class="container-fluid text-center">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 agendamentoConsulta">

		<div class="form-group">

			 <div class="form-group has-feedback">
               <h3>Avaliar a consulta oferecida</h3>
              </div>
		</div>

		<table class="table table-striped">
         		<tr>
         		<td class='text-left'>Nome completo:</td>
         		<td class='text-left'>   <?php echo $jsonUsuario->nome_cliente;?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>E-mail:</td>
         		<td class='text-left'>      <?php echo $jsonUsuario->email_cliente;?></td>
         		</tr>

         	

         	
         		<tr>
         		<td class='text-left'>Início da consulta:</td>
         		<td class='text-left'>     <?php echo date('d/m/Y H:i:s',strtotime($agenda['inicio_agendamento']));?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>Fim:</td>
         		<td class='text-left'>  <?php echo date('d/m/Y H:i:s',strtotime($agenda['fim_agendamento']));?></td>
         		</tr>

         		<tr>
         		<td class='text-left'>Valor da consulta:</td>
         		<td class='text-left'>     R$ <?php echo number_format($agenda['valor_agendamento'],2,',','');?></td>
         		</tr>


         		<tr>

         		<td colspan='2'>
         		<textarea class="textarea" name="mensagem_agendamento_cliente"
                  placeholder="Faça uma breve descrição de como foi a sua consulta e se lhe agradou ou algo deixou a desejar"
                  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                  cols="" rows=""></textarea>
         		</td>
         		</tr>

                    <tr>
                <td class='text-left'>Avalie a consulta de 0 - 10</td>
                <td class='text-left'>
                <select name='nota_agendamento'>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                </td>
                </tr>


         		<td colspan='2'>
         	 <button type="button" class="btn btn-default btn-block btn-flat" id='btnEfetivar' onclick="efetivarAvaliacao()" >Avaliar consulta</button>
         		</td>
         		</tr>
		</table>
      </div>
    </div>
</div>
</form>

<script>
function efetivarAvaliacao(){

$.post('process/efetuarAvaliacao.php' , $("#frmAgendamento").serialize(),function(data){
        $('#loginModal').modal('hide');      
        $(".modal-body").load('dadosCliente.php',function(){
                            $('#loginModal').modal('show');
        })
})

}



</script>
