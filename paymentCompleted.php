<?php
session_start();
include_once('conf/config.php');

$configs = $db->select('configuracoes');
$configs = $configs[0];

$email = $configs['email_pagseguro'];
$token = $configs['token_pagseguro'];
$urlPagseguro = $configs['ambiente']==1 ? $configs['url_pagseguro_homolog'] : $configs['url_pagseguro_prod'];
$qry_str = "?email={$email}&token={$token}";
 $xmlUrl= $urlPagseguro."/v3/transactions/".$_REQUEST['transaction_id'];

$ch = curl_init();
// Set query data here with the URL
curl_setopt($ch, CURLOPT_URL, $xmlUrl . $qry_str);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, '10');
// $verbose = fopen('php://temp', 'w+');
// curl_setopt($ch, CURLOPT_VERBOSE, true);
// curl_setopt($ch, CURLOPT_STDERR, $verbose);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$content = trim(curl_exec($ch));
//
// rewind($verbose);
// $verboseLog = stream_get_contents($verbose);
//
// echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
curl_close($ch);
$simple = simplexml_load_string($content);
$reference = (string)$simple->reference;
$bind = array(':numero_pagamento'=>$reference);


$pagamento = $db->select('pagamento' , 'numero_pagamento = :numero_pagamento' , $bind);

if($pagamento[0]){
	//Atualiza o pagamento
	$pagamento = $pagamento[0];	
	
	$dados_cliente = ($simple->sender);
	$id_forma = (string)($simple->paymentMethod->code);
	$status = (string)$simple->status;
	$cliente = $db->select('cliente' , 'id_cliente = '.$pagamento['id_cliente']);
	if($cliente){
		$cliente = $cliente[0];
		$valorCredito = (string)($simple->grossAmount);
		if($status == 3){
		$cliente['saldo_cliente']  = $cliente['saldo_cliente'] + $valorCredito;
		$pagamento['credito_adicionado'] = 1;
		}
		
		$cliente['telefone_cliente'] = (string)$simple->sender->phone->areaCode.''.(string)$simple->sender->phone->number;
		$cliente['cpf_cliente'] = (string)$simple->sender->documents->document->value;
		$cliente['endereco_cliente'] = (string)$simple->shipping->address->street;
		$cliente['numero_cliente'] = (string)$simple->shipping->address->number;
		$cliente['complemento_cliente'] = (string)$simple->shipping->address->complement;
		$cliente['cidade_cliente'] = (string)$simple->shipping->address->city;
		$cliente['estado_cliente'] = (string)$simple->shipping->address->state;
		$db->update('cliente',$cliente,'id_cliente = '.$cliente['id_cliente']);
		
		
		
		$pagamento['id_forma'] = $id_forma;
		$pagamento['status_pagamento'] = $status;
		
		$pagamento['transacao_pagamento'] = (string)$simple->code;
		if($pagamento['transacao_pagamento'] == ''){
			$pagamento['transacao_pagamento'] = $_REQUEST['transaction_id'];
		}
		$db->update('pagamento',$pagamento,' id_pagamento = '.$pagamento['id_pagamento']);
	}
}
file_put_contents('/tmp/simple.txt', print_r($simple,true));
if($status <= 4){
echo 'ok';
exit;
}else{
  echo 'erro';
  exit;
}

 ?>